﻿update admin
set password = crypt( encode(digest('password', 'sha256'), 'hex'), gen_salt('bf', 14));
