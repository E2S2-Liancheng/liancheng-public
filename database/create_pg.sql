--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: mashup; Type: DATABASE; Schema: -; Owner: thomas
--

CREATE DATABASE mashup WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_SG.UTF-8' LC_CTYPE = 'en_SG.UTF-8';


ALTER DATABASE mashup OWNER TO thomas;

\connect mashup

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account_status; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE account_status (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.account_status OWNER TO thomas;

--
-- Name: admin; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE admin (
    id integer NOT NULL,
    name text NOT NULL,
    email text NOT NULL,
    username text NOT NULL,
    password text NOT NULL
);


ALTER TABLE public.admin OWNER TO thomas;

--
-- Name: admin_id_seq; Type: SEQUENCE; Schema: public; Owner: thomas
--

CREATE SEQUENCE admin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_id_seq OWNER TO thomas;

--
-- Name: admin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thomas
--

ALTER SEQUENCE admin_id_seq OWNED BY admin.id;


--
-- Name: crew; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE crew (
    id bigint NOT NULL,
    name text NOT NULL,
    creator_member_id integer NOT NULL
);


ALTER TABLE public.crew OWNER TO thomas;

--
-- Name: crew_id_seq; Type: SEQUENCE; Schema: public; Owner: thomas
--

CREATE SEQUENCE crew_id_seq
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crew_id_seq OWNER TO thomas;

--
-- Name: crew_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thomas
--

ALTER SEQUENCE crew_id_seq OWNED BY crew.id;


--
-- Name: crew_member; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE crew_member (
    crew_id bigint NOT NULL,
    member_id integer NOT NULL
);


ALTER TABLE public.crew_member OWNER TO thomas;

--
-- Name: directory; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE directory (
    id bigint NOT NULL,
    name text NOT NULL,
    modified_at timestamp without time zone NOT NULL,
    parent_id bigint
);


ALTER TABLE public.directory OWNER TO thomas;

--
-- Name: directory_id_seq; Type: SEQUENCE; Schema: public; Owner: thomas
--

CREATE SEQUENCE directory_id_seq
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.directory_id_seq OWNER TO thomas;

--
-- Name: directory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thomas
--

ALTER SEQUENCE directory_id_seq OWNED BY directory.id;


--
-- Name: file_obfuscated; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE file_obfuscated (
    id text NOT NULL,
    member_id integer NOT NULL,
    inode_id bigint NOT NULL,
    annotation text
);


ALTER TABLE public.file_obfuscated OWNER TO thomas;

--
-- Name: file_published; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE file_published (
    member_id integer NOT NULL,
    inode_id bigint NOT NULL
);


ALTER TABLE public.file_published OWNER TO thomas;

--
-- Name: inode; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE inode (
    id bigint NOT NULL,
    name text NOT NULL,
    directory_id bigint NOT NULL
);


ALTER TABLE public.inode OWNER TO thomas;

--
-- Name: inode_id_seq; Type: SEQUENCE; Schema: public; Owner: thomas
--

CREATE SEQUENCE inode_id_seq
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inode_id_seq OWNER TO thomas;

--
-- Name: inode_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thomas
--

ALTER SEQUENCE inode_id_seq OWNED BY inode.id;


--
-- Name: inode_revision; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE inode_revision (
    inode_id bigint NOT NULL,
    revision bigint NOT NULL,
    size bigint NOT NULL,
    modified_by integer NOT NULL,
    modified_at timestamp without time zone NOT NULL,
    mime_type_id integer NOT NULL,
    storage_file_id bigint
);


ALTER TABLE public.inode_revision OWNER TO thomas;

--
-- Name: job; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE job (
    id bigint NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    started_at timestamp without time zone,
    finished_at timestamp without time zone,
    job_status_id integer NOT NULL
);


ALTER TABLE public.job OWNER TO thomas;

--
-- Name: job_id_seq; Type: SEQUENCE; Schema: public; Owner: thomas
--

CREATE SEQUENCE job_id_seq
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.job_id_seq OWNER TO thomas;

--
-- Name: job_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thomas
--

ALTER SEQUENCE job_id_seq OWNED BY job.id;


--
-- Name: job_input; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE job_input (
    job_id bigint NOT NULL,
    inode_id bigint NOT NULL,
    revision bigint NOT NULL
);


ALTER TABLE public.job_input OWNER TO thomas;

--
-- Name: job_output; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE job_output (
    job_id bigint NOT NULL,
    inode_id bigint NOT NULL,
    revision bigint NOT NULL
);


ALTER TABLE public.job_output OWNER TO thomas;

--
-- Name: job_status; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE job_status (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.job_status OWNER TO thomas;

--
-- Name: job_workflow; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE job_workflow (
    job_id bigint NOT NULL,
    inode_id bigint NOT NULL,
    revision bigint NOT NULL
);


ALTER TABLE public.job_workflow OWNER TO thomas;

--
-- Name: member; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE member (
    id integer NOT NULL,
    name text NOT NULL,
    email text NOT NULL,
    password text NOT NULL,
    home_directory_id bigint,
    trash_directory_id bigint,
    username text,
    account_status_id integer NOT NULL
);


ALTER TABLE public.member OWNER TO thomas;

--
-- Name: member_id_seq; Type: SEQUENCE; Schema: public; Owner: thomas
--

CREATE SEQUENCE member_id_seq
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.member_id_seq OWNER TO thomas;

--
-- Name: member_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thomas
--

ALTER SEQUENCE member_id_seq OWNED BY member.id;


--
-- Name: mime_type; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE mime_type (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.mime_type OWNER TO thomas;

--
-- Name: mime_type_id_seq; Type: SEQUENCE; Schema: public; Owner: thomas
--

CREATE SEQUENCE mime_type_id_seq
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mime_type_id_seq OWNER TO thomas;

--
-- Name: mime_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thomas
--

ALTER SEQUENCE mime_type_id_seq OWNED BY mime_type.id;


--
-- Name: share_invite; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE share_invite (
    directory_id bigint NOT NULL,
    member_id integer NOT NULL,
    share_invite_status_id integer NOT NULL,
    item_read boolean NOT NULL
);


ALTER TABLE public.share_invite OWNER TO thomas;

--
-- Name: share_invite_status; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE share_invite_status (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.share_invite_status OWNER TO thomas;

--
-- Name: share_with_crew; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE share_with_crew (
    directory_id bigint NOT NULL,
    crew_id bigint NOT NULL,
    read_write boolean
);


ALTER TABLE public.share_with_crew OWNER TO thomas;

--
-- Name: share_with_member; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE share_with_member (
    directory_id bigint NOT NULL,
    member_id integer NOT NULL,
    read_write boolean
);


ALTER TABLE public.share_with_member OWNER TO thomas;

--
-- Name: storage_file; Type: TABLE; Schema: public; Owner: thomas; Tablespace:
--

CREATE TABLE storage_file (
    id bigint NOT NULL,
    md5_id text NOT NULL,
    sha1sum text NOT NULL
);


ALTER TABLE public.storage_file OWNER TO thomas;

--
-- Name: storage_file_id_seq; Type: SEQUENCE; Schema: public; Owner: thomas
--

CREATE SEQUENCE storage_file_id_seq
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.storage_file_id_seq OWNER TO thomas;

--
-- Name: storage_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thomas
--

ALTER SEQUENCE storage_file_id_seq OWNED BY storage_file.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY admin ALTER COLUMN id SET DEFAULT nextval('admin_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY crew ALTER COLUMN id SET DEFAULT nextval('crew_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY directory ALTER COLUMN id SET DEFAULT nextval('directory_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY inode ALTER COLUMN id SET DEFAULT nextval('inode_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY job ALTER COLUMN id SET DEFAULT nextval('job_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY member ALTER COLUMN id SET DEFAULT nextval('member_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY mime_type ALTER COLUMN id SET DEFAULT nextval('mime_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY storage_file ALTER COLUMN id SET DEFAULT nextval('storage_file_id_seq'::regclass);


--
-- Data for Name: account_status; Type: TABLE DATA; Schema: public; Owner: thomas
--

COPY account_status (id, name) FROM stdin;
1	validation pending
10	active
\.


--
-- Data for Name: job_status; Type: TABLE DATA; Schema: public; Owner: thomas
--

COPY job_status (id, name) FROM stdin;
1	created
3	cancelled
4	failed
5	completed
2	started
\.


--
-- Data for Name: mime_type; Type: TABLE DATA; Schema: public; Owner: thomas
--

COPY mime_type (id, name) FROM stdin;
1	application/x-urbandb-workflow
2	application/x-urbandb-stream
\.


--
-- Data for Name: share_invite_status; Type: TABLE DATA; Schema: public; Owner: thomas
--

COPY share_invite_status (id, name) FROM stdin;
1	invited
2	blocked
10	accepted
\.


--
-- Name: account_status_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY account_status
    ADD CONSTRAINT account_status_pk PRIMARY KEY (id);


--
-- Name: admin_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT admin_pk PRIMARY KEY (id);


--
-- Name: admin_uq_email; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT admin_uq_email UNIQUE (email);


--
-- Name: admin_uq_username; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT admin_uq_username UNIQUE (username);


--
-- Name: crew_member_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY crew_member
    ADD CONSTRAINT crew_member_pk PRIMARY KEY (crew_id, member_id);


--
-- Name: crew_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY crew
    ADD CONSTRAINT crew_pk PRIMARY KEY (id);


--
-- Name: directory_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY directory
    ADD CONSTRAINT directory_pk PRIMARY KEY (id);


--
-- Name: directory_uq_name; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY directory
    ADD CONSTRAINT directory_uq_name UNIQUE (name, parent_id);


--
-- Name: file_obfuscated_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY file_obfuscated
    ADD CONSTRAINT file_obfuscated_pk PRIMARY KEY (id);


--
-- Name: file_published_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY file_published
    ADD CONSTRAINT file_published_pk PRIMARY KEY (member_id, inode_id);


--
-- Name: inode_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY inode
    ADD CONSTRAINT inode_pk PRIMARY KEY (id);


--
-- Name: inode_revision_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY inode_revision
    ADD CONSTRAINT inode_revision_pk PRIMARY KEY (inode_id, revision);


--
-- Name: job_input_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY job_input
    ADD CONSTRAINT job_input_pk PRIMARY KEY (job_id, inode_id, revision);


--
-- Name: job_output_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY job_output
    ADD CONSTRAINT job_output_pk PRIMARY KEY (job_id, inode_id, revision);


--
-- Name: job_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY job
    ADD CONSTRAINT job_pk PRIMARY KEY (id);


--
-- Name: job_status_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY job_status
    ADD CONSTRAINT job_status_pk PRIMARY KEY (id);


--
-- Name: job_workflow_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY job_workflow
    ADD CONSTRAINT job_workflow_pk PRIMARY KEY (job_id, inode_id, revision);


--
-- Name: member_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY member
    ADD CONSTRAINT member_pk PRIMARY KEY (id);


--
-- Name: member_uq_name; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY member
    ADD CONSTRAINT member_uq_name UNIQUE (name);


--
-- Name: mime_type_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY mime_type
    ADD CONSTRAINT mime_type_pk PRIMARY KEY (id);


--
-- Name: mime_type_uq_name; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY mime_type
    ADD CONSTRAINT mime_type_uq_name UNIQUE (name);


--
-- Name: share_invite_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY share_invite
    ADD CONSTRAINT share_invite_pk PRIMARY KEY (directory_id, member_id);


--
-- Name: share_invite_status_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY share_invite_status
    ADD CONSTRAINT share_invite_status_pk PRIMARY KEY (id);


--
-- Name: share_with_crew_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY share_with_crew
    ADD CONSTRAINT share_with_crew_pk PRIMARY KEY (directory_id, crew_id);


--
-- Name: share_with_member_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY share_with_member
    ADD CONSTRAINT share_with_member_pk PRIMARY KEY (directory_id, member_id);


--
-- Name: storage_file_pk; Type: CONSTRAINT; Schema: public; Owner: thomas; Tablespace:
--

ALTER TABLE ONLY storage_file
    ADD CONSTRAINT storage_file_pk PRIMARY KEY (id);


--
-- Name: fki_inode_revision_fk_modified_by; Type: INDEX; Schema: public; Owner: thomas; Tablespace:
--

CREATE INDEX fki_inode_revision_fk_modified_by ON inode_revision USING btree (modified_by);


--
-- Name: fki_member_trash_directory_id_fk; Type: INDEX; Schema: public; Owner: thomas; Tablespace:
--

CREATE INDEX fki_member_trash_directory_id_fk ON member USING btree (trash_directory_id);


--
-- Name: fki_share_invite_share_invite_status_id; Type: INDEX; Schema: public; Owner: thomas; Tablespace:
--

CREATE INDEX fki_share_invite_share_invite_status_id ON share_invite USING btree (share_invite_status_id);


--
-- Name: index_storage_file_sha1sum; Type: INDEX; Schema: public; Owner: thomas; Tablespace:
--

CREATE INDEX index_storage_file_sha1sum ON storage_file USING btree (sha1sum);


--
-- Name: crew_fk_creator_member_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY crew
    ADD CONSTRAINT crew_fk_creator_member_id FOREIGN KEY (creator_member_id) REFERENCES member(id);


--
-- Name: crewm_member_fk_crew_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY crew_member
    ADD CONSTRAINT crewm_member_fk_crew_id FOREIGN KEY (crew_id) REFERENCES crew(id);


--
-- Name: crewm_member_fk_member_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY crew_member
    ADD CONSTRAINT crewm_member_fk_member_id FOREIGN KEY (member_id) REFERENCES member(id);


--
-- Name: directory_fk_parent_directory_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY directory
    ADD CONSTRAINT directory_fk_parent_directory_id FOREIGN KEY (parent_id) REFERENCES directory(id);


--
-- Name: file_obfuscated_fk_inode_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY file_obfuscated
    ADD CONSTRAINT file_obfuscated_fk_inode_id FOREIGN KEY (inode_id) REFERENCES inode(id);


--
-- Name: file_obfuscated_fk_member_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY file_obfuscated
    ADD CONSTRAINT file_obfuscated_fk_member_id FOREIGN KEY (member_id) REFERENCES member(id);


--
-- Name: file_published_fk_inode_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY file_published
    ADD CONSTRAINT file_published_fk_inode_id FOREIGN KEY (inode_id) REFERENCES inode(id);


--
-- Name: file_published_fk_member_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY file_published
    ADD CONSTRAINT file_published_fk_member_id FOREIGN KEY (member_id) REFERENCES member(id);


--
-- Name: inode_fk_directory_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY inode
    ADD CONSTRAINT inode_fk_directory_id FOREIGN KEY (directory_id) REFERENCES directory(id);


--
-- Name: inode_revision_fk_inode_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY inode_revision
    ADD CONSTRAINT inode_revision_fk_inode_id FOREIGN KEY (inode_id) REFERENCES inode(id);


--
-- Name: inode_revision_fk_mime_type_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY inode_revision
    ADD CONSTRAINT inode_revision_fk_mime_type_id FOREIGN KEY (mime_type_id) REFERENCES mime_type(id);


--
-- Name: inode_revision_fk_modified_by; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY inode_revision
    ADD CONSTRAINT inode_revision_fk_modified_by FOREIGN KEY (modified_by) REFERENCES member(id);


--
-- Name: inode_revision_fk_storage_file_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY inode_revision
    ADD CONSTRAINT inode_revision_fk_storage_file_id FOREIGN KEY (storage_file_id) REFERENCES storage_file(id);


--
-- Name: job_fk_created_by; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY job
    ADD CONSTRAINT job_fk_created_by FOREIGN KEY (created_by) REFERENCES member(id);


--
-- Name: job_fk_job_status_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY job
    ADD CONSTRAINT job_fk_job_status_id FOREIGN KEY (job_status_id) REFERENCES job_status(id);


--
-- Name: job_input_fk_inode_revision; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY job_input
    ADD CONSTRAINT job_input_fk_inode_revision FOREIGN KEY (inode_id, revision) REFERENCES inode_revision(inode_id, revision);


--
-- Name: job_input_fk_job_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY job_input
    ADD CONSTRAINT job_input_fk_job_id FOREIGN KEY (job_id) REFERENCES job(id);


--
-- Name: job_output_fk_inode_revision; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY job_output
    ADD CONSTRAINT job_output_fk_inode_revision FOREIGN KEY (inode_id, revision) REFERENCES inode_revision(inode_id, revision);


--
-- Name: job_output_fk_job_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY job_output
    ADD CONSTRAINT job_output_fk_job_id FOREIGN KEY (job_id) REFERENCES job(id);


--
-- Name: job_workflow_fk_inode_revision; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY job_workflow
    ADD CONSTRAINT job_workflow_fk_inode_revision FOREIGN KEY (inode_id, revision) REFERENCES inode_revision(inode_id, revision);


--
-- Name: job_workflow_fk_job_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY job_workflow
    ADD CONSTRAINT job_workflow_fk_job_id FOREIGN KEY (job_id) REFERENCES job(id);


--
-- Name: member_fk_account_status_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY member
    ADD CONSTRAINT member_fk_account_status_id FOREIGN KEY (account_status_id) REFERENCES account_status(id);


--
-- Name: member_fk_home_directory_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY member
    ADD CONSTRAINT member_fk_home_directory_id FOREIGN KEY (home_directory_id) REFERENCES directory(id);


--
-- Name: member_trash_directory_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY member
    ADD CONSTRAINT member_trash_directory_id_fk FOREIGN KEY (trash_directory_id) REFERENCES directory(id);


--
-- Name: share_invite_directory_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY share_invite
    ADD CONSTRAINT share_invite_directory_id_fk FOREIGN KEY (directory_id) REFERENCES directory(id);


--
-- Name: share_invite_member_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY share_invite
    ADD CONSTRAINT share_invite_member_id_fk FOREIGN KEY (member_id) REFERENCES member(id);


--
-- Name: share_invite_share_invite_status_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY share_invite
    ADD CONSTRAINT share_invite_share_invite_status_id FOREIGN KEY (share_invite_status_id) REFERENCES share_invite_status(id);


--
-- Name: share_with_crew_fk_crew_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY share_with_crew
    ADD CONSTRAINT share_with_crew_fk_crew_id FOREIGN KEY (crew_id) REFERENCES crew(id);


--
-- Name: share_with_crew_fk_directory_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY share_with_crew
    ADD CONSTRAINT share_with_crew_fk_directory_id FOREIGN KEY (directory_id) REFERENCES directory(id);


--
-- Name: share_with_member_fk_directory_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY share_with_member
    ADD CONSTRAINT share_with_member_fk_directory_id FOREIGN KEY (directory_id) REFERENCES directory(id);


--
-- Name: share_with_member_fk_member_id; Type: FK CONSTRAINT; Schema: public; Owner: thomas
--

ALTER TABLE ONLY share_with_member
    ADD CONSTRAINT share_with_member_fk_member_id FOREIGN KEY (member_id) REFERENCES member(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO thomas;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

