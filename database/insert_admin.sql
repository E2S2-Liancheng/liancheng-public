﻿insert into admin( name,
                   email,
                   username,
                   password )
values( 'Admin',
        'admin@nus.edu.sg',
        'admin',
        crypt( encode(digest('password', 'sha256'), 'hex'),
               gen_salt('bf', 14)) );
