--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.13
-- Dumped by pg_dump version 9.1.13
-- Started on 2014-07-03 18:10:27 SGT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1921 (class 1262 OID 34404)
-- Name: foreign_jail; Type: DATABASE; Schema: -; Owner: foreign_worker
--

CREATE DATABASE foreign_jail WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_SG.UTF-8' LC_CTYPE = 'en_SG.UTF-8';


ALTER DATABASE foreign_jail OWNER TO foreign_worker;

\connect foreign_jail

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 167 (class 3079 OID 11687)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1924 (class 0 OID 0)
-- Dependencies: 167
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 168 (class 3079 OID 34405)
-- Dependencies: 5
-- Name: file_fdw; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS file_fdw WITH SCHEMA public;


--
-- TOC entry 1925 (class 0 OID 0)
-- Dependencies: 168
-- Name: EXTENSION file_fdw; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION file_fdw IS 'foreign-data wrapper for flat file access';


--
-- TOC entry 1438 (class 1417 OID 34409)
-- Dependencies: 1437
-- Name: file_fdw_server; Type: SERVER; Schema: -; Owner: postgres
--

CREATE SERVER file_fdw_server FOREIGN DATA WRAPPER file_fdw;


ALTER SERVER file_fdw_server OWNER TO postgres;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

--
-- TOC entry 1923 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-07-03 18:10:27 SGT

--
-- PostgreSQL database dump complete
--

