﻿insert into member(name,
                   password,
                   email,
                   home_directory_id,
                   account_status_id)
values('Member',
       crypt(encode(digest('password', 'sha256'), 'hex'), gen_salt('bf', 14)),
       'member@nus.edu.sg',
       NULL,
       10);
