#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'digest'
require 'thread'

module HeaderAssets
  # The development? method from main is a bit hard to access from within a module
  development = TOPLEVEL_BINDING.eval('self').send(:'development?')

  # List of CSS files that should always be included
  GLOBAL_CSS = [ 'layout.css' ]

  # List of extra CSS files for a given resource
  EXTRA_CSS = { 'elfinder' => [ "jquery/smoothness/jquery-ui-1.10.4.custom#{development ? "" : ".min"}.css",
                                "elfinder/css/common.css",
                                "elfinder/css/dialog.css",
                                "elfinder/css/toolbar.css",
                                "elfinder/css/navbar.css",
                                "elfinder/css/statusbar.css",
                                "elfinder/css/contextmenu.css",
                                "elfinder/css/cwd.css",
                                "elfinder/css/quicklook.css",
                                "elfinder/css/commands.css",
                                "elfinder/css/fonts.css",
                                "elfinder/css/theme.css" ],
                'workflow' => ["jquery/jquery-ui-1.11.4/jquery-ui#{development ? "" : ".min"}.css",
                               "file_picker.css" ],
                'streams' => [ "jquery/smoothness/jquery-ui-1.10.4.custom#{development ? '' : '.min'}.css",
                               'file_picker.css' ],
                'visu' => [ 'visu/jquery-ui-1.8.css',
                            'visu/css/graph.css',
                            'visu/css/detail.css',
                            'visu/css/legend.css',
                            'visu/css/extensions.css' ],
                'admin/login' => [ 'login.css' ],
              }

  # List of JS files that should always be included
  GLOBAL_JS = [ "jquery/jquery-2.1.3#{development ? "" : ".min"}.js" ]

  # List of extra JS files for a given resource
  EXTRA_JS = { 'login' => ['sha256.js'],
               'register' => ['sha256.js'],
               'settings' => ['sha256.js'],
               'elfinder' => [ "jquery/jquery-ui-1.10.4.custom#{development ? "" : ".min"}.js",
                               # core
                               "elfinder/js/elFinder.js",
                               "elfinder/js/elFinder.version.js",
                               "elfinder/js/jquery.elfinder.js",
                               "elfinder/js/elFinder.resources.js",
                               "elfinder/js/elFinder.options.js",
                               "elfinder/js/elFinder.history.js",
                               "elfinder/js/elFinder.command.js",
                               # ui
                               "elfinder/js/ui/overlay.js",
                               "elfinder/js/ui/workzone.js",
                               "elfinder/js/ui/navbar.js",
                               "elfinder/js/ui/dialog.js",
                               "elfinder/js/ui/tree.js",
                               "elfinder/js/ui/cwd.js",
                               "elfinder/js/ui/toolbar.js",
                               "elfinder/js/ui/button.js",
                               "elfinder/js/ui/uploadButton.js",
                               "elfinder/js/ui/viewbutton.js",
                               "elfinder/js/ui/searchbutton.js",
                               "elfinder/js/ui/sortbutton.js",
                               "elfinder/js/ui/panel.js",
                               "elfinder/js/ui/contextmenu.js",
                               "elfinder/js/ui/path.js",
                               "elfinder/js/ui/stat.js",
                               "elfinder/js/ui/places.js",
                               # commands
                               "elfinder/js/commands/back.js",
                               "elfinder/js/commands/forward.js",
                               "elfinder/js/commands/reload.js",
                               "elfinder/js/commands/up.js",
                               "elfinder/js/commands/home.js",
                               "elfinder/js/commands/copy.js",
                               "elfinder/js/commands/cut.js",
                               "elfinder/js/commands/paste.js",
                               "elfinder/js/commands/open.js",
                               "elfinder/js/commands/rm.js",
                               "elfinder/js/commands/info.js",
                               "elfinder/js/commands/duplicate.js",
                               "elfinder/js/commands/rename.js",
                               "elfinder/js/commands/help.js",
                               "elfinder/js/commands/getfile.js",
                               "elfinder/js/commands/mkdir.js",
                               "elfinder/js/commands/mkfile.js",
                               "elfinder/js/commands/upload.js",
                               "elfinder/js/commands/download.js",
                               "elfinder/js/commands/edit.js",
                               "elfinder/js/commands/quicklook.js",
                               "elfinder/js/commands/quicklook.plugins.js",
                               "elfinder/js/commands/extract.js",
                               "elfinder/js/commands/archive.js",
                               "elfinder/js/commands/search.js",
                               "elfinder/js/commands/view.js",
                               "elfinder/js/commands/resize.js",
                               "elfinder/js/commands/sort.js",
                               "elfinder/js/commands/netmount.js",
                               "elfinder/js/commands/share.js",
                               "elfinder/js/commands/new_workflow.js",
                               "elfinder/js/commands/edit_workflow.js",
                               "elfinder/js/commands/external_access.js",
                               # languages
                               "elfinder/js/i18n/elfinder.ar.js",
                               "elfinder/js/i18n/elfinder.bg.js",
                               "elfinder/js/i18n/elfinder.ca.js",
                               "elfinder/js/i18n/elfinder.cs.js",
                               "elfinder/js/i18n/elfinder.de.js",
                               "elfinder/js/i18n/elfinder.el.js",
                               "elfinder/js/i18n/elfinder.en.js",
                               "elfinder/js/i18n/elfinder.es.js",
                               "elfinder/js/i18n/elfinder.fa.js",
                               "elfinder/js/i18n/elfinder.fr.js",
                               "elfinder/js/i18n/elfinder.hu.js",
                               "elfinder/js/i18n/elfinder.it.js",
                               "elfinder/js/i18n/elfinder.jp.js",
                               "elfinder/js/i18n/elfinder.ko.js",
                               "elfinder/js/i18n/elfinder.nl.js",
                               "elfinder/js/i18n/elfinder.no.js",
                               "elfinder/js/i18n/elfinder.pl.js",
                               "elfinder/js/i18n/elfinder.pt_BR.js",
                               "elfinder/js/i18n/elfinder.ru.js",
                               "elfinder/js/i18n/elfinder.sl.js",
                               "elfinder/js/i18n/elfinder.sv.js",
                               "elfinder/js/i18n/elfinder.tr.js",
                               "elfinder/js/i18n/elfinder.zh_CN.js",
                               "elfinder/js/i18n/elfinder.zh_TW.js",
                               "elfinder/js/i18n/elfinder.vi.js",
                               # whatever
                               "elfinder/js/jquery.dialogelfinder.js" ],
               'workflow' => [ "jquery/jquery-ui-1.11.4/jquery-ui#{development ? '' : '.min'}.js",
                               'workflow/detect_resize.js',
                               'workflow/nodes.js',
                               'workflow/connectors.js',
                               'workflow/workflow.js',
                               'workflow/import_csv.js',
                               'workflow/projection.js',
                               'workflow/filter.js',
                               'workflow/smooth.js',
                               'workflow/copy.js',
                               'workflow/optiliner.js',
                               'workflow/provconcn.js',
                               'workflow/export_csv.js',
                               'workflow/export_optikml.js',
                               'file_picker.js' ],
               'streams' => [ "jquery/jquery-ui-1.10.4.custom#{development ? '' : '.min'}.js",
                              'file_picker.js' ],
               'visu' => [  'jquery/no_conflict.js',
                            'visu/js/d3.v3.js',
                            'visu/jquery-1.6.2.min.js',
                            'jquery/no_conflict.js',
                            'visu/jquery-ui-1.8.15.min.js',
                            'visu/js/Rickshaw.js',
                            'visu/js/Rickshaw.Class.js',
                            'visu/js/Rickshaw.Compat.ClassList.js',
                            'visu/js/Rickshaw.Graph.js',
                            'visu/js/Rickshaw.Graph.Renderer.js',
                            'visu/js/Rickshaw.Graph.Renderer.Area.js',
                            'visu/js/Rickshaw.Graph.Renderer.Line.js',
                            'visu/js/Rickshaw.Graph.Renderer.Bar.js',
                            'visu/js/Rickshaw.Graph.Renderer.ScatterPlot.js',
                            'visu/js/Rickshaw.Graph.Renderer.Stack.js',
                            'visu/js/Rickshaw.Graph.RangeSlider.js',
                            'visu/js/Rickshaw.Graph.RangeSlider.Preview.js',
                            'visu/js/Rickshaw.Graph.HoverDetail.js',
                            'visu/js/Rickshaw.Graph.Annotate.js',
                            'visu/js/Rickshaw.Graph.Legend.js',
                            'visu/js/Rickshaw.Graph.Axis.Time.js',
                            'visu/js/Rickshaw.Graph.Behavior.Series.Toggle.js',
                            'visu/js/Rickshaw.Graph.Behavior.Series.Order.js',
                            'visu/js/Rickshaw.Graph.Behavior.Series.Highlight.js',
                            'visu/js/Rickshaw.Graph.Smoother.js',
                            'visu/js/Rickshaw.Fixtures.Time.js',
                            'visu/js/Rickshaw.Fixtures.Time.Local.js',
                            'visu/js/Rickshaw.Fixtures.Number.js',
                            'visu/js/Rickshaw.Fixtures.RandomData.js',
                            'visu/js/Rickshaw.Fixtures.Color.js',
                            'visu/js/Rickshaw.Color.Palette.js',
                            'visu/js/Rickshaw.Graph.Axis.Y.js',
                            'visu/js/extensions.js' ],
               'admin/login' => [ 'sha256.js' ],
             }

  # How to include each type of file in the HTML page
  INCLUDE_TAGS = [ ['css', '<link rel="stylesheet" href="%s" />'],
                 ['js', '<script src="%s"></script>'] ]

  @cache = {}
  @cache_lock = Mutex.new

  INTRUSIVE_VERSIONING = SETTINGS['features']['intrusive_versioning']

  # Adds the file's contents MD5 hash to its name.
  #
  # Intrusive versioning needs to be explicitely activated in the configuration
  # file because the reverse-proxy must be configured to rewrite the file names
  # without the MD5 hash in the middle.
  #
  def self.append_version(file)
    ef = file.start_with?('elfinder/') ? file : "public/#{file}"

    if File.exists?(ef)
      @cache_lock.synchronize do
        if (v = @cache[file]).nil? || v[0] != File.mtime(ef)
          md5 = Digest::MD5.file(ef).hexdigest
          name =
            if INTRUSIVE_VERSIONING
              part = file.rpartition('.')
              "#{part[0]}.#{md5}.#{part[2]}"
            else
              "#{file}?v=#{md5}"
            end
          v = [File.mtime(ef), name]
          @cache[file] = v
        end

        v[1]
      end
    else
      file
    end
  end

  print 'Pre-caching static assets... '
  [ GLOBAL_CSS + EXTRA_CSS.values + GLOBAL_JS + EXTRA_JS.values ].flatten.each { |f| append_version(f) }
  puts 'Done.'

end

helpers do
  # This helper function determines the name of the resource (the first part of the URL) then creates the include
  # tags for the relevant files which are defined in 3 categories:
  # - global, as defined in HeaderAssets
  # - extra, as defined in HeaderAssets (if ever)
  # - same name
  # The last category is files with the same name than the resource, which will automatically be included, whether they
  # are static or need to be generated.
  #
  def include_in_header(route)
    name =
      case
        when route == '/' then 'index'
        when route == '/admin' || route == '/admin/' then '/admin/index'
        else route[/^\/?(.+)$/, 1]
      end

    links = HeaderAssets::INCLUDE_TAGS.map do |ext, out|
      files = HeaderAssets.const_get("GLOBAL_#{ext.upcase}".to_sym).dup
      files << HeaderAssets.const_get("EXTRA_#{ext.upcase}".to_sym).send(:'[]', name)
      full_name = "#{name}.#{ext}"
      files << full_name if File.exists?("public/#{full_name}") || File.exists?("views/#{full_name}.erb")

      # remove any level of nesting and empty (nil) values
      files = files.flatten.compact

      # if we are in the admin pages, we need to look for the files on level up compared to the route's base
      files.map! { |f| "../#{f}" } if route.start_with?('/admin')

      # then append the versioning query string
      # and finally create the tag
      files.map { |f| HeaderAssets.append_version(f) }.map { |l| out % l }
    end

    links.flatten.join("\n")
  end
end
