#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

get '/login' do
  redirect '/' if session[:id]
  set_reuse()
  haml :login
end

post '/login' do
  halt if session[:id]

  conn = HTTPClient.new
  res = conn.post(MASHUP_SERVER + '/login', params)
  msg = JSON[res.content]

  if msg['status'] == 'ok'
    session[:id] = msg['id']
    session[:name] = msg['name']
    session[:conn] = conn

    res = conn.get(MASHUP_SERVER + '/member')
    session[:root_id] = JSON[res.content]['member']['home_directory_id']

    redirect '/'
  end

  error_msg = case res.status
    when 200 then msg['err_msg']
    else 'Unexpected error, please contact the administrator'
  end
  refill = ['username']
  session[:reuse] = [request.path_info, error_msg, params.select { |k,_| refill.include?(k) }]

  redirect '/login'
end

get '/logout' do
  reuse = session[:reuse]
  admin = session[:admin]
  session.clear
  session[:reuse] = reuse if reuse
  session[:admin] = admin if admin
  redirect '/login'
end

get '/register' do
  redirect '/' if session[:id]

  haml :register
end

post '/register' do
  halt if session[:id]

  res = HTTPClient.post(MASHUP_SERVER + '/register', params)
  check_http_code(res)
  msg = JSON.parse(res.content)

  if msg['status'] == 'err' && !msg['err_msg'].nil?
    err_msg = API_ERROR_CODES[msg['err_msg'].to_sym]
    msg['err_msg'] = err_msg unless err_msg.nil?
  end

  content_type :json
  JSON[msg]
end

get '/pending_registration/:pending_id' do
  redirect '/' if session[:id]

  haml :pending_registration
end
