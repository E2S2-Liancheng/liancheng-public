#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

get '/notifications' do
  res = session[:conn].get(MASHUP_SERVER + "/notif/list")
  check_http_code(res)
  msg = JSON.parse(res.content)

  @shares = msg['notifs'].select { |n| n['type'] == 'share' }

  haml :notifications
end

post '/notifs/share' do
  res = session[:conn].post(MASHUP_SERVER + "/share/invitation", params)
  check_http_code(res)

  content_type :json
  res.content
end
