#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

post '/workflow' do
  inode_id = params[:inode_id]
  revision = params[:revision]

  if inode_id
    session[:workflow] = { :inode_id => Integer(inode_id),
                           :revision => (revision ? Integer(revision) : -1) }
  else
    session[:workflow] = nil
    halt 400
  end

  redirect to('/workflow'), 303
end

get '/workflow' do
  if session[:workflow]
    @wf_inode_id = session[:workflow][:inode_id]
    @wf_revision = session[:workflow][:revision]

    res = session[:conn].get(MASHUP_SERVER + "/inode/#{@wf_inode_id}")
    check_http_code(res)
    msg = JSON.parse(res.content)

    @wf_revision = msg['inode']['revision']['revision'] if @wf_revision == -1
    @wf_name = msg['inode']['name']
  end

  haml :workflow
end

post '/workflow/new' do
  session[:workflow] = nil

  content_type :json
  JSON[ 'status' => 'ok' ]
end

post '/workflow/node/:node_name' do
  @template = params[:node_name]
  @id = params[:id]
  @load = params[:load] ? JSON.parse(params[:load]) : nil
  @data = @load ? @load['data'] : nil

  @title, @in, @out, @family = case @template
    when 'import_csv'            then ['Import CSV',    0, 1, nil]
    when 'import_netcdf'         then ['Import netCDF', 0, 1, nil]
    when 'operator_db'           then ['DB query',      1, 1, nil]
    when 'operator_optiemission' then ['Optiemission',  4, 1, 'optiliner']
    when 'operator_opticost'     then ['Opticost',      5, 1, 'optiliner']
    when 'operator_opticontract' then ['Opticontract',  6, 1, 'optiliner']
    when 'operator_projection'   then ['Projection',    1, 1, nil]
    when 'operator_filter'       then ['Filter',        1, 1, nil]
    when 'control_copy'
      n = @data ? @data['hooks'] : 2
                                      ['Copy',          1, n, nil]
    when 'operator_smooth'       then ['Smooth',        1, 1, nil]
    when 'operator_provconcn'    then ['Concentration', 4, 1, nil]
    when 'operator_provdiff'     then ['Diffusion',     1, 1, nil]
    when 'export_optikml'        then ['Optiliner KML', 1, 0, nil]
    when 'export_csv'            then ['Export CSV',    1, 0, nil]
  end

  haml :'_workflow_node', :layout => false
end

get '/workflow/load' do
  inode_id = Integer(params['inode_id'])
  revision = Integer(params['revision'])

  res = session[:conn].get(MASHUP_SERVER + "/inode/#{inode_id}/#{revision}/contents")
  check_http_code(res)

  content_type :json
  JSON[ 'status' => 'ok', 'file_load' => res.content.force_encoding('utf-8') ]          # HACK ? force UTF-8
end

post '/workflow/save' do
  inode_id = Integer(params['inode_id'])
  t = Tempfile.new('wf-')
  t.write(params['save_file'])
  t.rewind

  res = session[:conn].get(MASHUP_SERVER + "/inode/#{inode_id}")
  check_http_code(res)
  msg = JSON.parse(res.content)
  directory_id = msg['inode']['directory_id']
  name = msg['inode']['name']

  res = session[:conn].get(MASHUP_SERVER + "/directory/#{directory_id}/read_write")
  check_http_code(res)
  if JSON.parse(res.content)['read_write'] == false
    content_type :json
    return JSON[ 'status' => 'err', 'err_msg' => 'User cannot write in that directory' ]
  end

  form = [ ['directory_id', directory_id],
           ['upload[]', t],
           ['filename[]', name],
           ['mime_type[]', 'application/x-urbandb-workflow'] ]
  res = session[:conn].post(MASHUP_SERVER + "/inode", form)
  check_http_code(res)
  msg = JSON.parse(res.content)

  inode_id = msg['inodes'][0]['id']
  revision = msg['inodes'][0]['revision']['revision']

  session[:workflow] = { :inode_id => inode_id,
                         :revision => revision }
  content_type :json
  JSON[ 'status' => 'ok', 'inode_id' => inode_id, 'revision' => revision ]
end

post '/workflow/save_new' do
  directory_id = Integer(params['directory_id'])
  file_name = params['file_name']
  t = Tempfile.new('wf-')
  t.write(params['save_file'])
  t.rewind

  res = session[:conn].get(MASHUP_SERVER + "/directory/#{directory_id}/read_write")
  check_http_code(res)
  if JSON.parse(res.content)['read_write'] == false
    content_type :json
    return JSON[ 'status' => 'err', 'err_msg' => 'User cannot write in that directory' ]
  end

  form = [ ['directory_id', directory_id],
           ['upload[]', t],
           ['filename[]', file_name],
           ['mime_type[]', 'application/x-urbandb-workflow'] ]
  res = session[:conn].post(MASHUP_SERVER + "/inode", form)
  check_http_code(res)
  msg = JSON.parse(res.content)

  inode_id = msg['inodes'][0]['id']
  revision = msg['inodes'][0]['revision']['revision']

  session[:workflow] = { :inode_id => inode_id,
                         :revision => revision }
  content_type :json
  JSON[ 'status' => 'ok', 'inode_id' => inode_id, 'revision' => revision, 'name' => msg['inodes'][0]['name'] ]
end

post '/workflow/run' do
  res = session[:conn].post(MASHUP_SERVER + "/job", params)
  check_http_code(res)

  content_type :json
  res.content
end

get '/workflow/file_picker' do
  res = session[:conn].get(MASHUP_SERVER + "/directory/home")
  check_http_code(res)
  @root = JSON.parse(res.content)['directory']

  res = session[:conn].get(MASHUP_SERVER + "/share/list_accessible")
  check_http_code(res)
  @shares = JSON.parse(res.content)['shares']

  @shares.map! do |s|
    res = session[:conn].get(MASHUP_SERVER + "/directory/#{s}")
    check_http_code(res)
    JSON.parse(res.content)['directory']
  end

  @dirs = @root['children_ids'].map do |c|
    res = session[:conn].get(MASHUP_SERVER + "/directory/#{c}")
    check_http_code(res)
    JSON.parse(res.content)['directory']
  end

  res = session[:conn].get(MASHUP_SERVER + "/directory/#{@root['id']}/inodes")
  check_http_code(res)
  @files = JSON.parse(res.content)['inodes'].reject { |f| f['deleted'] }

  @save_mode = params.has_key?('save')

  haml :'_file_picker', :layout => false
end

get %r{^/workflow/file_picker/contents/(\d+)$} do
  directory_id = params['captures'][0].to_i

  res = session[:conn].get(MASHUP_SERVER + "/directory/#{directory_id}")
  check_http_code(res)
  @directory = JSON.parse(res.content)['directory']

  @dirs = @directory['children_ids'].map do |c|
    res = session[:conn].get(MASHUP_SERVER + "/directory/#{c}")
    check_http_code(res)
    JSON.parse(res.content)['directory']
  end

  res = session[:conn].get(MASHUP_SERVER + "/directory/#{directory_id}/root_for_member")
  check_http_code(res)
  root_id = JSON.parse(res.content)['root_id'].to_i
  @dirs.unshift({ 'id' => @directory['parent_id'], 'name' => '↸ parent folder' }) if root_id != directory_id

  res = session[:conn].get(MASHUP_SERVER + "/directory/#{directory_id}/inodes")
  check_http_code(res)
  @files = JSON.parse(res.content)['inodes'].reject { |f| f['deleted'] }

  haml :'_file_picker_contents', :layout => false
end

get '/workflow/import_csv/:inode_id/:revision' do
  res = session[:conn].get(MASHUP_SERVER + "/inode/#{params['inode_id']}/#{params['revision']}/import_csv")
  check_http_code(res)

  content_type :json
  res.content
end

post '/workflow/import_csv/cols' do
  @values = params['original'].zip(params['user_choice'])

  haml :'_workflow_import_csv_cols', :layout => false
end

post '/workflow/operator_projection/cols' do
  @values = JSON.parse(params['cols'])

  haml :'_workflow_operator_projection_cols', :layout => false
end

post '/workflow/operator_filter/clause' do
  @cols = JSON.parse(params['cols'])

  haml :'_workflow_operator_filter_clause', :layout => false
end
