#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

module Obfuscated
  UUID_RE = /[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}/
end


get %r{^/object/(#{Obfuscated::UUID_RE})$} do
  uuid = params[:captures][0]

  res = HTTPClient.new.get(MASHUP_SERVER + "/publish/obfuscated/#{uuid}/name")
  halt 404 if res.code == 404
  check_http_code(res)
  name = JSON.parse(res.content)['name']

  res = HTTPClient.new.get(MASHUP_SERVER + "/publish/obfuscated/#{uuid}")
  check_http_code(res)

  temp_file = Tempfile.new('dl-')
  temp_file.write(res.content)
  temp_file.close

  send_file(temp_file.path, :filename => name, :disposition => 'attachment', :type => res.content_type)
end
