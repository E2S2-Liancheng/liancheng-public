#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
helpers do
  def space_large(n)
    n.to_s.reverse.scan(/.{1,3}/).join(' ').reverse
  end

  UNITS = [ '', 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y', '?' ]

  def human_large(n)
    i = 0
    m = 0
    while n >= 1000
      i += 1
      n, m = n.divmod(1000)
    end

    i = UNITS.length - 1 if i >= UNITS.length

    v = ("%d.%03d" % [n, m])[0,4].gsub(/0+$/, '').gsub(/\.$/, '')
    "#{v} #{UNITS[i]}B"
  end
end

get '/admin/login' do
  redirect '/' if session[:admin]
  set_reuse()
  haml :'admin/login', :layout => :'admin/layout'
end

post '/admin/login' do
  halt if session[:admin]

  conn = HTTPClient.new
  res = conn.post(MASHUP_SERVER + '/admin/login', params)

  if (status = JSON[res.content]['status']) == 'ok'
    session[:admin] = { :name => params[:username],
                        :conn => conn }
    redirect '/admin'
  end

  error_msg = case status
    when 'err' then 'Wrong username or password'
    else 'Unexpected error'
  end
  refill = ['username']
  session[:reuse] = [request.path_info, error_msg, params.select { |k,_| refill.include?(k) }]

  redirect '/admin/login'
end

get '/admin/logout' do
  reuse = session[:reuse]
  session.clear
  session[:reuse] = reuse if reuse
  redirect '/admin/login'
end

def check_admin()
  redirect '/admin/login' unless session[:admin]
end

def check_admin_response_code(res)
  if res.status != 200
    session[:reuse] = ['/admin/login', 'Server oops!', { :username => session[:admin][:name] }]
    redirect '/admin/logout'
  end
end

def check_admin_ajax_response_code(res)
  if res.status == 401 || res.status == 404
    halt 500
  end
end

get '/admin' do
  check_admin()

  res = session[:admin][:conn].get(MASHUP_SERVER + '/admin/stats')
  check_admin_response_code(res)

  @stats = JSON[res.content, :symbolize_names => true][:stats]

  res = session[:admin][:conn].get(MASHUP_SERVER + '/register/pending')
  check_admin_response_code(res)

  @pendings = JSON[res.content]['pendings']

  haml :'admin/index', :layout => :'admin/layout'
end

post '/admin/validate_pending' do
  check_admin()

  res = session[:admin][:conn].post(MASHUP_SERVER + '/register/validate', params)
  check_admin_ajax_response_code(res)

  begin
     raise if JSON[res.content]['status'] != 'ok'
  rescue
    halt 500
  end

  res.content
end

post '/admin/reject_pending' do
  check_admin()

  res = session[:admin][:conn].post(MASHUP_SERVER + '/register/reject', params)
  check_admin_ajax_response_code(res)

  begin
     raise if JSON[res.content]['status'] != 'ok'
  rescue
    halt 500
  end

  res.content
end
