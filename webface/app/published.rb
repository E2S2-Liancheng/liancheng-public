#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


# GET /~:username/:path
#
get %r{^/~([-.\w]{2,32})/(.*)$} do
  username = params[:captures][0]
  path = params[:captures][1]

  res = HTTPClient.new.get(MASHUP_SERVER + "/publish/home/info", { :username => username, :path => path })
  halt 404 if res.code == 404
  check_http_code(res)
  info = JSON.parse(res.content)['info']

  res = HTTPClient.new.get(MASHUP_SERVER + "/publish/home/#{info['member_id']}/#{info['inode_id']}")
  check_http_code(res)

  temp_file = Tempfile.new('dl-')
  temp_file.write(res.content)
  temp_file.close

  name = info['name']
  disposition = name.end_with?('.html') ? 'inline' : 'attachment'

  send_file(temp_file.path, :filename => name, :disposition => disposition, :type => res.content_type)
end
