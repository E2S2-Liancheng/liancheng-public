#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
get '/jobs' do
  res = session[:conn].get(MASHUP_SERVER + "/job/list")
  check_http_code(res)

  @jobs = JSON.parse(res.content)['jobs'].sort { |a,b| b['id'].to_i <=> a['id'].to_i }

  # TODO case for multiple output files
  @jobs.each do |j|
    if !(o = j['output']).nil? && o.size == 1
      j['direct'] = "/direct/#{o[0]['inode_id']}/#{o[0]['revision']}"
    end
  end

  haml :jobs
end
