#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'securerandom'

post '/elfinder/ea_window' do
  inode_id = params[:file_id]

  res = session[:conn].get(MASHUP_SERVER + "/publish/#{inode_id}")
  check_http_code(res)
  @msg = json_sym(res.content)[:publish]

  @escaped_url = "/~#{@msg[:username]}/#{@msg[:inode][:id]}/#{Rack::Utils.escape_html(@msg[:inode][:name])}"

  haml :'_explorer_external_access', :layout => false
end

post '/elfinder/ea_window/obfuscated/generate' do
  inode_id = params[:file_id]
  annotation = params[:annotation]

  res = session[:conn].post(MASHUP_SERVER + "/publish/#{inode_id}/obfuscated", { :annotation => annotation })
  check_http_code(res)
  @msg = { :obfuscated => [ json_sym(res.content)[:obfuscated] ] }

  html = haml :'_explorer_external_access_rows', :layout => false

  content_type :json
  JSON[ :status => 'ok', :html => html ]
end

post '/elfinder/ea_window/obfuscated/delete' do
  inode_id = params[:file_id]
  id = params[:id]

  res = session[:conn].delete(MASHUP_SERVER + "/publish/obfuscated/#{id}")
  check_http_code(res)

  content_type :json
  JSON[ :status => 'ok' ]
end

post %r{^/published/(\d+)/publish$} do
  inode_id = params[:captures][0]

  res = session[:conn].put(MASHUP_SERVER + "/publish/home/#{inode_id}")
  check_http_code(res)

  content_type :json
  JSON[ :status => 'ok' ]
end

post %r{^/published/(\d+)/unpublish$} do
  inode_id = params[:captures][0]

  res = session[:conn].delete(MASHUP_SERVER + "/publish/home/#{inode_id}")
  check_http_code(res)

  content_type :json
  JSON[ :status => 'ok' ]
end
