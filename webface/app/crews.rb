#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

get '/groups' do
  res = session[:conn].get(MASHUP_SERVER + "/member/crews")
  check_http_code(res)

  res2 = session[:conn].get(MASHUP_SERVER + "/member/list")
  check_http_code(res2)

  @crews = JSON[res.content]['crews']
  @members = JSON[res2.content]['members']

  haml :crews
end

get '/crews/:crew_id' do
  res = session[:conn].get(MASHUP_SERVER + "/crew/#{params['crew_id']}/members")
  check_http_code(res)

  @members = JSON[res.content]['members']

  haml :'_crews_members', :layout => false
end

post '/crews/create' do
  forward = { 'name' => params['name'] }
  forward['members[]'] = params['members'] if params['members']

  res = session[:conn].post(MASHUP_SERVER + "/crew", forward)
  check_http_code(res)

  msg = JSON[res.content]

  content_type :json
  return res.content unless msg['status'] == 'ok'

  @crews = [msg['crew']]
  ret = haml :'_crews_crews', :layout => false
  JSON[ 'status' => 'ok', 'html' => ret ]
end

post %r{^/crews/prepare_update/(\d+)$} do
  crew_id = params[:captures][0].to_i
  forward = {}
  forward['members[]'] = params['members'] if params['members']

  res = session[:conn].post(MASHUP_SERVER + "/crew/#{crew_id}/prepare_update", forward)
  check_http_code(res)
  msg = JSON[res.content]

  @members = msg['impacted_members']
  @name = esc_html(params[:name])

  ret = if @members
    @added = @members['added']
    @removed = @members['removed']
    { :status => 'confirm', :confirm => haml(:'_crews_confirm_update', :layout => false) }
  else
    { :status => 'ok' }
  end

  content_type :json
  JSON[ret]
end

post '/crews/update/:crew_id' do
  forward = { 'name' => params['name'] }
  forward['members[]'] = params['members'] if params['members']
  forward['give_copy'] = true if !params[:give_copy].nil?

  res = session[:conn].put(MASHUP_SERVER + "/crew/#{params['crew_id']}", forward)
  check_http_code(res)

  content_type :json
  res.content
end

post %r{^/crews/prepare_delete/(\d+)$} do
  crew_id = params[:captures][0].to_i

  res = session[:conn].post(MASHUP_SERVER + "/crew/#{crew_id}/prepare_delete")
  check_http_code(res)
  msg = JSON[res.content]

  ret = if msg['status'] == 'err'
    msg
  else
    if (@members = msg['impacted_members'])
      @name = esc_html(msg['name'])
      { :status => 'confirm', :confirm => haml(:'_crews_confirm_delete', :layout => false) }
    else
      { :status => 'ok' }
    end
  end

  content_type :json
  JSON[ret]
end

post %r{^/crews/delete/(\d+)$} do
  crew_id = params[:captures][0].to_i

  forward = {}
  forward['give_copy'] = true unless params[:give_copy].nil?

  res = session[:conn].delete(MASHUP_SERVER + "/crew/#{crew_id}", forward)
  check_http_code(res)

  content_type :json
  res.content
end
