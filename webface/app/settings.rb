#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

get '/settings' do
  res = session[:conn].get(MASHUP_SERVER + '/member')
  check_http_code(res)

  @member = JSON[res.content]['member']

  haml :settings
end

post '/settings/name' do
  res = session[:conn].put(MASHUP_SERVER + '/member/name', params)
  check_http_code(res)
  msg = JSON[res.content]

  session[:name] = msg['name'] if msg['status'] == 'ok'
  if msg['status'] == 'err' && msg['err_msg']
    complete_error = API_ERROR_CODES[msg['err_msg'].to_sym]
    msg['err_msg'] = complete_error unless complete_error.nil?
  end

  content_type :json
  JSON[msg]
end

post '/settings/change_password' do
  res = session[:conn].put(MASHUP_SERVER + '/member/password', params)
  check_http_code(res)
  msg = JSON[res.content]

  if msg['status'] == 'err' && msg['err_msg']
    complete_error = API_ERROR_CODES[msg['err_msg'].to_sym]
    msg['err_msg'] = complete_error unless complete_error.nil?
  end

  content_type :json
  JSON[msg]
end

get '/settings/username/check' do
  username = params[:username]

  res = session[:conn].get(MASHUP_SERVER + "/member/username/check", { :username => username })

  check_http_code(res)
  msg = JSON[res.content]

  if msg['status'] == 'err' && msg['err_msg']
    complete_error = API_ERROR_CODES[msg['err_msg'].to_sym]
    msg['err_msg'] = complete_error unless complete_error.nil?
  end

  content_type :json
  JSON[msg]
end

post '/settings/username' do
  username = params[:username]

  res = session[:conn].put(MASHUP_SERVER + "/member/username", { :username => username })

  check_http_code(res)
  msg = JSON[res.content]

  if msg['status'] == 'err' && msg['err_msg']
    complete_error = API_ERROR_CODES[msg['err_msg'].to_sym]
    msg['err_msg'] = complete_error unless complete_error.nil?
  end

  content_type :json
  JSON[msg]
end
