#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# GET /streams/accounts
#
get '/streams/accounts' do
  res = session[:conn].get(MASHUP_SERVER + "/stream/list")
  check_http_code(res)
  @msg = JSON.parse(res.content)

  haml :streams_accounts
end

# GET /streams/records
#
get '/streams/records' do
  res = session[:conn].get(MASHUP_SERVER + "/stream/list")
  check_http_code(res)
  @msg = JSON.parse(res.content)

  # TODO integrate active support
  js_data = @msg.respond_to?(:deep_copy) ? @msg.deep_copy : JSON.parse(res.content)
  js_data['hobo'].each { |a| a['devices'].keep_if { |d| d['file_name'].nil? } }.delete_if { |a| a['devices'].empty? }
  @js_data = JSON[js_data]

  haml :streams_records
end

# GET /streams/hobo
#
get '/streams/hobo' do
  h = { 'login' => '',
        'devices' => [] }
  haml :'_streams_hobo', { :layout => false, :locals => { :h => h } }
end

# POST /streams/hobo
#
# WARNING do not log password
#
post '/streams/hobo' do
  old_login = params['old_login']
  new_login = params['new_login']
  password = params['password']
  devices = params['devices']

  method = (old_login.nil?) ? 'post' : 'put'
  form = { 'old_login' => old_login,
           'new_login' => new_login,
           'password' => password,
           'devices[]' => devices }.delete_if { |_,v| v.nil? }
  res = session[:conn].request(method, MASHUP_SERVER + "/stream/hobo", nil, form)
  check_http_code(res)

  status 200
end

# POST /streams/hobo
# Deletes an account information.
#
post '/streams/hobo/delete' do
  res = session[:conn].delete(MASHUP_SERVER + "/stream/hobo", { 'login' => params['login'] })
  check_http_code(res)

  status 200
end

# POST /streams/hobo/record
# Adds a record
#
post '/streams/hobo/record' do
  form = { 'login' => params['login'],
           'device_id' => params['device_id'],
           'directory_id' => params['directory_id'],
           'file_name' => params['file_name'] }

  res = session[:conn].post(MASHUP_SERVER + "/stream/hobo/record", form)
  check_http_code(res)

  locals = { 'login' => form['login'], 'device' => form }
  haml :'_streams_records_hobo_device', { :layout => false, :locals => locals }
end

# POST /streams/hobo/record/delete
# Deletes a record
#
post '/streams/hobo/record/delete' do
  form = { 'login' => params['login'],
           'device_id' => params['device_id'] }

  res = session[:conn].delete(MASHUP_SERVER + "/stream/hobo/record", form)
  check_http_code(res)

  status 200
end
