#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

get '/visu' do
  session[:visu] ||= { :inode_id => '', :revision => ''}
  @inode = session[:visu][:inode_id]
  @revision = session[:visu][:revision]

  haml :visu
end

post '/visu/csv' do
  inode_id = params[:inode_id]
  revision = params[:revision]

  res = session[:conn].get(MASHUP_SERVER + "/inode/#{inode_id}/#{revision}/display_csv")
  check_http_code(res)

  content_type :json
  res.content
end

post '/visu' do
  session[:visu] = { :inode_id => params[:inode_id], :revision => params[:revision] }

  redirect to('/visu'), 303
end
