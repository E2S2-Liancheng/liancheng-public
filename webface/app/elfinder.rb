#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'set'
require 'tempfile'

module EfConnector
  class HttpErrorCode < StandardError
    def initialize(http_code)
      @http_code = http_code
    end

    attr_reader :http_code
  end
end

helpers do

  def efc_check_http_code(res)
    raise EfConnector::HttpErrorCode.new(res.status) if res.status != 200
  end

  def efc_parse_check_message(res, ignore_status = false)
    efc_check_http_code(res)

    begin
      JSON.parse(res.content).tap do |msg|
        throw :elfinder_error, { 'error' => 'errResponse' } if msg['status'].nil?
        if !ignore_status && msg['status'] == 'err'
          err_msg = API_ERROR_CODES[msg['err_msg'].to_sym] || msg['err_msg']
          throw :elfinder_error, { 'error' => err_msg }
        end
      end
    rescue => e
      $logger.error(e.msg_bt)
      throw :elfinder_error, { 'error' => 'errResponse' }
    end
  end

  def efc_parse_check_paste_message(res)
    efc_check_http_code(res)

    begin
      JSON.parse(res.content).tap do |msg|
        throw :elfinder_error, { 'error' => 'errResponse' } if msg['status'].nil?
        if msg['status'] == 'err' && msg['status'] != 'err_dir_paste_conflict'
          err_msg = API_ERROR_CODES[msg['err_msg'].to_sym] || msg['err_msg']
          throw :elfinder_error, { 'error' => err_msg }
        end
      end
    rescue => e
      $logger.error(e.msg_bt)
      throw :elfinder_error, { 'error' => 'errResponse' }
    end
  end

  COMMANDS = %w(open tree parents mkdir upload file rename new_workflow paste rm).to_set

  def efc_open()
    if params['init'] == '1' && params['target'].empty?
      params['target'] = "h#{session[:root_id]}_#{session[:root_id]}"
    end
    volume_id, directory_id = params['target'].split('_')

    # get current working directory and its ancestry
    files = efc_get_ancestry(directory_id, volume_id)
    cwd = files.find { |f| f['hash'] == params['target'] }

    # get current working directory's files
    res = session[:conn].get(MASHUP_SERVER + '/directory/' + directory_id + '/inodes')
    msg = efc_parse_check_message(res)

    files += msg['inodes'].map { |f| efc_msg_to_inode(f, volume_id) }

    # eventually add the root folder in front if the current working directory is in a shared folder
    files = efc_get_dir(session[:root_id], "h#{session[:root_id]}", 0) + files \
                                                                                if "h#{session[:root_id]}" != volume_id
    # get shared directories
    res = session[:conn].get(MASHUP_SERVER + '/share/list_accessible')
    msg = efc_parse_check_message(res)['shares']

    shared = msg.map { |d2| ("h#{d2}_#{d2}" != files[0]['hash']) ? efc_get_dir(d2, "h#{d2}", 0) : nil }
    files += shared.compact.flatten

    { 'cwd' => cwd, 'files' => files }.tap { |h| h['api'] = 2 if params['init'] == '1' }
  end

  def efc_tree()
    volume_id, directory_id = params['target'].split('_')

    tree = efc_get_dir(directory_id, volume_id, 1)
    tree.shift                                                  # remove cwd

    { 'tree' => tree }
  end

  def efc_parents()
    volume_id, directory_id = params['target'].split('_')

    parents = efc_get_ancestry(directory_id, volume_id)

    # eventually add the root folder in front if we are looking for the parents of a shared folder
    parents = efc_get_dir(session[:root_id], "h#{session[:root_id]}", 1) + parents \
                                                  if parents[0]['hash'] != "h#{session[:root_id]}_#{session[:root_id]}"

    # eventually add the shared folders
    res = session[:conn].get(MASHUP_SERVER + '/share/list_accessible')
    msg = efc_parse_check_message(res)['shares']
    shared = msg.map { |d2| ("h#{d2}_#{d2}" != parents[0]['hash']) ? efc_get_dir(d2, "h#{d2}", 0) : nil }
    parents += shared.compact.flatten

    { 'tree' => parents }
  end

  def efc_mkdir()
    volume_id, directory_id = params['target'].split('_')

    form = { 'parent_id' => directory_id, 'name' => params['name'] }
    res = session[:conn].post(MASHUP_SERVER + '/directory', form)
    msg = efc_parse_check_message(res)

    { 'added' => [efc_msg_to_dir(msg['directory'], volume_id)] }
  end

  def efc_upload()
    volume_id, directory_id = params['target'].split('_')

    res = session[:conn].get(MASHUP_SERVER + "/directory/#{directory_id}/read_write")
    msg = efc_parse_check_message(res)
    throw :elfinder_error, { 'error' => 'errAccess' } if msg['read_write'] == false

    form = [ ['directory_id', directory_id ] ]
    form += params['upload'].map { |f| ['upload[]', f[:tempfile]] }
    form += params['upload'].map { |f| ['filename[]', f[:filename]] }
    form += params['upload'].map { |f| ['mime_type[]', f[:type]] }
    res = session[:conn].post(MASHUP_SERVER + "/inode", form)
    msg = efc_parse_check_message(res)

    files = msg['inodes'].map { |f| efc_msg_to_inode(f, volume_id) }

    { 'added' => files }
  end

  def efc_file()
    inode_id = params['target'].split('_')[2]

    res = session[:conn].get(MASHUP_SERVER + "/inode/#{inode_id}")
    msg = efc_parse_check_message(res)

    name = msg['inode']['name']

    res = session[:conn].get(MASHUP_SERVER + "/inode/#{inode_id}/-1/contents")
    efc_check_http_code(res)

    temp_file = Tempfile.new('dl-')
    temp_file.write(res.content)
    temp_file.rewind

    { 'file' => temp_file, 'name' => name, 'mime_type' => res.content_type }
  end

  def efc_rename()
    volume_id, directory_id, inode_id = params['target'].split('_')

    url_path = (inode_id.nil?) ? "/directory/#{directory_id}" : "/inode/#{inode_id}"
    res = session[:conn].put(MASHUP_SERVER + "#{url_path}/name", { 'name' => params['name'] })
    msg = efc_parse_check_message(res)

    ret = (inode_id.nil?) ? efc_msg_to_dir(msg['directory'], volume_id) : efc_msg_to_inode(msg['inode'], volume_id)
    { 'added' => [ret], 'removed' => [] }
  end

  def efc_new_workflow()
    t = Tempfile.new('wf')
    params['upload'] = [{ :tempfile => t,
                          :filename => params['name'],
                          :type => 'application/x-urbandb-workflow' }]
    efc_upload()
  end

  def efc_paste()
    params['cut'] == '1' ? efc_cut_paste() : efc_copy_paste()
  end

  def efc_cut_paste()
    src_vol, src = params['src'].split('_')
    dst_vol, dst = params['dst'].split('_')

    targets = params['targets'].map { |t| t.split('_'); }
    dirs, files = targets.partition { |t| t.size == 2 }.map { |p| p.map { |t| t[-1] } }

    form = { 'src' => src, 'dst' => dst, 'dirs[]' => dirs, 'inodes[]' => files }

    res = session[:conn].post(MASHUP_SERVER + "/ops/cut_paste", form)
    msg = efc_parse_check_message(res, true)

    return efc_paste_errors(msg['info']) if msg['status'] == 'err'

    added = msg['created'].map { |o| efc_msg_to_hash(o, dst_vol) }
    removed = msg['deleted'].map { |o| [src_vol, o['dir_id'], o['inode_id']].compact.join('_') }

    { :added => added, :removed => removed }
  end

  def efc_copy_paste()
    src = params['src'].split('_')[1]
    volume_id, dst = params['dst'].split('_')

    targets = params['targets'].map { |t| t.split('_'); }
    dirs, files = targets.partition { |t| t.size == 2 }.map { |p| p.map { |t| t[-1] } }

    form = { 'src' => src, 'dst' => dst, 'dirs[]' => dirs, 'inodes[]' => files }

    res = session[:conn].post(MASHUP_SERVER + "/ops/copy_paste", form)
    msg = efc_parse_check_message(res, true)

    return efc_paste_errors(msg['info']) if msg['status'] == 'err'

    added = msg['pasted'].map { |o| efc_msg_to_hash(o, volume_id) }

    { :added => added, :removed => [] }
  end

  def efc_paste_errors(errors)
    $logger.info("Session ID #{session[:id]}: #{errors}")
    e = errors[0]['type']
    throw :elfinder_error, { :error => API_ERROR_CODES[errors[0]['type'].to_sym] }

    # TODO return a proper error message
    result = []
    clashes = errors.select { |e| e['type'] = 'err_paste_name_clash' }.map { |e| e.delete('type'); e }
    result << { :msg => API_ERROR_CODES[:err_paste_name_clash], :list => clashes } unless clashes.empty?

    { :added => [], :removed => [], :errors => result }
  end

  def efc_rm()
    dirs, files = params['targets'].map { |t| t.split('_') }.partition { |t| t.size == 2 }

    [dirs, files].each { |s| s.map! { |d| d[-1] } }

    res = session[:conn].post(MASHUP_SERVER + "/ops/delete", { :'dirs[]' => dirs, :'inodes[]' => files })
    msg = efc_parse_check_message(res, true)

    return { :removed => [], :errors => msg['err_msg'] } if msg['status'] == 'err'

    src_volume_id = params['targets'][0].split('_')[0]
    removed = msg['deleted'].map do |o|
      if o['inode_id'].nil?
        "#{src_volume_id}_#{o['dir_id'].to_s}"
      else
        "#{src_volume_id}_#{o['dir_id'].to_s}_#{o['inode_id'].to_s}"
      end
    end

    errors = msg['errors'].map do |o|
      hash = if o['inode_id'].nil?
        "#{src_volume_id}_#{o['dir_id'].to_s}"
      else
        "#{src_volume_id}_#{o['dir_id'].to_s}_#{o['inode_id'].to_s}"
      end
      err = API_ERROR_CODES[o['msg'].to_sym] || o['msg']
      { :hash => hash, :msg => err }
    end

    { :removed => removed, :errors => errors }
  end

  # Given a directory object sent by the back-end, convert it to an elfinder hash.
  #
  def efc_msg_to_dir(directory, volume_id)
    { 'name' => directory['name'],
      'hash' => "#{volume_id}_#{directory['id']}",
      'mime' => 'directory',
      'ts' => directory['modified_at'],
      'dirs' => directory['children_ids'].empty? ? 0 : 1,
      'read' => 1,
      'write' => 1,
      'shared' => directory['shared'],
      'owner_name' => directory['owner_name'],
      'can_be_shared' => directory['can_be_shared'] }
    .tap do |h|
      if directory['parent_id'].nil?
        h['volume_id'] = "#{volume_id}_"
      else
        h['phash'] = "#{volume_id}_#{directory['parent_id']}"
      end
    end
  end

  # Given an inode object sent by the back-end, convert it to an elfinder hash.
  #
  def efc_msg_to_inode(inode, volume_id)
    phash = "#{volume_id}_#{inode['directory_id']}"

    { 'name' => inode['name'],
      'hash' => "#{phash}_#{inode['id']}",
      'phash' => phash,
      'revision' => inode['revision']['revision'],
      'mime' => inode['revision']['mime_type'],
      'ts' => inode['revision']['modified_at'],
      'size' => inode['revision']['size'],
      'owner_name' => inode['revision']['owner_name'],
      'read' => 1,
      'write' => 1 }
  end

  # Given an object sent by the back-end, convert it to an elfinder hash.
  #
  def efc_msg_to_hash(object, volume_id)
    (object['revision'].nil?) ? efc_msg_to_dir(object, volume_id) : efc_msg_to_inode(object, volume_id)
  end

  # Returns an array of the directory itself and its eventual descendants up to a given depth.
  # The directory itself is always in first position.
  #
  def efc_get_dir(directory_id, volume_id, depth)
    res = session[:conn].get(MASHUP_SERVER + "/directory/#{directory_id}")
    msg = efc_parse_check_message(res)

    directory = msg['directory']
    d = efc_msg_to_dir(directory, volume_id)

    if depth > 0 && !d.nil?
      c = directory['children_ids'].map { |c| efc_get_dir(c, volume_id, depth -1) }.flatten.compact
      d['dirs'] = c.empty? ? 0 : 1
      c.unshift(d)
      c
    else
      [d].compact
    end
  end

  # Retrieve an array of all the ancestors and their children (target is also considered an ancestor).
  #
  def efc_get_ancestry(directory_id, volume_id)
    dirs = efc_get_dir(directory_id, volume_id, 1)

    if (parent_dir = dirs[0]['phash'])
      dirs.shift
      efc_get_ancestry(parent_dir[/.*_(\d+)/, 1], volume_id) + dirs
    else
      dirs
    end
  end

end


get '/elfinder' do
  haml :elfinder
end

# hack for GET only crap
get '/elfinder/connector' do
  halt 500 unless params['cmd'] == 'file' && params['download'] == '1'

  begin
    res = efc_file()
  rescue EfConnector::HttpErrorCode => e
    $logger.error(e.msg_bt)
    session.clear if e.http_code == 401
    halt 500
  end

  send_file(res['file'].path, :filename => res['name'], :disposition => 'attachment', :type => res['mime_type'])
end

post '/elfinder/connector' do
  $logger.info("/elfinder/connector, ##{session[:id]}, params:  #{params.inspect}")

  res = if COMMANDS.include?(params['cmd'])
    begin
      catch :elfinder_error do
        send("efc_#{params['cmd']}".to_sym)
      end
    rescue EfConnector::HttpErrorCode => e
      $logger.error(e.msg_bt)
      session.clear if e.http_code == 401
      halt 500
    end
  else
    { 'error' => 'errUnknownCmd' }
  end

  $logger.info("/elfinder/connector, ##{session[:id]}, result:  #{res.inspect}")

  if params['cmd'] == 'file'
    session[:tmp_file] = res['file']
    disposition = (params[:download] == '1') ? 'attachment' : 'inline'
    if params[:download] == '1'
      send_file(res['file'].path, :filename => res['name'], :disposition => disposition, :type => res['mime_type'])
    else
      res['file'].read
    end
  else
    content_type :json
    JSON[res]
  end
end

get '/elfinder/*.*' do
  file = [['elfinder'], params['splat'].join('.')].join('/')

  halt 404 unless File.exists?(file)

  send_file file
end

post '/elfinder/ac_window/show' do
  res = session[:conn].get(MASHUP_SERVER + '/directory/' + params[:directory_id])
  check_http_code(res)

  msg = JSON[res.content]['directory']
  @dir_name = msg['name']
  shared = msg['shared']

  # list of members
  res = session[:conn].get(MASHUP_SERVER + '/member/list')
  check_http_code(res)

  msg = JSON[res.content]
  @members = msg['members']
  @members.reject! { |m| m['id'] == session['id'] }

  # list of crews
  res = session[:conn].get(MASHUP_SERVER + '/member/crews')
  check_http_code(res)

  msg = JSON[res.content]
  @crews = msg['crews']

  # current share status
  if shared
    res = session[:conn].get(MASHUP_SERVER + "/share/#{params[:directory_id]}")
    check_http_code(res)

    msg = JSON[res.content]['share']
    ac_members = msg['member_ids'].to_set
    @members.each { |m| m['checked'] = 'checked' if ac_members.include?(m['id']) }
    ac_crews = msg['crew_ids'].to_set
    @crews.each { |m| m['checked'] = 'checked' if ac_crews.include?(m['id']) }
  end

  haml :'_explorer_share', :layout => false
end

post '/elfinder/ac_window/validate' do
  forward = {}
  forward['member_ids[]'] = params['member_ids'] if params['member_ids']
  forward['crew_ids[]'] = params['crew_ids'] if params['crew_ids']
  forward['read_write'] = 'true'                # WARNING remove to get back access control

  res = session[:conn].post(MASHUP_SERVER + "/share/#{params['directory_id']}", forward)
  check_http_code(res)
  msg = JSON.parse(res.content)

  if msg['status'] == 'ok'
    if params['member_ids']
      members = params['member_ids'].map do |member_id|
        res = session[:conn].get(MASHUP_SERVER + "/member/#{member_id}")
        check_http_code(res)
        JSON.parse(res.content)['member']
      end
      msg['members_html'] = haml :'_explorer_share_item', :layout => false, :locals => { :values => members, :item_type => 'member', :item_status => 'invited' }
    end

    if params['crew_ids']
      crews = (params['crew_ids'] || []).map do |crew_id|
        res = session[:conn].get(MASHUP_SERVER + "/crew/#{crew_id}")
        check_http_code(res)
        JSON.parse(res.content)['crew']
      end
      msg['crews_html'] = haml :'_explorer_share_item', :layout => false, :locals => { :values => crews, :item_type => 'crew', :item_status => 'invited' }
    end
  else
    full_text = API_ERROR_CODES[msg['err_msg'].to_sym]
    msg['err_msg'] = full_text unless full_text.nil?
  end

  content_type :json
  JSON[msg]
end

post '/elfinder/ac_window/prepare_remove_member' do
  directory_id = Integer(params["directory_id"])
  member_id = Integer(params["member_id"])

  res = session[:conn].get(MASHUP_SERVER + "/share/#{directory_id}/crews_for_member/#{member_id}")
  check_http_code(res)
  msg = JSON.parse(res.content)
  @crews = msg['crews']
  @name = esc_html(msg['member_name'])

  haml :'_explorer_confirm_remove_member', :layout => false
end

post '/elfinder/ac_window/remove_member' do
  directory_id = Integer(params[:directory_id])
  member_id = Integer(params[:member_id])

  form = {}
  form[:give_copy] = true unless params[:give_copy].nil?

  res = session[:conn].delete(MASHUP_SERVER + "/share/#{directory_id}/member/#{member_id}", form)
  check_http_code(res)
  msg = JSON.parse(res.content)

  res = session[:conn].get(MASHUP_SERVER + "/member/#{member_id}")
  check_http_code(res)
  item = JSON.parse(res.content)

  msg['item_html'] = haml :'_explorer_share_item', :layout => false, :locals => { :values => [item['member']], :item_status => 'available', :item_type => 'member' }

  content_type :json
  JSON[msg]
end

post '/elfinder/ac_window/prepare_remove_crew' do
  directory_id = Integer(params["directory_id"])
  crew_id = Integer(params["crew_id"])

  res = session[:conn].get(MASHUP_SERVER + "/share/#{directory_id}/members_for_crew/#{crew_id}")
  check_http_code(res)
  msg = JSON.parse(res.content)
  @members_kept, @members_lost = msg['members'].partition { |m| m['kept'] }
  @name = esc_html(msg['crew_name'])

  haml :'_explorer_confirm_remove_crew', :layout => false
end

post '/elfinder/ac_window/remove_crew' do
  directory_id = Integer(params["directory_id"])
  crew_id = Integer(params["crew_id"])

  form = {}
  form[:give_copy] = true unless params[:give_copy].nil?

  res = session[:conn].delete(MASHUP_SERVER + "/share/#{directory_id}/crew/#{crew_id}", form)
  check_http_code(res)
  msg = JSON.parse(res.content)

  res = session[:conn].get(MASHUP_SERVER + "/crew/#{crew_id}")
  check_http_code(res)
  item = JSON.parse(res.content)

  msg['item_html'] = haml :'_explorer_share_item', :layout => false, :locals => { :values => [item['crew']], :item_status => 'available', :item_type => 'crew' }

  content_type :json
  JSON[msg]
end

post '/elfinder/ac_window/items' do
  vars = { :values => JSON.parse(params['items']),
           :item_type => params['type'],
           :item_status => params['status'] }

  haml :'_explorer_share_item', :layout => false, :locals => vars
end
