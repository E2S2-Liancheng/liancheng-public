#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'digest'

helpers do
  def protected!(conn)
    auth = Rack::Auth::Basic::Request.new(request.env)

    if auth.provided? && auth.basic? && auth.credentials
      params = { :username => auth.credentials[0],
                :hashed_password => Digest::SHA256.hexdigest(auth.credentials[1]) }
      res = conn.post(MASHUP_SERVER + '/login', params)
      msg = JSON[res.content]

      return if msg['status'] == 'ok'
    end

    headers['WWW-Authenticate'] = 'Basic realm="Restricted Area"'
    halt 401, "Not authorized\n"
  end
end


get %r{^/direct/(\d+)/(-1|\d+)$} do
  c = HTTPClient.new
  protected!(c)

  inode_id = params[:captures][0].to_i
  revision = params[:captures][1].to_i

  res = c.get(MASHUP_SERVER + "/inode/#{inode_id}")
  halt 403, "Insufficient rights" if res.code == 403
  check_http_code(res)
  msg = JSON.parse(res.content)

  res = c.get(MASHUP_SERVER + "/inode/#{inode_id}/#{revision}/contents")
  check_http_code(res)

  temp_file = Tempfile.new('dl-')
  temp_file.write(res.content)
  temp_file.close

  send_file(temp_file.path, :filename => msg['inode']['name'], :disposition => 'attachment', :type => res.content_type)
end
