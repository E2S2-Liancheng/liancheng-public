#!/usr/bin/env ruby
#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require 'sinatra'
require 'haml'
require 'json'
require 'net/http'
require 'httpclient'
require 'securerandom'
require_relative '../common/common'
require_relative '../common/api_errors'
require_relative '../common/settings'


# read the Sinatra's web server's configuration and set it up
SETTINGS['self'].each { |k,v| set k.to_sym, v } if SETTINGS['self']

# use sessions with a timeout of 12 hours
use Rack::Session::Pool, :expire_after => 43200

# This tells HAML to render in HTML5 (needed for pieces rendered without the layout)
set :haml, :format => :html5

# A few settings cached
STREAMS_ENABLED = SETTINGS['features']['streams']

# base URI of the web service machine
MASHUP_SERVER = SETTINGS['mashup']['scheme'] + '://' + SETTINGS['mashup']['host'] + ':' + SETTINGS['mashup']['port']


require './header_assets'


helpers do
  def show_error_msg()
    html = @reuse ? %Q(<div class="error_msg">#{@reuse[1]}</div>) : '<div class="error_msg no_error">&nbsp;</div>'
    '<div id="outer_error_msg">' + html + '</div>'
  end

  def check_http_code(res)
    if res.status != 200
      session.clear if res.status == 401
      halt 500
    end
  end

  def esc_html(text)
    Rack::Utils.escape_html(text)
  end

  def count_notifications()
    if session[:conn]
      res = session[:conn].get(MASHUP_SERVER + "/notif/count")
      if res.status == 200
        msg = JSON.parse(res.content)
        return Integer(msg['count']) if msg['status'] == 'ok'
      end
    end

    nil
  rescue
    nil
  end
end


def set_reuse()
  @reuse = session.delete(:reuse)
  @reuse = nil if @reuse && @reuse[0] != request.path_info
end

def check_response_code(conn)
  if conn.status == 401 || conn.status == 404
    session[:reuse] = ['/login', 'Server oops!', { :username => session[:id] }]
    redirect '/logout'
  end
end

def json_sym(string)
  JSON.parse(string, :symbolize_names => true)
end


# User Access Control
# Check before access to all resources but the ones from PUBLIC_PAGES
#
PUBLIC_PAGES = %w[ /login /contact /register /favicon.png ].to_set
before do
  pass if PUBLIC_PAGES.include?(request.path_info) || request.path_info =~ /\/admin.*/     \
                                                   || request.path_info =~ /\/object\/.*/  \
                                                   || request.path_info =~ /\/~.*/         \
                                                   || request.path_info =~ /\/direct\/.*/
  if session[:id].nil?
    unless request.path_info == '/'
      msg = "Non-authenticated client request from #{request.ip} on #{request.request_method} #{request.path_info}"
      $logger.warn(msg)
    end
    redirect '/login'
  end
end


require_relative 'app/login.rb'


get '/contact' do
  haml :contact
end


get '/' do
  call env.merge("PATH_INFO" => '/elfinder')
end


require_relative 'app/admin'
require_relative 'app/elfinder'
require_relative 'app/elfinder_external_access'
require_relative 'app/crews'
require_relative 'app/workflow'
require_relative 'app/jobs'
require_relative 'app/streams'
require_relative 'app/visu'
require_relative 'app/settings'
require_relative 'app/direct'
require_relative 'app/obfuscated'
require_relative 'app/published'
require_relative 'app/notifications'
