//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function(){

    //////////////////////////////////////////////////////////////////////////
    // accounts

    $("#toolbar .create_hobo").on("click", function(){
        $.get("/streams/hobo")
            .done(function(data){
                var account = $(data);
                saveDefaultDevicesHobo(account);
                account.find(".buttons button.cancel").prop("disabled", false);
                $("#accounts").append(account);
            });
    });

    function createHtmlDeviceHobo(name) {
        return "<div class='device'><span>" + name + "</span><div class='delete_device'>×</div></div>";
    }

    function getDevicesListHobo(deviceList) {
        return deviceList.find(".device > span")
                         .map(function(i,v){ return $(v).text(); })
                         .get().sort();
    }

    function loadDefaultDevicesHobo(hobo) {
        var deviceList = $(hobo).find(".device_list");
        devices = deviceList.data("default_devices");
        var container = deviceList.children(".device_list_container");
        container.empty();
        devices.forEach(function(v){ container.append(createHtmlDeviceHobo(v)); });
    }

    function saveDefaultDevicesHobo(hobo) {
        var deviceList = $(hobo).find(".device_list");
        var devices = getDevicesListHobo(deviceList);
        deviceList.data("default_devices", devices);
    }

    $(".hobo_account").each(function(i,a){ saveDefaultDevicesHobo(a); });

    $("#accounts").on("click", ".hobo_account .add_device", function(){
        var line = $(this).parent();
        var input = line.find("input")
        var name = input.val();
        if(name.length > 0) {
            line.parent().find(".device_list_container").append(createHtmlDeviceHobo(name));
            checkMyChangesHobo(line);
            input.val("");
        }
    });

    $("#accounts").on("click", ".hobo_account .device .delete_device", function(){
        var device = $(this).parent();
        var deviceList = device.parent();
        device.remove();
        checkMyChangesHobo(deviceList);
    });


    function checkMyChangesHobo(hobo) {
        var account = $(hobo).closest(".hobo_account");

        var login = account.find("input.login");
        var oldLogin = login.attr("value");
        var newLogin = login.val();
        var loginOk = oldLogin == newLogin;
        var loginValid = newLogin.length > 0;

        var passwordOk = account.find("input.password").val().length == 0;

        var deviceList = account.find(".device_list");
        var oldDevices = deviceList.data("default_devices");
        var currentDevices = getDevicesListHobo(deviceList);
        var deviceListOk = oldDevices.length == currentDevices.length &&
                           oldDevices.reduce(function(s,v,i,_) { return s && v == currentDevices[i]; }, true);

        var formIntact = loginOk && passwordOk && deviceListOk;

        var buttons = account.find(".buttons button");
        var a = buttons.filter(".apply");
        buttons.filter(".apply")
            .prop("disabled", !loginValid || formIntact);
        buttons.filter(".cancel")
            .prop("disabled", oldLogin.length > 0 && formIntact);
    }

    $("#accounts").on("input", ".hobo_account input.login, .hobo_account input.password", function(){
        checkMyChangesHobo(this);
    });


    $("#accounts").on("click", ".hobo_account .apply", function(){
        var hobo = $(this).closest(".hobo_account");

        var login = hobo.find("input.login");
        var oldLogin = login.attr("value");
        var newLogin = login.val();
        var password = hobo.find("input.password").val();
        var devices = getDevicesListHobo(hobo.find(".device_list"));

        if(newLogin.length == 0) {
            alert("Your login cannot be an empty string");
            return;
        }

        if(oldLogin.length == 0 && password.length == 0) {
            alert("You must provide a password");
            return;
        }

        var form = { devices: devices };
        if(oldLogin.length > 0)
            form["old_login"] = oldLogin;
        if(newLogin != oldLogin)
            form["new_login"] = newLogin;
        if(password.length > 0)
            form["password"] = password;

        hobo.parent().find(".buttons button").prop("disabled", true);
        $.post("/streams/hobo", form)
            .done(function(){
                login.attr("value", newLogin);
                hobo.find("input.password").val("");
            })
            .fail(function(){
                alert("An error occurred while saving, please try again later");
                hobo.parent().find(".buttons button").prop("disabled", false);
            });
    });

    $("#accounts").on("click", ".hobo_account .cancel", function(){
        var hobo = $(this).closest(".hobo_account");

        var login = hobo.find("input.login");
        var oldLogin = login.attr("value");

        if(oldLogin.length == 0) {
            hobo.remove();
            return;
        }

        login.val(oldLogin);
        hobo.find("input.password").val("");
        loadDefaultDevicesHobo(hobo);

        hobo.parent().find(".buttons button").prop("disabled", true);
    });

    $("#accounts").on("click", ".hobo_account .delete_account", function(){
        if(!window.confirm("Are you sure?"))
            return;

        var hobo = $(this).closest(".hobo_account");

        $.post("/streams/hobo/delete", { login: hobo.find("input.login").attr("value") })
            .done(function(){
                hobo.remove();
            })
            .fail(function(){
                alert("nope");
            });
    });

    //////////////////////////////////////////////////////////////////////////
    // records

    if($("#records").length > 0)
        var streams = JSON.parse($("#records").attr("data-streams"));

    $("#ar_account_type").on("change", function(){
        var $this = $(this);
        $this.parent().nextAll().remove();
        var ar = $this.parent().parent();

        switch($this.val()) {
            case "hobo":
                var field = $("<span/>", { class: "ar_field" });
                field.append($("<span/>", { html: "Login:&nbsp;" }));
                var select1 = $("<select/>", { id: "ar_hobo_login" });
                streams['hobo'].forEach(function(v){
                    select1.append($("<option/>", { value: v['login'], text: v['login'] }));
                });
                field.append(select1);
                ar.append(field);

                field = $("<span/>", { class: "ar_field" });
                field.append($("<span/>", { html: "Device:&nbsp;" }));
                var select2 = $("<select/>", { id: "ar_hobo_device_id" });
                var devices = streams['hobo'].filter(function(v){ return v["login"] == select1.val(); })[0]['devices'];
                devices.forEach(function(v){
                    select2.append($("<option/>", { value: v['device_id'], text: v['device_id'] }));
                });
                field.append(select2);
                ar.append(field);

                field = $("<span/>", { class: "ar_field" });
                field.append($("<span/>", { html: "File name:&nbsp;" }));
                field.append($("<input/>", { id: "ar_hobo_file_path", type: "text", readonly: "readonly" }));
                field.append($("<button/>", { id: "ar_hobo_file_path_picker", text: "Select" }));
                ar.append(field);

                ar.append($("<button/>", { id: "ar_hobo_add", text: "Add" }));

                break;
            default:
        }
    });

    $("#add_record").on("change", "#ar_hobo_login", function(){
        var $this = $(this);

        var devices = streams['hobo'].filter(function(v){ return v["login"] == $this.val(); })[0]['devices'];
        var deviceList = $("#ar_hobo_device_id");
        deviceList.children().remove();
        devices.forEach(function(v){
            deviceList.append($("<option/>", { value: v['device_id'], text: v['device_id'] }));
        });
    });

    $("#add_record").on("click", "#ar_hobo_file_path_picker", function(){
        var $this = $(this);
        $this.prop("disabled", true);

        $.get("/workflow/file_picker", { save : "" })
            .done(function(data){
                var html = $(data);
                $("body").append(html);
                var filePicker = new FilePicker(html)
                    .ok(function(data){
                        $("#ar_hobo_file_path").val(data["file_name"]);
                        $("#ar_hobo_file_path").data("directory_id", data["directory_id"]);
                    })
                    .finally(function(){
                        $this.prop("disabled", false);
                        html.remove();
                    });
            })
            .fail(function(){
                $this.prop("disabled", false);
            });
    });

    $("#add_record").on("click", "#ar_hobo_add", function(){
        var login = $("#ar_hobo_login").val();
        var deviceId = $("#ar_hobo_device_id").val();

        var form = { login: login,
                     device_id: deviceId,
                     directory_id: $("#ar_hobo_file_path").data("directory_id"),
                     file_name: $("#ar_hobo_file_path").val() };

        $.post("/streams/hobo/record", form)
            .done(function(data){
                var sHobo = streams["hobo"];
                var sAccount = sHobo.filter(function(v){ return v["login"] == login; })[0];
                var sDevices = sAccount["devices"];

                sAccount["devices"] = sDevices.filter(function(v){ return v["device_id"] != deviceId; });
                if(sAccount["devices"].length != 0)
                    $("#ar_hobo_device_id").find("option").filter(function(i,v){
                        return $(v).val() == deviceId;
                    }).remove();
                else {
                    streams["hobo"] = sHobo.filter(function(v){ return v["login"] != login; });
                    if(streams["hobo"].length != 0) {
                        $("#ar_hobo_login").find("option").filter(function(i,v){
                            return $(v).val() == login;
                        }).remove();
                        $("#ar_hobo_login").trigger("change");
                    }
                    else {
                        $("#ar_account_type").find("option").filter(function(i,v){
                            return $(v).val() == "hobo";
                        }).remove();
                        $("#ar_account_type").trigger("change");
                    }
                }

                $("#hobolink_records").append(data);
            })
            .fail(function(){
                alert("Nope.");
            });
    });

    $("#hobolink_records").on("click", ".delete_hobo_device", function(){
        var $this = $(this);

        var form = { login: $this.attr("data-login"),
                     device_id: $this.attr("data-device-id") };
        $.post("/streams/hobo/record/delete", form)
            .done(function(){
                $this.closest(".hobolink_device").remove();

                // TODO reinsert the device in the adding list
            })
            .fail(function(){
                alert("Nope.");
            });
    });
});
