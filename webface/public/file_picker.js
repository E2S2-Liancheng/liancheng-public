//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
var FilePicker = function(main_node) {

    var ok_callback = null;
    this.ok = function(callback) {
        ok_callback = callback;
        return this;
    };

    var cancel_callback = null;
    this.cancel = function(callback) {
        cancel_callback = callback;
        return this;
    };

    var finally_callback = null;
    this.finally = function(callback) {
        finally_callback = callback;
        return this;
    };

    var saveMode = main_node.find(".fp_save_mode").length > 0;
    var $fileName = main_node.find(".fp_new_file_name");                // only used when saving

    var current_directory_id = main_node.find(".fp_nav").attr("data-starting-dir");
    var selected = null;

    function updateDirsFiles(dir_id) {
        var $fp_dirs = main_node.find(".fp_dirs");
        var $fp_files = main_node.find(".fp_files");

        $fp_dirs.children().remove();
        $fp_files.children().remove();
        selected = null;

        var spinner = "<img src='images/spinning.svg' class='fp_spinning'/>";
        $fp_dirs.append($(spinner));
        $fp_files.append($(spinner));

        $.get("/workflow/file_picker/contents/" + dir_id, function(data){
            var contents = $(data);
            current_directory_id = contents.attr("data-directory-id");
            $fp_dirs.children().remove();
            $fp_files.children().remove();
            contents.find(".dirs").children().appendTo($fp_dirs);
            contents.find(".files").children().appendTo($fp_files);
        });
    }

    main_node.on("click", ".fp_roots .fp_dir", function(){
        var dir_id = $(this).attr("data-dir-id");
        if(!this.classList.contains("selected")) {
            main_node.find(".fp_roots .selected").removeClass("selected");
            this.classList.add("selected");
        }
        updateDirsFiles(dir_id);
    });

    main_node.on("click", ".fp_dirs .fp_dir", function(){
        var dir_id = $(this).attr("data-dir-id");
        updateDirsFiles(dir_id);
    });

    main_node.on("click", ".fp_file", function(){
        if(selected != null)
        {
            selected.removeClass("selected");
            if(selected[0] === this) {
                selected = null;
                if(saveMode)
                    $fileName.val("");
                return;
            }
        }
        selected = $(this);
        selected.addClass("selected");
        if(saveMode)
            $fileName.val(selected.text());
    });


    main_node.find(".fp_button_ok").on("click", function(){
        if(!saveMode && selected == null || saveMode && $fileName.val().length == 0)
            return;

        if(ok_callback) {
            if(!saveMode)
                ok_callback({ inode_id:  selected.attr("data-inode-id"),
                              revision:  selected.attr("data-inode-revision"),
                              file_name: selected.text() });
            else
                ok_callback({ directory_id: current_directory_id,
                              file_name:    $fileName.val() });
        }
        if(finally_callback)
            finally_callback();
    });

    main_node.find(".fp_button_cancel").on("click", function(){
        if(cancel_callback)
            cancel_callback();
        if(finally_callback)
            finally_callback();
    });

    main_node.draggable({ handle: ".fp_title", cursor: "move", containment: "parent" });


    function isEllipsisActive($jQueryObject) {
        return ($jQueryObject.outerWidth() < $jQueryObject[0].scrollWidth);
    }

    main_node.height(Math.round($("body").height() * 0.8));
    var filesHeight = main_node.innerHeight() - main_node.find(".fp_title").outerHeight()
                                              - main_node.find(".fp_save_mode").outerHeight()
                                              - main_node.find(".fp_buttons").outerHeight();
    main_node.find(".fp_files").height(filesHeight);
    var dirsHeight = filesHeight - main_node.find(".fp_roots").outerHeight()
                                 - main_node.find(".fp_horizontal").outerHeight();
    main_node.find(".fp_dirs").height(dirsHeight);

    main_node.find(".fp_file").each(function(i,v){
        var $v = $(v);
        if(isEllipsisActive($v))
            $v.attr("title", $v.text());
    });
};

$(document).ready(function(){
});
