//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function() {
    $("#elfinder").elfinder({
        url : "/elfinder/connector",
        requestType : "post",
        resizable : false,
        rememberLastDir : false,        // important! otherwise opens wrong directory when changing user
        //commandsOptions : { help : {view : ['shortcuts'] } },
        uiOptions : {
            toolbar : [
                ['home', 'up', 'back', 'forward'],
                ['mkdir', 'upload'], // ['mkdir', 'mkfile', 'upload'],
                //['new_workflow', 'edit_workflow'],
                ['download', 'rename'], //['open', 'download', 'getfile'],
                ['info'], //['info', 'quicklook'],
                ['copy', 'cut', 'paste'],
                //['rm'],
                //['duplicate', 'edit', 'resize'],
                //['extract', 'archive'],
                //['search'],
                ['view', 'sort'],
                ['help']
            ]
        }
        // lang: 'ru'
    });

    $("#elfinder").height($("#contents_td").height() - 2);
});
