//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function() {

    var edited_crew = -1;

    function getMemberList(crew_node, callback) {
        var crew_member_list = crew_node.find(".crew_member_list").first();

        if(crew_member_list.attr("data-retrieved") == "true") {
            callback();
            return;
        }

        $.get("/crews/" + crew_node.attr("data-id"))
        .done(function(data){
            crew_member_list.append(data);
            crew_member_list.attr("data-retrieved", "true");

            callback();
        })
        .fail(function(event){
            if(event.status == 500)
                window.location.href = "/";
        });
    }

    $("#col1").on("click", ".showhide", function(event){
        var $this = $(this);
        var crew = $this.parent().parent();
        var crew_member_list = crew.find(".crew_member_list").first();

        if(!crew_member_list.hasClass("shown")) {
            getMemberList(crew, function(){
                crew_member_list.addClass("shown");
                $this.text("—");
            });
        }
        else {
            crew_member_list.removeClass("shown");
            $this.text("+");
        }

        return false;
    });

    function enableEdit() {
        $("#crew_edit").prop("disabled", $("#col1 .crew.selected").length != 1);
    }

    function enableDelete() {
        $("#crew_delete").prop("disabled", $("#col1 .crew.selected").length == 0);
    }

    $("#col1").on("click", ".crew", function(event){
        var $this = $(this);

        if(!$this.hasClass("selected")) {
            getMemberList($this, function(){
                $("#col1 .crew").removeClass("selected");
                $this.addClass("selected");
                enableEdit();
                enableDelete();
            });
        }
        else {
            $("#col2 .crew_member_list").first().empty();
            $this.removeClass("selected");
            enableEdit();
            enableDelete();
        }

        event.preventDefault();
        return false;
    });

    $("#crew_create").on("click", function(){
        edited_crew = -1;
        $("#crew_create, #crew_edit, #crew_delete").prop("disabled", true);
        $("#crew_save, #crew_cancel, #member_add, #member_del").prop("disabled", false);
        $("#crew_name").prop("disabled", false).val("");
        $("#col2 .crew_member_list").empty();
        $("#col3 .member .action").removeClass("disabled");
    });

    $("#crew_edit").on("click", function(){
        var crew = $("#col1 .crew.selected").first();
        var crew_members = crew.find(".member");

        $("#col2 .crew_member_list").first().empty().append(crew_members.clone());
        $("#col2 .crew_member_list .member .action").removeClass("hidden").text("»");

        var ids = crew_members.map(function(){ return $(this).attr("data-id"); }).toArray();
        ids.forEach(function(value){ $("#col3 .member[data-id='" + value + "']").addClass("filtered"); });
        $("#col3 .member .action").removeClass("disabled");

        edited_crew = parseInt(crew.attr("data-id"));
        $("#crew_create, #crew_edit, #crew_delete").prop("disabled", true);
        $("#crew_save, #crew_cancel, #member_add, #member_del").prop("disabled", false);
        $("#crew_name").prop("disabled", false).val(crew.find(".name").first().text());
    });

    function deleteCrew(crew_id, give_copy) {
        var params = {};
        if(give_copy)
            params["give_copy"] = true;

        $.post("/crews/delete/" + crew_id, params)
            .done(function(){
                $("#col1 .crew.selected").remove();
                enableEdit();
                $("#crew_delete").prop("disabled", true);
            });
    }

    $("#crew_delete").on("click", function(){
        var crew_id = $("#col1 .crew.selected").first().attr("data-id");

        $.post("/crews/prepare_delete/" + crew_id)
            .done(function(msg){
                switch(msg["status"]) {
                    case "ok":  deleteCrew(crew_id); break;
                    case "err": alert(msg["err_msg"]); break;
                    case "confirm":
                        var $confirm = $(msg["confirm"]);
                        $("body").append($confirm);
                        // TODO add jquery-ui ?
                        //$confirm.draggable({ handle: ".cc_title" });
                        //$confirm.find(".cc_title").disableSelection();
                        $("#modal_background").css("display", "block");

                        $confirm.find("button.ok").on("click", function(){
                            var give_copy = $confirm.find("input.give_copy").prop("checked");
                            deleteCrew(crew_id, give_copy);
                            $confirm.remove();
                            $("#modal_background").css("display", "none");
                        });

                        $confirm.find("button.cancel").on("click", function(){
                            $confirm.remove();
                            $("#modal_background").css("display", "none");
                        });

                        break;
                }
            });
        
    });

    function endSave() {
        edited_crew = -1;
        $("#crew_create").prop("disabled", false);
        $("#crew_save, #crew_cancel").prop("disabled", true);
        enableEdit();
        enableDelete();
        $("#crew_name").prop("disabled", true).val("");
        $("#col2 .member").remove();
        $("#col3 .member").removeClass("filtered");
        $("#col3 .member .action").addClass("disabled");
    }

    function updateCrew(params, give_copy) {
        if(give_copy)
            params["give_copy"] = true;

        $.post("/crews/update/" + edited_crew, params)
            .done(function(){
                var crew = $("#col1 .crew[data-id='" + edited_crew + "']");
                crew.find("span.name").text(params["name"]);

                var members = $("#col2 .member").clone();
                crew.find(".crew_member_list").first().empty().append(members);

                endSave();
            })
            .fail(function(){
                alert("Error while saving, please try again later");
            });
    }

    $("#crew_save").on("click", function(){
        var name = $("#crew_name").val();
        if(name == "") {
            alert("You must give a name to the group");
            return;
        }

//        $("#controls button").prop("disabled", true);
        var members = $("#col2 .member").map(function(){ return $(this).attr("data-id"); }).toArray();

        if(edited_crew == -1) {
            $.post("/crews/create", { name: name, "members[]": members })
            .done(function(data){
                $("#col1").append(data["html"]);
                $("#col2 .crew_member_list").first().empty();

                endSave();
            })
            .fail(function(){
                alert("Error while saving, please try again later");
            });
        }
        else {
            var params = { name: name, "members[]": members };

            $.post("/crews/prepare_update/" + edited_crew, params)
                .done(function(msg){
                    switch(msg["status"]) {
                        case "ok":  updateCrew(params); break;
                        case "err": alert(msg["err_msg"]); break;
                        case "confirm":
                            var $confirm = $(msg["confirm"]);
                            $("body").append($confirm);
                            // TODO add jquery-ui ?
                            //$confirm.draggable({ handle: ".cc_title" });
                            //$confirm.find(".cc_title").disableSelection();
                            $("#modal_background").css("display", "block");

                            $confirm.find("button.ok").on("click", function(){
                                var give_copy = $confirm.find("input.give_copy").prop("checked");
                                updateCrew(params, give_copy);
                                $confirm.remove();
                                $("#modal_background").css("display", "none");
                            });

                            $confirm.find("button.cancel").on("click", function(){
                                $confirm.remove();
                                $("#modal_background").css("display", "none");
                            });

                            break;
                    }
                });
        }
    });

    function putBack(members) {
        var ids = members.map(function(){ return $(this).attr("data-id"); }).toArray();
        ids.forEach(function(value){ $("#col3 .member[data-id='" + value + "']").removeClass("selected filtered"); });
        members.remove();
    }

    $("#crew_cancel").on("click", function(){
        var members = $("#col2 .member");
        putBack(members);

        edited_crew = -1;
        $("#crew_create").prop("disabled", false);
        enableEdit();
        enableDelete();
        $("#crew_save, #crew_cancel, #member_add, #member_del").prop("disabled", true);
        $("#crew_name").prop("disabled", true).val("");
        $("#col3 .member").removeClass("filtered");
        $("#col3 .member .action").addClass("disabled");
    });

    $("#col2").on("click", ".member .action", function(){
        var $this = $(this);
        var $member = $this.closest(".member");

        $("#col3 .member[data-id='" + $member.attr("data-id") + "']").removeClass("filtered");
        $member.remove();
    });

    $("#col3 .member .action").on("click", function(){
        var $this = $(this);
        var $member = $this.closest(".member");
        if($member.hasClass("filtered") || $this.hasClass("disabled"))
            return false;

        var $clone = $member.clone();
        $clone.find(".action").text("»");
        $("#col2 .crew_member_list").append($clone);

        $member.addClass("filtered");

        return false;
    });
});
