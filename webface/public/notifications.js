//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function(){
    $("#inner_list").on("click", ".share button", function(){
        var $this = $(this);
        var $item = $this.closest(".item");

        $item.find("button").prop("disabled", true);
        var params = { directory_id: $item.attr("data-id"),
                       action: $this.attr("data-action") };

        $.post("/notifs/share", params)
            .done(function(){
                $item.slideUp(300, function(){ $item.remove(); });
            })
            .fail(function(){
                alert("An unexpected error occurred. Please try again later.");
                $item.find("button").prop("disabled", false);
            })
    });
});
