//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function(){
    $("#i_agree").on("click", function(){
        if($("#name").val().length == 0) {
            alert("Your must provide your name");
            return;
        }

        if($("#password1").val() != $("#password2").val())
        {
            alert("Your password verification failed");
            return;
        }

        if($("#password1").val().length < 8)
        {
            alert("Your password must be at least 8 characters long");
            return;
        }

        // email validation is awfully complicated, we just do a basic check
        // and will rely on the validation mechanism instead
        var email = $("#email").val();
        if(email.length < 5 || email.indexOf("@") == -1) {
            alert("Your email address is not valid");
            return;
        }

        var pass = $("#password1");
        $("#hashed_password").val(CryptoJS.SHA256(pass.val()));
        pass.val("");
        $("#password2").val("");

        $("#modal_background").css("display", "block");
        $("#modal_background").css("cursor", "wait");

        $.post("/register", $("#form").serialize())
            .done(function(msg){
                if(msg["status"] != "ok") {
                    alert(msg["err_msg"]);
                    return;
                }

                location.replace("/pending_registration/" + msg["pending_id"]);
            })
            .fail(function(){
                alert("Unexpected server error, please try again later");
            })
            .always(function(){
                $("#modal_background").css("display", "none");
                $("#modal_background").css("cursor", "auto");
        });
    });
});
