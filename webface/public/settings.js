//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function() {

    $("#form_password .submit button").on("click", function() {
        if($("#new_password1").val() != $("#new_password2").val())
        {
            alert("Your password verification failed");
            return false;
        }

        if($("#new_password1").val().length < 8)
        {
            alert("Your password must be at least 8 characters long");
            return false;
        }

        $("#modal_background").css("display", "block");
        $("#modal_background").css("cursor", "wait");

        var oldHash = CryptoJS.SHA256($("#password").val()).toString(CryptoJS.enc.Hex);
        var newHash = CryptoJS.SHA256($("#new_password1").val()).toString(CryptoJS.enc.Hex);

        $.post('/settings/change_password', { old_password: oldHash, new_password: newHash })
        .done(function(data) {
            if(data["status"] == 'ok') {
                alert("Your password has been changed.");
                $("#password").val($("#new_password1").val());
                $("#new_password1").val("");
                $("#new_password2").val("");
            }
            else
                alert(data["err_msg"]);
        })
        .fail(function() {
            alert("Unexpected error on the server. Please try again later.");
        })
        .always(function() {
            $("#modal_background").css("display", "none");
            $("#modal_background").css("cursor", "auto");
        });

        return false;
    });

    $(".buttons").each(function(_,b){
        var $b = $(b);
        var w1 = $b.find("button.edit").width();
        var w2 = $b.find(".ok_cancel").width();
        $b.width(Math.max($b.find("button.edit").width(), $b.find(".ok_cancel").width()));
    });

    $("#user_info .field button.edit").on("click", field_edit);
    $("#user_info .field button.ok").on("click", field_ok);
    $("#user_info .field button.cancel").on("click", field_cancel);
    $("#user_info form.field").on("submit", field_ok);

    function field_edit(event) {
        var $field = $(event.target).closest(".field");
        $field.find(".buttons > *").toggleClass("hidden");
        var $input = $field.find("input");
        var a = $input.val();
        $input.prop("disabled", false).focus().val("").val(a);          // trick to get the cursor at the end
        return false;
    }

    function field_ok(event) {
        var $field = $(event.target).closest(".field");
        var target = $field.attr("data-target");
        var $input = $field.find("input");
        var form = {};
        form[target] = $input.val();
        $.post("/settings/" + target, form)
            .done(function(data){
                if(data["status"] == "ok") {
                    var name = data[target];
                    $input.attr("data-saved", name).val(name).prop("disabled", true);
                    $field.find(".buttons > *").toggleClass("hidden");
                    if(target == "name")
                        $("#user_name").text(name);
                }
                else {
                    alert(data["err_msg"]);
                }
            });
        return false;
    }

    function field_cancel(event) {
        var $field = $(event.target).closest(".field");
        $field.find(".buttons > *").toggleClass("hidden");
        var $input = $field.find("input");
        $input.prop("disabled", true).val($input.attr("data-saved"));
        return false;
    }
});
