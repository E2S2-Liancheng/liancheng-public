//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function() {
    var selector = ".wf_node[data-node-type='operator_projection']";

    Globals.wfDesktop.on("wfSave", selector, onSave);

    Globals.wfDesktop.on("click", selector + " .buttons .check_all", onModifyAll);
    Globals.wfDesktop.on("click", selector + " .buttons .uncheck_all", onModifyAll);
    Globals.wfDesktop.on("click", selector + " .buttons .reorder", onReorder);
    Globals.wfDesktop.on("change", selector + " .column input[type='checkbox']", onSelectCols);
    Globals.wfDesktop.on("sortupdate", selector + " .columns", onSelectCols);
    Globals.wfDesktop.on("wfUpdatedCols", selector, onUpstreamUpdatedCols);
    Globals.wfDesktop.on("wfRenamedCol", selector, onRenamedCol);
    Globals.wfDesktop.on("wfConnected", selector, onSelectCols);

    function onSave(event, $domElement, info) {
        var $cols = $domElement.find(".columns .column input");
        if($cols.length > 0)
            info["data"] = { cols: $cols.map(function(_,col){
                                        $col = $(col);
                                        return { name: $col.attr("data-col-name"),
                                                 index: parseInt($col.attr("data-src-index")),
                                                 checked: $col.prop("checked") };
                                    }).toArray() };
        return false;
    }

    function onModifyAll() {
        var $button = $(this);
        var check = $button.hasClass("check_all");

        var $node = $button.closest(".wf_node");
        var $cols = $node.find(".columns .column input");

        var changes = false;
        $cols.each(function(_,col){
            var $col = $(col);
            if($col.prop("checked") != check) {
                $col.prop("checked", check);
                changes = true;
            }
        });

        if(changes)
            onSelectCols.apply(this);

        return false;
    }

    function onReorder() {
        var $node = $(this).closest(".wf_node");
        var $parent = $node.find(".columns");
        var $cols = $parent.children(".column");

        if($cols.length < 2)
            return false;

        var indices = $cols.map(function(_,col){
            var $col = $(col);
            if(typeof $col.data("index") === "undefined")
                $col.data("index", parseInt($col.find("input").attr("data-src-index")));

            return $col.data("index");
        });

        var sorted = true;
        for(var i = 0; i < indices.length - 1 && sorted; ++i)
            if(indices[i] > indices[i+1])
                sorted = false;

        if(sorted)
            return false;

        $cols.detach().sort(function(a, b) {
            return $(a).data("index") > $(b).data("index");
        }).appendTo($parent);

        onSelectCols.apply(this);

        return false;
    }

    function onSelectCols() {
        var $node = $(this).closest(".wf_node");
        var $cols = $node.find(".columns .column input");
        var cols = $cols.filter(function(_,col){ return $(col).prop("checked"); }).map(function(_,col){
            $col = $(col);
            return { name: $col.attr("data-col-name"),
                     type: "string" };
        }).toArray();

        $node.data("self").trigger(0, "wfUpdatedCols", [cols]);

        // do not return false, otherwise the sort will be cancelled
    }

    function onUpstreamUpdatedCols(event, hookNum, cols) {
        var $node = $(this);
        var self = $node.data("self");

        var tokenId = self.getCounter("update");

        // TODO do not delete and add, modify on name change
        var ncols = cols.map(function(col, index) {
            return { name: col["name"],
                    index: index,
                    checked: false };
        });

        if(ncols.length != 0)
            $.post("/workflow/operator_projection/cols", { cols: JSON.stringify(ncols) })
                .done(function(data) {
                    if(tokenId != self.checkCounter("update"))
                        return;
                    var $cols = $node.find(".columns");
                    $cols.find(".column").remove();
                    $cols.append($("<div>"+data+"</div>").children());
                });
        else
            $node.find(".columns .column").remove();

        onSelectCols.apply(this);
        return false;
    }

    function onRenamedCol(event, hookNum, index, name) {
        var $node = $(this);
        var $cols = $node.find(".columns .column input");
        var newIndex = -1;
        for(var i = 0; i < $cols.length; ++i) {
            var $col = $cols.eq(i);
            var checked = $col.prop("checked");
            if(checked)
                ++newIndex;
            if(parseInt($col.attr("data-src-index")) == index) {
                if(!checked)
                    newIndex = -1;
                $col.attr("data-col-name", name);
                $col.next().text(name);
                break;
            }
        }
        if(newIndex >= 0)
            $node.data("self").trigger(0, "wfRenamedCol", [newIndex, name]);
    }
});
