//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function() {
    var selector = ".wf_node[data-node-type='operator_filter']";

    Globals.wfDesktop.on("wfSave", selector, onSave);

    Globals.wfDesktop.on("click", selector + " .buttons .add", onAddClause);
    Globals.wfDesktop.on("click", selector + " .clause button.delete", onDeleteClause);
    Globals.wfDesktop.on("wfUpdatedCols", selector, onUpstreamUpdatedCols);
    Globals.wfDesktop.on("wfRenamedCol", selector, onRenamedCol);
    Globals.wfDesktop.on("wfConnected", selector, onConnected);
    Globals.wfDesktop.on("wfDisconnected", selector, onDisconnected);

    function onSave(event, $domElement, info) {
        var cols = $domElement.data("cols");
        info["data"] = { cols : cols };

        var $clauses = $domElement.find(".clause");
        if($clauses.length > 0)
            info["data"]["clauses"] = $clauses.map(function(_,clause){
                                          $clause = $(clause);
                                          return { name:  $clause.find(".col_name").val(),
                                                   op:    $clause.find(".col_op").val(),
                                                   value: $clause.find(".col_value").val() };
                                      }).toArray();
        return false;
    }

    function onAddClause() {
        var $button = $(this)
        var $node = $button.closest(".wf_node");

        $button.prop("disabled", true);

        $.post("/workflow/operator_filter/clause", { cols: JSON.stringify($node.data("cols")) })
            .done(function(data){
                $node.find(".clauses").append($(data));
            })
            .always(function(){
                $button.prop("disabled", false);
            });
    }

    function onDeleteClause() {
        var $clause = $(this).closest(".clause");
        $clause.fadeOut(400, function(){ $clause.remove(); });
    }

    function canReuseCols(oldCols, newCols) {
        if(oldCols.length != newCols.length)
            return false;

        for(var i = 0; i < oldCols.length; ++i)
            if(oldCols[i].name != newCols[i].name || oldCols[i].type != newCols[i].type)
                return false;

        return true;
    }

    function onUpstreamUpdatedCols(event, hookNum, cols) {
        var $node = $(this);

        var oldCols = $node.data("cols");
        var canReuse = canReuseCols(oldCols, cols);

        if(oldCols.length == 0) {
            $node.data("cols", cols);
        }
        else if(!canReuse) {
            $node.data("cols", cols);
            $node.find(".clauses .clause").remove();
        }
        if(cols.length != 0)
            $node.find(".buttons .add").attr("disabled", false);

        if(!canReuse)
            $node.data("self").trigger(0, "wfUpdatedCols", [cols]);
        return false;
    }

    function onRenamedCol(event, hookNum, index, name) {
        var $node = $(this);
        $node.find(".clauses .col_name").each(function(_,clause){
            $option = $(clause).find("option").eq(index);
            $option.attr("value", name);
            $option.text(name);
        });
        $node.data("cols")[index]["name"] = name;

        $node.data("self").trigger(0, "wfRenamedCol", [index, name]);
    }

    function onConnected(event) {
        var $node = $(this);
        var cols = $node.data("cols");
        $node.data("self").trigger(0, "wfUpdatedCols", [cols]);
        return false;
    }

    function onDisconnected(event) {
        var $node = $(this);
        $node.find(".buttons .add").attr("disabled", true);

        return false;
    }
});
