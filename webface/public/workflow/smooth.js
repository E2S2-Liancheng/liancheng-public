//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function() {
    var selector = ".wf_node[data-node-type='operator_smooth']";

    Globals.wfDesktop.on("wfSave", selector, onSave);

    Globals.wfDesktop.on("wfUpdatedCols", selector, onUpstreamUpdatedCols);
    Globals.wfDesktop.on("wfRenamedCol", selector, onRenamedCol);
    Globals.wfDesktop.on("wfConnected", selector, onConnected);

    function onSave(event, $domElement, info) {
        var cols = $domElement.data("cols");
        info["data"] = { cols : cols };

        if(cols.length > 0) {
            [[".latitude", "lat"], [".longitude", "lon"], [".timestamp", "ts"]].forEach(function(v){
                var q = $domElement.find(v[0] + " select");
                var w = q.val();
                info["data"][v[1]] = w;
            });
            info["data"]["steps"] = $domElement.find(".steps input").val();
        }

        return false;
    }

    function onUpstreamUpdatedCols(event, hookNum, cols) {
        var $node = $(this);
        $node.data("cols", cols);

        $node.find(".operator_smooth select").children().remove();
        if(cols.length != 0) {
            var $slat = $node.find(".latitude select");
            var rlat = /latitude/i;
            var flat = false;
            cols.forEach(function(c){
                var selected = rlat.test(c['name']);
                flat = flat || selected;
                var $opt = $("<option/>", { value: c['name'], selected: selected }).text(c['name']);
                $slat.append($opt);
            });
            if(!flat)
                $slat.children().eq(0).prop("selected", true);

            var $slon = $node.find(".longitude select");
            var rlon = /longitude/i;
            var flon = false;
            cols.forEach(function(c){
                var selected = rlon.test(c['name']);
                flon = flon || selected;
                var $opt = $("<option/>", { value: c['name'], selected: selected }).text(c['name']);
                $slon.append($opt);
            });
            if(!flon)
                $slon.children().eq(1).prop("selected", true);

            var $sts = $node.find(".timestamp select");
            var rts = /longitude/i;
            var fts = false
            cols.forEach(function(c){
                var selected = rts.test(c['name']);
                fts = fts || selected;
                var $opt = $("<option/>", { value: c['name'], selected: selected }).text(c['name']);
                $sts.append($opt);
            });
            if(!fts)
                $sts.children().eq(2).prop("selected", true);
        }

        $node.data("self").trigger(0, "wfUpdatedCols", [cols]);
        return false;
    }

    function onRenamedCol(event, hookNum, index, name) {
        var $node = $(this);
        $node.find(".operator_smooth select option:nth-child(" + (index + 1) + ")").each(function(_,opt){
            var $opt = $(opt);
            $opt.attr("value", name).text(name);
        });
        $node.data("cols")[index]["name"] = name;

        $node.data("self").trigger(0, "wfRenamedCol", [index, name]);
    }

    function onConnected(event) {
        var $node = $(this);
        var cols = $node.data("cols");
        $node.data("self").trigger(0, "wfUpdatedCols", [cols]);
        return false;
    }
});
