//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function() {
    var selector = ".wf_node[data-node-type='import_csv']";

    Globals.wfDesktop.on("wfInserted", selector, onInsertion);
    Globals.wfDesktop.on("wfSave", selector, onSave);

    Globals.wfDesktop.on("click", ".button_import_csv_change", onImportCsv);
    Globals.wfDesktop.on("click", selector + " button.display_extra", onDisplayExtra);
    Globals.wfDesktop.on("change", ".button_import_csv_has_header", onHasHeader);
    Globals.wfDesktop.on("click", ".button_import_csv_use_original", onUseOriginal);
    Globals.wfDesktop.on("click", ".button_import_csv_clear", onClear);
    Globals.wfDesktop.on("input", selector + " .list .col input.user", onTextChange);
    Globals.wfDesktop.on("click", selector + " button.auto_fix", onAutoFix)
    Globals.wfDesktop.on("wfConnected", selector, onConnected);

    function resizeTables($target) {
        if($target.data("alreadyResized"))
            return;
        var $cont = $target.find(".list_container");
        var nbCols = $cont.find(".header > * > *").length;
        for(var i = 1; i <= nbCols; ++i) {
            var $divn = $cont.find(".table > * > *:nth-child(" + i + ")");
            var ws = $divn.map(function(_,d){ return $(d).width(); }).toArray();
            var maxW = Math.ceil(Math.max.apply(null, ws));
            $divn.each(function(_,d){ $(d).width(maxW); });
        }
        $target.data("alreadyResized", true);
    }

    function onInsertion(event, $target) {
        if(!$target.find(".extra").hasClass("hidden"))
            resizeTables($target);
        return false;
    }

    function onSave(event, $domElement, info) {
        info["state"] = { extra_hidden: $domElement.find(".extra").hasClass("hidden") };
        var $cols = $domElement.find(".list .col");
        if($cols.length > 0) {
            var file = { full_path: $domElement.find("[data-param-name='name_in']").val(),
                            inode_id: parseInt($domElement.find("[data-param-name='inode_id_in']").val()),
                            revision: parseInt($domElement.find("[data-param-name='revision_in']").val()) };
            info["data"] = { file: file };
            info["data"]["has_header"] = $domElement.find(".button_import_csv_has_header").prop("checked");
            info["data"]["cols"] = $cols.map(function(_,col){
                var $col = $(col);
                return { orig: $col.find(".orig").val(),
                         user: $col.find(".user").val() };
            }).toArray();
        }
        return false;
    }

    function collectCols($target) {
        var cols = $target.find(".list .col").map(function(_,col){
            var $col = $(col);
            return { name: $col.find(".user").val(),
                     type: "string" };
        }).toArray();

        return cols;
    }

    function propagateTitle($node) {
        var title = $node.find(".title_bar_text").text() +
                    $node.find(".title_bar_extra").text() +
                    $node.find(".title_bar_id").text();
        $node.data("self").trigger(0, "wfUpdatedTitle", [title]);
    }

    function displayCsv($target, inode_id, revision, file_name, data) {
        $target.find("[data-param-name='name_in']").val(data["path"]);
        $target.find("[data-param-name='inode_id_in']").val(inode_id);
        $target.find("[data-param-name='revision_in']").val(revision);

        var $list = $target.find(".list");

        var values = data["header"];
        $.post("/workflow/import_csv/cols", { original: values, user_choice: values })
            .done(function(data){
                $list.find(".col").remove();
                $(data).appendTo($list);
                $node = $target.closest(".wf_node");

                $target.data("alreadyResized", false);
                resizeTables($node);

                $node.find(".title_bar_extra").text(" | " + file_name);

                propagateTitle($node);
                $node.data("self").trigger(0, "wfUpdatedCols", [collectCols($target)]);
            });

        $target.find("button.display_extra").prop("disabled", false);
        $target.find(".extra").removeClass("hidden");
        $target.find("button.display_extra").text("–");
    }

    function onImportCsv() {
        var $this = $(this);
        var $main = $this.closest(".contents");
        $this.prop("disabled", true);

        $.get("/workflow/file_picker").done(function(data){
            var html = $(data);
            $("body").append(html);
            var filePicker = new FilePicker(html)
                .ok(function(data){
                    var inode_id = data["inode_id"];
                    var revision = data["revision"];
                    var file_name = data["file_name"];
                    $.get("/workflow/import_csv/" + inode_id + "/" + revision)
                        .done(function(data){
                            if(data["status"] == "ok") {
                                displayCsv($main, inode_id, revision, file_name, data["inode"]);
                            }
                            else if(data["status"] == "err")
                                alert(data["err_msg"]);
                        })
                })
                .finally(function(){
                    $this.prop("disabled", false);
                    html.remove();
                });
        });
        return false;
    }

    function onDisplayExtra() {
        var $node = $(this).closest(".import_csv");
        var $extra = $node.find(".extra");
        $extra.toggleClass("hidden");
        var isHidden = $extra.hasClass("hidden");
        $node.find("button.display_extra").text(isHidden ? "+" : "–");
        if(!isHidden)
            resizeTables($node);
        return false;
    }

    function onHasHeader() {
        var list = $(this).closest(".import_csv").find(".list");
        list.find(".original").toggleClass("greyed");
        list.find(".button_import_csv_use_original").prop("disabled", $(this).prop("checked") == false);
        return false;
    }

    function onUseOriginal() {
        var $cols = $(this).closest(".list_container").find(".col").filter(function(_,col){
            return $(col).find("input.orig").val() != $(col).find("input.user").val();
        });
        $cols.each(function(_,col){
            $(col).find("input.user").val($(col).find("input.orig").val());
        });
        $cols.trigger("input");
        return false;
    }

    function onClear() {
        var $inputs = $(this).closest(".list_container").find(".col input.user");
        $inputs.val("");
        $inputs.trigger("input");
        return false;
    }

    function onTextChange(event) {
        var $this = $(this);
        var name = $this.val();
        if(name.length == 0)
            return;
        var index = parseInt($this.closest(".col").attr("data-col-index"));

        var node = $this.closest(".wf_node").data("self");
        node.trigger(0, "wfRenamedCol", [index, name]);
    }

    function onAutoFix() {
        var $cols = $(this).closest(".list_container").find(".col");
        $cols.each(function(i,c){
            $(c).find("input.user").val("col_" + (i + 1));
        });
        return false;
    }

    function onConnected() {
        var $node = $(this);
        propagateTitle($node);
        $node.data("self").trigger(0, "wfUpdatedCols", [collectCols($node)]);
        return false;
    }
});
