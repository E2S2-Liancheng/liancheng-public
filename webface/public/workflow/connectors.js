//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
var ConnectorManager = function() {

    var Connector = function(node, isInput, index, manager) {
        this.upstream = isInput ? null : node;
        this.upstreamIndex = isInput ? null : index;
        this.downstream = isInput ? node : null;
        this.downstreamIndex = isInput ? index : null;

        this.manager = manager;

        this.upIsAttached = !isInput;
        this.isFloating = true;

        node.addConnector(isInput, index, this);

        this.path = document.createElementNS("http://www.w3.org/2000/svg", "path");
        this.path.setAttribute("class", "connector");
        $(this.path).data("self", this);

        this.ux = null;
        this.uy = null;
        this.dx = null;
        this.dy = null;

        this.refreshPath = function() {
            var offset = 100;

            var c1x = this.ux;
            var c1y = this.uy + offset;
            var c2x = this.dx;
            var c2y = this.dy - offset;
            var path = "M " + this.ux + " " + this.uy + " C" + c1x + " " + c1y +
                       ", " + c2x + " " + c2y + ", " + this.dx + " " + this.dy;
            this.path.setAttribute("d", path);
        };

        this.setNode = function(node, isInput, index, loading) {
            if(isInput) {
                if(this.downstream != null)
                    this.downstream.removeConnector(true, this.downstreamIndex);
                this.downstream = node;
                this.downstreamIndex = index;
            }
            else {
                if(this.upstream != null)
                    this.upstream.removeConnector(false, this.upstreamIndex);
                this.upstream = node;
                this.upstreamIndex = index;
            }
            this.isFloating = false;

            node.addConnector(isInput, index, this);
            if(!loading)
                this.upstream.connected(this.upstreamIndex);
        };

        this.setUpstreamPos = function(x, y) {
            this.ux = x;
            this.uy = y;
            this.refreshPath();
        };

        this.setDownstreamPos = function(x, y) {
            this.dx = x;
            this.dy = y;
            this.refreshPath();
        };

        this.setPos = function(isInput, x, y) {
            isInput ? this.setDownstreamPos(x, y) : this.setUpstreamPos(x, y);
        }

        this.setFloating = function(isInput) {
            this.isFloating = true;
            this.upIsAttached = isInput;
        }

        this.setFloatingPos = function(x, y) {
            if(!this.isFloating)
                throw "not floating";

            if(this.upIsAttached)
                this.setDownstreamPos(x, y);
            else
                this.setUpstreamPos(x, y);
        };

        this.save = function() {
            return { up:   { node: this.upstream.id,   hook: this.upstreamIndex },
                     down: { node: this.downstream.id, hook: this.downstreamIndex } };
        };

        this.delete = function() {
            $(this.path).remove();
            if(this.upstream != null)
                this.upstream.removeConnector(false, this.upstreamIndex);
            if(this.downstream != null)
                this.downstream.removeConnector(true, this.downstreamIndex);
        };
    };

    this.list = [];

    this.create = function(node, isInput, index) {
        var connector = new Connector(node, isInput, index, this);
        this.list.push(connector);

        return connector;
    };

    this.delete = function(connector) {
        var index = this.list.indexOf(connector);
        if(index == -1)
            throw "node not found";

        this.list[index].delete();
        this.list = this.list.filter(function(_,i){ return i != index; });
    };

    this.clear = function() {
        this.list.forEach(function(c){ c.delete(); });
        this.list = [];
    };
};
