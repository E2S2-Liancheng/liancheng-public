//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
var NodeManager = function(commonCallbacks, addConnectorCallback, removeConnectorCallback) {

    var Node = function(type, id, $domElement) {
        this.id = id;

        this.$domElement = $domElement;
        $domElement.data("self", this);

        this.insertHere = function(parent, top, left) {
            $domElement.css("top", top)
                       .css("left", left)
                       .css("position", "absolute")                     // HACK some sort of fix for chrome
                       .appendTo(parent)
                       .trigger("wfInserted", [$domElement]);
        };

        // cheat: boolean is coerced into string when accessing the properties
        var connectors = { true: [], false: [] };

        this.removeHook = function(isInput, index) {
            var cs = connectors[isInput];
            if(cs[index] != null || cs.length < 1)
                return;

            var cs2 = [];
            for(var i = 0; i < cs.length; ++i)
                if(i != index)
                    cs2.push(cs[i]);

            cs = cs2;
        }

        this.addConnector = function(isInput, index, connector) {
            var cs = connectors[isInput];
            if(cs[index] != null) {
                if(cs[index] === connector)
                    return;
                else
                    throw "wrong index";
            }

            cs[index] = connector;

            if(addConnectorCallback != null)
                addConnectorCallback($domElement, isInput, index);
            //$domElement.trigger("wfConnectorAdded", [isInput, index]);
        }

        this.getConnector = function(isInput, index) {
            return connectors[isInput][index];
        }

        this.removeConnector = function(isInput, index) {
            var cs = connectors[isInput];
            if(cs[index] == null)
                throw "wrong index";

            cs[index] = null;

            if(removeConnectorCallback != null)
                removeConnectorCallback($domElement, isInput, index);
            //$domElement.trigger("wfConnectorRemoved", [isInput, index]);
            if(isInput)
                $domElement.trigger("wfDisconnected", [index]);
        };

        this.connected = function(index) {
            $domElement.trigger("wfConnected", [index]);
        };

        this.eachConnector = function(callback) {
            connectors[true].forEach(function(c,i){
                if(c != null)
                    callback(c, true, i);
            });
            connectors[false].forEach(function(c,i){
                if(c != null)
                    callback(c, false, i);
            });
        };

        this.save = function() {
            var info = { type:  type, id: id };
            $domElement.trigger("wfSave", [$domElement, info]);

            return info;
        };

        this.numberOutputs = function() {
            return connectors[false].length;
        }

        this.delete = function() {
            connectors[true].forEach(function(c){
                if(c != null)
                    c.manager.delete(c);
            });
            connectors[false].forEach(function(c){
                if(c != null)
                    c.manager.delete(c);
            });
            $domElement.remove();
        };

        this.trigger = function(index, event, parameters) {
            var connector = connectors[false][index];
            if(connector == null)
                return;

            var node = connector.downstream;
            if(node == null)
                return;

            parameters.unshift(connector.downstreamIndex);

            node.$domElement.trigger(event, parameters);
        };

        // counters are used to check whether the result of an AJAX call is still relevant

        var counters = {};

        this.getCounter = function(name) {
            if( !(name in counters))
                counters[name] = 0;
            return ++counters[name];
        };

        this.checkCounter = function(name) {
            if( !(name in counters))
                return 0;

            return counters[name];
        }

        commonCallbacks($domElement);
    };

    var nodeList = [];

    this.create = function(type, id, params, callback) {
        var form = { id: id };
        if(params)
            form["load"] = JSON.stringify(params);

        $.post("/workflow/node/" + type, form).done(function(data) {
            var $box = $(data);
            $box.draggable({handle: ".title_bar", scroll: true});

            if(type == "operator_projection") {
                var $sort = $box.find(".columns");
                $sort.sortable();
                $sort.disableSelection();
            }

            if(params && params["data"] && params["data"]["cols"])
                $box.data("cols", params["data"]["cols"]);
            else
                $box.data("cols", []);

            var node = new Node(type, id, $box);

            nodeList[id] = node;
            callback(node);
        });
    };

    this.delete = function(node) {
        nodeList[node.id] = null;
        node.delete();
    };

    this.get = function(index) {
        return nodeList[index];
    };

    this.forEach = function(callback) {
        Object.keys(nodeList).forEach(function(index){
            if(nodeList[index] != null)
                callback(nodeList[index]);
        });
    };

    this.map = function(callback) {
        return Object.keys(nodeList).filter(function(index){ return nodeList[index] != null; }).map(function(index){
            return callback(nodeList[index]);
        });
    };

    this.clear = function() {
        nodeList.forEach(function(n){ n.delete(); });
        nodeList = [];
    };
};
