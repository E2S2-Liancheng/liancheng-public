//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function() {
    var selector = ".wf_node[data-node-type='export_csv']";

    Globals.wfDesktop.on("wfSave", selector, onSave);

    Globals.wfDesktop.on("click", selector + " .button_export_csv_change", onExportCsv);

    function onSave(event, $domElement, info) {
        var $file = $domElement.find(".file_name");
        var $name = $file.find("[data-param-name='name_out']");
        var $dir = $file.find("[data-param-name='directory_id_out']");
        if($name.length > 0)
            info["data"] = { file: { name: $name.val(),
                                     directory_id: parseInt($dir.val()) } };
        return false;
    }

    function onExportCsv() {
        var $this = $(this);
        var $main = $this.closest(".contents");
        $this.prop("disabled", true);

        $.get("/workflow/file_picker", { save : "" }).done(function(data){
            var $html = $(data);
            $("body").append($html);
            var filePicker = new FilePicker($html)
                .ok(function(file){
                    $main.find("[data-param-name='name_out']").val(file["file_name"]);
                    $main.find("[data-param-name='directory_id_out']").val(file["directory_id"]);
                    $main.closest(".core").find(".title_bar_extra").text(" | " + file["file_name"]);
                })
                .finally(function(){
                    $this.prop("disabled", false);
                    $html.remove();
                });
        });
    }
});
