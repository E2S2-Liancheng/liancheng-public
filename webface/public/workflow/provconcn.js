//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function() {
    var selector = ".wf_node[data-node-type='operator_provconcn']";

    Globals.wfDesktop.on("wfSave", selector, onSave);

    Globals.wfDesktop.on("mouseenter mouseleave", selector + " .hooks img.hook_top", onHighlightSource);
    Globals.wfDesktop.on("wfUpdatedTitle", selector, onUpdatedSourceName);
    Globals.wfDesktop.on("wfDisconnected", selector, onDisconnected);
    Globals.wfDesktop.on("wfConnected", selector, onConnected);

    function onSave(event, $domElement, info) {
        info["state"] = { sources: $domElement.find(".operator_provconcn .sources").children().map(function(_,c){
                                       return $(c).children().eq(1).text();
                                   }).toArray() };

        info["data"] = { samples: parseInt($domElement.find(".samples input").val()) };

        return false;
    }

    function onHighlightSource(event) {
        var $hook = $(this);
        var num = parseInt($hook.attr("data-hook-num"));
        var $sources = $hook.closest(".wf_node").find(".operator_provconcn .sources");
        $sources.children()[num].classList.toggle("highlight");
        return false;
    }

    function onUpdatedSourceName(event, hookNum, name) {
        $(this).find(".sources").children().eq(hookNum).children().eq(1).text(name);
        return false;
    }

    function onDisconnected(event, hookNum) {
        $(this).find(".sources").children().eq(hookNum).children().eq(1).text("");
        return false;
    }

    function onConnected() {
        var cols = [ { name: "Toxin level", type: "string" },
                     { name: "Number",  type: "string" } ];
        $(this).data("self").trigger(0, "wfUpdatedCols", [cols]);
        return false;
    }
});
