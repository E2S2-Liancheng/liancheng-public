//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function() {
    var selector = ".wf_node[data-node-type='control_copy']";

    Globals.wfDesktop.on("wfSave", selector, onSave);

    Globals.wfDesktop.on("click", selector + " .buttons .add", onAddHook);
    Globals.wfDesktop.on("click", selector + " .buttons .remove", onRemoveHook);
    Globals.wfDesktop.on("wfUpdatedCols", selector, onUpdatedCols);
    Globals.wfDesktop.on("wfRenamedCol", selector, onRenamedCol);
    Globals.wfDesktop.on("wfConnected", selector, onConnected);

    function onSave(event, $domElement, info) {
        var cols = $domElement.data("cols");
        info["data"] = { cols : cols,
                         hooks : $domElement.find(".wf_output_zone .hooks .hook").length };

        return false;
    }

    function onAddHook(event) {
        var $node = $(this).closest(".wf_node");
        var $hooks = $node.find(".wf_output_zone .hooks");
        var $newHook = $node.find(".hidden_hook").children().clone();

        var index = $hooks.children().length;
        $newHook.find("img").attr("data-hook-num", index);
        $hooks.append($newHook);

        $newHook.trigger("wfNewHook");          // to add the relevant event handlers

        $node.find(".buttons .remove").prop("disabled", false);

        return false;
    }

    function onRemoveHook(event) {
        var $node = $(this).closest(".wf_node");
        var $hooks = $node.find(".wf_output_zone .hooks .hook");
        var self = $node.data("self");

        for(var i = $hooks.length - 1; i >= 0; --i)
            if(self.getConnector(false, i) == null)
                break;

        if(i < 0)
        {
            // TODO show a warning or something
            return false;
        }

        self.removeHook(false, i);

        $hooks.eq(i).remove();
        for(; i < $hooks.length - 1; ++i)
            $hooks.eq(i + 1).attr("data-hook-num", i);

        if($hooks.length == 3)
            $(this).prop("disabled", true);

        $node.trigger("wfRemovedHook");

        return false;
    }

    function onUpdatedCols(event, hookNum, cols) {
        var $node = $(this);
        $node.data("cols", cols);

        var self = $node.data("self");
        for(var i = 0; i < self.numberOutputs(); ++i)
            self.trigger(i, "wfUpdatedCols", [cols]);
        return false;
    }

    function onRenamedCol(event, hookNum, index, name) {
        var $node = $(this);
        $node.data("cols")[index]["name"] = name;

        var self = $node.data("self");
        for(var i = 0; i < self.numberOutputs(); ++i)
            self.trigger(i, "wfRenamedCol", [index, name]);
        return false;
    }

    function onConnected(event, index) {
        var $node = $(this);
        var cols = $node.data("cols");

        $node.data("self").trigger(index, "wfUpdatedCols", [cols]);
        return false;
    }
});
