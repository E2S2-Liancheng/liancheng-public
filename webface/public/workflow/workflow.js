//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
var Globals = {};

$(document).ready(function() {
    Globals.wfDesktop = $("#wf_desktop");

    /**************************************************************************
     * Node manager
     */

    var nodeManager = new NodeManager(
        function($domElement) {
            $domElement.on("mousedown", selectNode);
            $domElement.find(".hook img").on("mousedown", mouseDownOnHook);
            addResizeListener($domElement[0], spaceHooks);
        },
        function($domElement, isInput, index) {
            var selector = ".hook img.hook_" + (isInput ? "top" : "bottom") + "[data-hook-num='" + index + "']";
            $domElement.find(selector).attr("src", hookSrc.get(isInput, hookSrc.enabled));
        },
        function($domElement, isInput, index) {
            var selector = ".hook img.hook_" + (isInput ? "top" : "bottom") + "[data-hook-num='" + index + "']";
            $domElement.find(selector).attr("src", hookSrc.get(isInput, hookSrc.normal));
        }
    );

    var nextNodeIndex = 0;

    Globals.wfDesktop.on("wfNewHook", ".hook", onNewHook);      // add the necessary event handlers to the new hook

    function onNewHook(event) {
        var $img = $(event.target).find("img");
        $img.on("mousedown", mouseDownOnHook);

        return false;
    }

    // act upon hook removal (respace remaining ones)
    Globals.wfDesktop.on("wfRemovedHook", ".wf_node", spaceHooks);

    /**************************************************************************
     * Node manager
     */

    var connectorManager = new ConnectorManager();

    var currentConnector = null;


    /**************************************************************************
     * Workflow file information
     */

    var WorkflowFile = new function() {
        var $wf_main = $("#wf_main");

        this.inode_id = $wf_main.attr("data-wf-inode-id");
        this.revision = $wf_main.attr("data-wf-revision");
        this.name = $wf_main.attr("data-wf-name");

        this.updateStatus = function() {
            var text = (this.name != null) ? "File: " + this.name + ", Revision: " + this.revision : "New document";
            $("#wf_status").text(text);
        }
    }

    /**************************************************************************
     * Handle window resize events
     */

    function resizeSvg() {
        var width = $("#wf_desktop").prop("scrollWidth");
        var height = $("#wf_desktop").prop("scrollHeight");
        var viewBox = "0 0 " + (width - 1) + " " + (height - 1);

        $("#wf_svg").attr("width", width).attr("height", height);
    }
    $("#wf_desktop").on("scroll", resizeSvg);

    function maximiseHeight() {
        var newTr = $(window).height() - $("#header").outerHeight() - $("#footer").outerHeight();

        var paddingTd = $("#contents_td").innerHeight() - $("#contents_td").height();

        var newTd = newTr - paddingTd;

        var notMain = $("#wf_main").outerHeight() - $("#wf_main").height();
        var newMain = newTd - $("#wf_toolbar").outerHeight() - $("#wf_status").outerHeight() - notMain;

        $("#wf_nodes").height(newMain - 16);
        $("#wf_desktop").height(newMain - 3);

        resizeSvg();
    }

    maximiseHeight();
    $(window).on("resize", maximiseHeight);


    /**************************************************************************
     * Drag & drop nodes
     */

    $("#wf_nodes .toggle_opset").on("click", function(){
        $(this).closest(".opset").find(".opset_contents").slideToggle();
    });

    function handleDrop(event, ui) {
        var type = $(ui.draggable[0]).attr("data-node-type");

        var $dk = $("#wf_desktop");
        var top = Math.round(ui.offset.top - $dk.offset().top + $dk.scrollTop());
        var left = Math.round(ui.offset.left - $dk.offset().left + $dk.scrollLeft());

        nodeManager.create(type, nextNodeIndex++, null, function(node){
            node.insertHere($dk, top, left);
            spaceHooks.apply(node.$domElement);
        });
    }

    $(".opbox").draggable({containment: "#wf_main", revert: "invalid", helper: "clone", cursor: "pointer"});
    $("#wf_desktop").droppable({accept: ".opbox", hoverClass: "wf-state-hover", drop: handleDrop });


    /**************************************************************************
     * Remaining stuff
     */

    var hookSrc = new function() {
        this.normal = 0;
        this.enabled = 1;
        this.allowed = 2;
        this.forbidden = 3;

        this.get = function(isInput, state) {
            var src = "images/wf/hook_" + (isInput ? "top" : "bottom");
            switch(state) {
                case this.normal:       break;
                case this.enabled:      src += "_enabled"; break;
                case this.allowed:      src += "_allowed"; break;
                case this.forbidden:    src += "_forbidden"; break;
            }
            return src + ".png";
        }
    }

    function getHookPosition($hook) {
        var isTop = $hook.hasClass("hook_top");
        var $d = $("#wf_desktop");
        var wf_x = $d.scrollLeft() - $d.offset().left;
        var wf_y = $d.scrollTop() - $d.offset().top;
        var x = Math.round(wf_x + $hook.offset().left + $hook.outerWidth() / 2);
        var y = Math.round(wf_y + $hook.offset().top + (isTop ? $hook.outerHeight() : 0));
        return { left: x, top: y };
    }

    function getMousePosition(event) {
        var $d = $("#wf_desktop");
        var wf_x = $d.scrollLeft() - $d.offset().left;
        var wf_y = $d.scrollTop() - $d.offset().top;
        var x = Math.round(wf_x + event.pageX);
        var y = Math.round(wf_y + event.pageY);
        return { left: x, top: y };
    }

    function spaceHooks() {
        var $node = $(this);
        var $hooks = $node.find(".hooks");
        var $core = $node.find(".core");

        $hooks.css("min-width", 0);
        $hooks.css("min-width", $core.outerWidth());

        repositionConnectors.apply(this);
        return false;
    }

    function restoreHook($hook) {
        var hookIndex = parseInt($hook.attr("data-hook-num"));
        var isInput = $hook.hasClass("hook_top");
        var hooked = $hook.closest(".wf_node").data("self").getConnector(isInput, hookIndex) != null;

        $hook.attr("src", hookSrc.get(isInput, hooked ? hookSrc.enabled : hookSrc.normal));
    }

    function mouseDownOnHook(event) {
        if(event.which != 1)
            return false;

        var $hook = $(event.target);
        var isInput = $hook.hasClass("hook_top");
        var hookIndex = parseInt($hook.attr("data-hook-num"));
        var $this = $hook.closest(".wf_node");
        var node = $this.data("self");

        currentConnector = node.getConnector(isInput, hookIndex);
        if(currentConnector != null) {
            currentConnector.setFloating(isInput);
            return false;
        }

        currentConnector = connectorManager.create(node, isInput, hookIndex);
        node.addConnector(isInput, hookIndex, currentConnector);

        var hookPos = getHookPosition($hook);
        var mousePos = getMousePosition(event)

        currentConnector.setPos(isInput, hookPos.left, hookPos.top);
        currentConnector.setFloatingPos(mousePos.left, mousePos.top);

        $("#wf_svg").append(currentConnector.path);
        return false;
    }

    function isLegitConnection($hook) {
        var hookIndex = parseInt($hook.attr("data-hook-num"));
        var isInput = $hook.hasClass("hook_top");
        var $this = $hook.closest(".wf_node");

        // if the current hook is of the same type than the starting hook, stop
        if(currentConnector.upIsAttached != isInput)
            return false;

        // if the current hook is already connected, stop
        var alreadyThere = $this.data("self").getConnector(isInput, hookIndex);
        if(alreadyThere != null && alreadyThere != currentConnector)
            return false;

        // TODO check for loops

        return true;
    }

    $("#wf_desktop").on("mouseenter", ".hook img", function(event) {
        if(currentConnector == null)
            return;

        var $hook = $(event.target);
        var isInput = $hook.hasClass("hook_top");

        $hook.attr("src", hookSrc.get(isInput, isLegitConnection($hook) ? hookSrc.allowed : hookSrc.forbidden));
    });

    $("#wf_desktop").on("mouseleave", ".hook img", function(event) {
        if(currentConnector == null)
            return;

        restoreHook($(event.target));
    });

    $("#wf_desktop").on("mousemove", function(event) {
        if(currentConnector == null)
            return;

        var mousePos = getMousePosition(event);
        currentConnector.setFloatingPos(mousePos.left, mousePos.top);
    });

    $("#wf_desktop").on("mouseup", ".hook img", function(event) {
        if(currentConnector == null)
            return;

        var $hook = $(event.target);
        var isInput = $hook.hasClass("hook_top");

        if(isLegitConnection($hook)) {
            var hookIndex = parseInt($hook.attr("data-hook-num"));
            var node = $hook.closest(".wf_node").data("self");
            var hookPos = getHookPosition($hook);
            currentConnector.setNode(node, isInput, hookIndex, false);
            currentConnector.setPos(isInput, hookPos.left, hookPos.top);
        }
        else
            connectorManager.delete(currentConnector);

        restoreHook($hook);
        currentConnector = null;
        return false;
    });

    $(window).on("mouseup", function(event) {
        if(currentConnector == null)
            return;

        connectorManager.delete(currentConnector);
        currentConnector = null;
        return false;
    });

    $("#wf_desktop").on("drag dragstop", ".wf_node", repositionConnectors);

    function repositionConnectors() {
        var $node = $(this);
        var node = $node.data("self");
        node.eachConnector(function(connector, isInput, index){
            var selector = "img.hook_" + (isInput ? "top" : "bottom") + "[data-hook-num='" + index + "']";
            var pos = getHookPosition($node.find(selector));
            connector.setPos(isInput, pos.left, pos.top);
        });
    }


    /**************************************************************************
     * Element (de)selection and deletion
     */

    var selectedElement = null;
    var preparedToDelete = false;

    function selectNode(event) {
        event.stopPropagation();

        // TODO should it be first?
        if(event.which != 1)
            return;

        if(selectedElement != null)
            selectedElement[0].classList.remove("selected");

        this.classList.add("selected");
        selectedElement = $(this);
    }

    $("#wf_desktop").on("mousedown", "path", selectNode);

    $("#wf_desktop").on("mousedown", function(event) {
        if(event.which != 1)
            return;

        if(selectedElement != null) {
            selectedElement[0].classList.remove("selected");
            selectedElement = null;
        }
    });


    var keyDownAlreadyCaught = [];

    $(document).on("keydown", "textarea, input", function(event) {
        if(event.which == 46)
            event.stopImmediatePropagation();
    });

    $(document).on("keydown", function(event) {
        if(keyDownAlreadyCaught[event.which] == true)
            return;
        keyDownAlreadyCaught[event.which] = true;

        if(event.which == 46)
            preparedToDelete = true;
        else if(event.which == 27)
            preparedToDelete = false;
    });

    $(document).on("keyup", function(event) {
        keyDownAlreadyCaught[event.which] = false;

        if(event.which == 46 && preparedToDelete && selectedElement != null) {
            var element = selectedElement.data("self");

            if(selectedElement.hasClass("wf_node"))
                nodeManager.delete(element);
            else if(selectedElement.is("path"))
                connectorManager.delete(element);

            selectedElement = null;
            preparedToDelete = false;
        }
    });


    /**************************************************************************
     * Load / save / serialisation
     */

    function tb_clear() {
        nextNodeIndex = 0;
        selectedElement = null;
        currentConnector = null;
        nodeManager.clear();
        connectorManager.clear();
    }

    $("#tb_new").on("click", function(){
        tb_clear();

        $.post("/workflow/new");

        WorkflowFile.name = null;
        WorkflowFile.inode_id = null;
        WorkflowFile.revision = null;
        WorkflowFile.updateStatus();
    });

    function tb_load() {
        $("#wf_spinner").show();

        var params = { inode_id: WorkflowFile.inode_id, revision: WorkflowFile.revision };
        $.get("/workflow/load", params).done(function(data){
            loadFile = JSON.parse(data["file_load"]);
            if(loadFile["meta"]["app"] != "UrbanDB") {
                $("#wf_spinner").hide();
                return;
            }

            var dk = $("#wf_desktop");
            var svg = $("#wf_svg");

            function loadConnectors() {
                loadFile["connectors"].forEach(function(conn) {
                    var uNode = nodeManager.get(conn["up"]["node"]);
                    var connector = connectorManager.create(uNode, false, conn["up"]["hook"]);

                    var dNode = nodeManager.get(conn["down"]["node"]);
                    connector.setNode(dNode, true, conn["down"]["hook"], true);

                    var $uHook = uNode.$domElement.find("img.hook_bottom[data-hook-num='" + conn["up"]["hook"] + "']");
                    var uPos = getHookPosition($uHook);
                    connector.setPos(false, uPos.left, uPos.top);

                    var $dHook = dNode.$domElement.find("img.hook_top[data-hook-num='" + conn["down"]["hook"] + "']");
                    var dPos = getHookPosition($dHook);
                    connector.setPos(true, dPos.left, dPos.top);

                    svg.append(connector.path);
                });
            }
            var remainingNodes = loadFile["nodes"].length;

            loadFile["nodes"].map(function(n) {
                if(nextNodeIndex <= n["id"])
                    nextNodeIndex = n["id"] + 1;

                var params = { data: n["data"],
                               state: n["state"] };

                nodeManager.create(n["type"], n["id"], params, function(node){
                    node.insertHere(dk, n["view"]["top"], n["view"]["left"]);
                    if(n["view"]["tiex"])
                        node.$domElement.find(".title_bar_extra").text(n["view"]["tiex"]);
                    spaceHooks.apply(node.$domElement);
                    if(--remainingNodes == 0) {
                        loadConnectors();
                        $("#wf_spinner").hide();
                    }
                });
            });
        });
    }

    $("#tb_load").on("click", function() { tb_clear(); tb_load(); });

    function serialize_workflow() {
        var dt = $("#wf_desktop");
        var dt_x = dt.scrollLeft() - dt.offset().left;
        var dt_y = dt.scrollTop() - dt.offset().top;

        var meta = { app: "UrbanDB",
                     version: 1 };

        var nodes = nodeManager.map(function(n){
            var node = n.save();
            var $e = n.$domElement;
            node["view"] = { left: Math.round($e.position().left + $e.parent().scrollLeft()),
                             top:  Math.round($e.position().top  + $e.parent().scrollTop()) };
            var titleExtra = $e.find(".title_bar_extra").text();
            if(titleExtra.length > 0)
                node["view"]["tiex"] = titleExtra;
            return node;
        });

        var connectors = connectorManager.list.map(function(c){ return c.save(); });
        return JSON.stringify({ meta: meta, nodes: nodes, connectors: connectors });
    }

    function tb_save(success_callback) {
        if(!WorkflowFile.inode_id)
            tb_save_as(success_callback);
        else
            $.post("/workflow/save", { inode_id: WorkflowFile.inode_id, save_file: serialize_workflow() })
                .done(function(data){
                    if(data["status"] == "ok")
                        WorkflowFile.revision = data["revision"];
                    WorkflowFile.updateStatus();
                    success_callback();
                })
    }

    function tb_save_as(success_callback) {
        $.get("/workflow/file_picker", { save : "" }).done(function(data){
            var html = $(data);
            $("body").append(html);
            var filePicker = new FilePicker(html)
                .ok(function(data){
                    var params = { directory_id: data["directory_id"],
                                    file_name: data["file_name"],
                                    save_file: serialize_workflow() };
                    $.post("/workflow/save_new", params)
                        .done(function(data){
                            if(data["status"] == "ok") {
                                WorkflowFile.inode_id = data["inode_id"];
                                WorkflowFile.revision = data["revision"];
                                WorkflowFile.name = data["name"];
                                WorkflowFile.updateStatus();
                                success_callback();
                            }
                            else
                                alert(data["err_msg"]);
                        });
                })
                .finally(function(){
                    html.remove();
                });
        });
    }

    $("#tb_save").on("click", tb_save);

    $("#tb_save_as").on("click", tb_save_as);

    // if we must load a file at startup
    if(WorkflowFile.inode_id) {
        tb_load();
        WorkflowFile.updateStatus();
    }


    function tb_run() {
        if(WorkflowFile.inode_id == null) {
            alert("Please save the workflow first");
            return;
        }

        var onSave = function(){
            $.post("/workflow/run", { inode_id: WorkflowFile.inode_id, revision: WorkflowFile.revision })
                .done(function(data){
                    if(data["status"] == "err")
                        alert(data["err_msg"]);
                    if(data["status"] == "ok")
                        location.href = "/jobs";
                })
                .fail(function(){
                    alert("Unexpected server-side error");
                });
        };

        tb_save(onSave);
    }

    $("#tb_run").on("click", tb_run);
});
