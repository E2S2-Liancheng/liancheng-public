//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
function encode(address) {
    chars = []
    for(var i = 0; i < address.length; ++i)
        chars.push(address.charCodeAt(i));
    document.write(chars);
}

function decode(address) {
    return String.fromCharCode.apply(null, address);
}

function print(address) {
    var address = decode(address);
    document.write("<a href='mailto:" + address + "'>" + address + "</a>");
}
