//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function(){
    $(".pending_buttons > .validate").on("click", function(){
        var $this = $(this).parent().parent();

        $this.find("button").prop("disabled", true);

        $.post('/admin/validate_pending', { pending_id: $this.attr("data-id") })
            .done(function(){
                $this.remove();
            })
            .fail(function(){
                $this.find("button").prop("disabled", false);
        });
    });

    $(".pending_buttons > .reject").on("click", function(){
        var $this = $(this).parent().parent();

        $this.find("button").prop("disabled", true);

        $.post('/admin/reject_pending', { pending_id: $this.attr("data-id") })
            .done(function(){
                $this.remove();
            })
            .fail(function(){
                $this.find("button").prop("disabled", false);
        });
    });
});
