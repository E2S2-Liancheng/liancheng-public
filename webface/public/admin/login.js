//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function() {
    var tf = $("#username");
    if(tf.val() == "")
        tf.focus();
    else
        $("#password").focus();

    var $form = $("#login_form");

    $form.on("submit", function(){
        if($("#username").val() == "") {
            alert("Please enter an email address");
            return false;
        }

        if($("#password").val() == "") {
            alert("Please enter a password");
            return false;
        }

        $("#modal_background").css("display", "block").css("cursor", "wait");

        $("#hashed_password").val(CryptoJS.SHA256($("#password").val()));
    });
})
