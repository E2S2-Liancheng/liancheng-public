//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
$(document).ready(function(){
    $("#table_jobs a.workflow").on("click", postTo);
    $("#table_jobs button.visu").on("click", postTo);

    function postTo(event) {
        var $this = $(event.target);

        var action = null;
        if($this.is("a.workflow"))
            action = "/workflow";
        else if($this.is("button.visu"))
            action = "/visu";

        var myForm = document.createElement("form");
        myForm.method = "post";
        myForm.action = action;
        myForm.className = "hidden";

        var myInput = document.createElement("input") ;
        myInput.setAttribute("name", "inode_id");
        myInput.setAttribute("value", $this.attr("data-inode-id"));
        myForm.appendChild(myInput);

        var myInput = document.createElement("input") ;
        myInput.setAttribute("name", "revision");
        myInput.setAttribute("value", $this.attr("data-revision"));
        myForm.appendChild(myInput);

        document.body.appendChild(myForm);
        myForm.submit();
    }

    window.setTimeout(function(){ if($("span.created, span.started").length > 0) window.location.reload(true); }, 2000);
});
