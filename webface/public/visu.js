//
// Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
var DisplayFile = function(data) {

    // var palette = new Rickshaw.Color.Palette( { scheme: 'classic9' } );
    var palette = new Rickshaw.Color.Palette( { scheme: 'colorwheel' } ); //classic9 looks good for more than 6 items; for less than 3, colorwheel is good; cool

    var series = [];

    for(var j = 0; j < data["values"].length; j++)
        data["values"][j][0] = Math.round(Date.parse(data["values"][j][0]) / 1000, 0);

    for(var i = 1; i < data["header"].length; ++i) {
        var serie = [];
        for(var j = 0; j < data["values"].length; j++) {
            serie.push({ x: data["values"][j][0], y: parseFloat(data["values"][j][i]) });
        }

        series.push({ color: palette.color(), data: serie, name: data["header"][i] });
    }

    // instantiate our graph!

    var graph = new Rickshaw.Graph( {
            element: document.getElementById("chart"),
            width: 900,
            height: 500,
            renderer: 'area',
            stroke: true,
            preserve: true,
            series: series
    } );

    graph.render();

    var preview = new Rickshaw.Graph.RangeSlider( {
            graph: graph,
            element: document.getElementById('preview'),
    } );

    var hoverDetail = new Rickshaw.Graph.HoverDetail( {
            graph: graph,
            xFormatter: function(x) {
                    return new Date(x * 1000).toString();
            }
    } );

    var annotator = new Rickshaw.Graph.Annotate( {
            graph: graph,
            element: document.getElementById('timeline')
    } );

    var legend = new Rickshaw.Graph.Legend( {
            graph: graph,
            element: document.getElementById('legend')

    } );

    var shelving = new Rickshaw.Graph.Behavior.Series.Toggle( {
            graph: graph,
            legend: legend
    } );

    var order = new Rickshaw.Graph.Behavior.Series.Order( {
            graph: graph,
            legend: legend
    } );

    var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight( {
            graph: graph,
            legend: legend
    } );

    var smoother = new Rickshaw.Graph.Smoother( {
            graph: graph,
            element: document.querySelector('#smoother')
    } );

    var ticksTreatment = 'glow';

    var xAxis = new Rickshaw.Graph.Axis.Time( {
            graph: graph,
            ticksTreatment: ticksTreatment,
            timeFixture: new Rickshaw.Fixtures.Time.Local()
    } );

    xAxis.render();

    var yAxis = new Rickshaw.Graph.Axis.Y( {
            graph: graph,
            tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
            ticksTreatment: ticksTreatment
    } );

    yAxis.render();


    var controls = new RenderControls( {
            element: document.querySelector('form'),
            graph: graph
    } );
}

jQuery(document).ready(function() {
    var $ = jQuery;

    $.post("/visu/csv", { inode_id: fileInode, revision: fileRevision })
        .done(function(data){
            DisplayFile(data["contents"]);
            $("#loading").hide();
        })
        .fail(function(){
            $("#loading").text("Loading failed.");
        });
});
