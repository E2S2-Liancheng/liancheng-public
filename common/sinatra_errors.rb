#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

enable :raise_errors, :dump_errors
disable :show_exceptions


# Generic case, just log the exception's name and backtrace and return a 500 error code
#
error do
  $logger.warn(env['sinatra.error'].msg_bt)
end


# Wrong parameters when calling an URL
#
class BadRequestError < StandardError
end

helpers do
  def param_value(name)
    params[name].tap { |v| raise BadRequestError, "Can't find parameter \"#{name}\"" if v.nil? }
  end

  def param_non_empty(name)
    param_value(name).tap { |v| raise BadRequestError, "Parameter #{name} is an empty String" if v.empty? }
  end

  def param_to_int(name)
    Integer(param_value(name))
  rescue ArgumentError
    raise BadRequestError, "Can't convert parameter \"#{name}\"'s value \"#{params[name]}\" to an Integer"
  end

  def param_to_array(name)
    param_value(name).tap { |v| raise BadRequestError, "Parameter \"#{name}\" is not an Array" unless v.is_a?(Array) }
  end

  def param_to_array_ints(name)
    a = param_value(name)
    raise BadRequestError, "Parameter \"#{name}\" is not an Array" unless a.is_a?(Array)
    a.map { |v| Integer(v) }
  rescue ArgumentError
    raise BadRequestError, "Can't convert one of parameter \"#{name}\"'s values \"#{params[name]}\" to an Integer"
  end
end

error BadRequestError do
  $logger.error(env['sinatra.error'].msg_bt)
  status 400
end


# The user tries to access a resource he should not be aware of
# (could happen if he is removed from a shared directory, so should only be informative)
#
class AccessRightsError < StandardError
  def initialize(object_type, object_id, member_id)
    super("Unauthorised access to #{object_type} ##{object_id} from member ##{member_id}")
  end
end

error AccessRightsError do
  $logger.error(env['sinatra.error'].msg_bt)
  status 403
end


# The user tries to access a resource that does not exist
#
class ObjectNotFoundError < StandardError
  def initialize(object_type, object_id, member_id)
    super("Could not find #{object_type} ##{object_id} for member ##{member_id}")
  end
end

error ObjectNotFoundError do
  $logger.error(env['sinatra.error'].msg_bt)
  status 404
end
