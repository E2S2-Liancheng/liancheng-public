#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
API_ERROR_CODES = {
  :err_dir_ro => 'The directory is read-only',
  :err_dir_ren_root => 'Cannot rename the home folder',
  :err_dir_ren_share => 'Cannot rename a shared folder',
  :err_dir_ren_already => 'The name already exists',
  :err_dir_del_home => 'Cannot delete home directory',
  :err_dir_del_share => 'Cannot delete shared directory',
  :err_dir_del_desc_share => 'Cannot delete a directory containing a shared directory',

  :err_member_name_exists => 'This name is already registered in the system',
  :err_member_email_exists => 'This email address is already registered in the system',
  :err_member_inc_old_pass => 'The member\'s old password is incorrect',
  :err_member_username_exists => 'This username is already registered in the system',

  :err_share_nested_share => 'Cannot nest shared directories',
  :err_share_not_shared_with => 'The directory isn\'t shared with this resource',

  :err_invite_already_used => 'The share invitation was already acted upon',

  :err_inode_ro => 'The file is read-only',
  :err_inode_ren_already => 'The name already exists',

  :err_wf_csv_parse => 'Error while trying to parse the file as a CSV input',

  :err_paste_name_clash => 'A folder and a file have the same',
  :err_paste_both_shared => 'Two folders are shared and cannot be combined',
  :err_paste_src_shared => 'The source folder is shared and cannot be combined',
  :err_paste_dst_shared => 'The destination folder is shared and cannot be combined',

  :err_job_disabled => 'Job running has been disabled by the system administrator',
  :err_job_wf_parse => 'Error while trying to parse the workflow',
  :err_job_wf_evaluate => 'Error while trying to evaluate the workflow',
  :err_job_creation => 'Error while creating the job'
}
