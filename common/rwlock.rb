#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'thread'

module RwLock

  READ_ONLY = false
  READ_WRITE = true

  class Locker
    def initialize(rights)
      @queue = Queue.new
      @rights = rights
    end

    attr_reader :queue, :rights
  end

  class RwMutex
    def initialize
      @submit_queue = Queue.new
      @main_lock = Mutex.new

      @waiting = []
      @running = []
      @current = nil

      Thread.new do
        loop do
          lock = @submit_queue.pop

          @main_lock.synchronize do
            if @current == nil || @current == READ_ONLY && lock.rights == READ_ONLY && @waiting.empty?
              @current = lock.rights
              @running << lock
              lock.queue.push(1)
            else
              @waiting << lock
            end
          end
        end
      end
    end

    def clean_mutex(lock)
      @main_lock.synchronize do
        @running.delete(lock)

        if @running.empty?
          if @waiting.empty?
            @current = nil
          else
            @running << @waiting.shift
            @current = @running[0].rights
            if @current == READ_ONLY
              @running << @waiting.shift while !@waiting.empty? && @waiting[0].rights == READ_ONLY
            end
            @running.each { |r| r.queue.push(1) }
          end
        end
      end
    end

    def synchronize(type, &block)
      l = Locker.new(type)
      @submit_queue.push l
      l.queue.pop

      begin
        yield
      ensure
        clean_mutex(l)
      end
    end
  end

end
