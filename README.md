# Disclaimer

This program is funded by Singapore's [National Research Fundation](http://www.nrf.gov.sg/)
under the [E2S2](http://www.create.edu.sg/about-create/research-centres/e2s2)
(Energy and Environmental Sustainability Solutions for Megacities) program.

# Overview

The directories are:

* common/ contains common files (seriously) between front-end and back-end
* database/ contains scripts for the database
* images/ contains the sources of the displayed images
* mashup/ contains the back-end server
* operators/ contains the necessary code for each (internal) operator
* pipe/ contains the stream server
* scripts/ contains sysvinit scripts
* webface/ contains the front-end server

# Installation
You need to follow the few following steps to have a working installation.

## System

    # apt-get install vim git build-essential bzip2 curl gawk sqlite3 \
                        libgdbm-dev libncurses5-dev bison pkg-config libffi-dev

## RVM
[RVM](https://rvm.io/) manages different Ruby versions and their Gems
(libraries). It must be installed within Bash, and takes the control over
some ruby-specific commands (`ruby`, `gem`, etc.). It might require the
installation of some dev packages:

    $ gpg --keyserver hkp://keys.gnupg.net --recv-keys \
                                       409B6B1796C275462A1703113804BB82D39DC0E3
    $ curl -sSL https://get.rvm.io | bash -s stable
    $ source /home/thomas/.rvm/scripts/rvm             # first time RVM load
    $ rvm autolibs fail                                # because no sudo
    $ rvm install 2.2.1
    # apt-get install whatever-is-still-missing
    $ rvm install 2.2.1
    $ rvm use 2.2.1 --default

Because RVM modifies some environment variables, you might need to logout
from your X session then login again to have them taken into account.
*Typically RVM works only in the current terminal, and will fail in a new
tab or a new window until the next login.*

## PostgreSQL
All the data is stored in PostgreSQL, you must first install it with all the
necessary dependencies for development:

* Ubuntu 13.10:
  * postgresql-contrib-9.1 (for pgcrypto)
  * libpq-dev (for building the ruby pg gem)
  * pgadmin3 (or not)
* CentOS 6.5:
  * postgresql91-contrib
  * postgresql91-devel
* Debian 7.8:
  * postgresql-contrib
  * libpq-dev

Once PostgreSQL is installed and setup, add a Login Role called thomas (edit
the SQL scripts to put another name). The database will belong to that user.
Then import the database:

    $ cd /wherever_the_app_root/database
    $ sudo -u postgres psql
    -or- su postgres -c psql

    postgres=# CREATE ROLE thomas LOGIN NOSUPERUSER INHERIT NOCREATEDB
                                  NOCREATEROLE NOREPLICATION
                                  PASSWORD 'choose a password';
    postgres=# \i create_pg.sql
    postgres=# INSERT INTO admin( name, email, username, password )
                   VALUES( 'Admin', 'admin@nus.edu.sg', 'admin',
                           crypt( encode(digest('password', 'sha256'), 'hex'),
                                  gen_salt('bf', 14)) );

*You must choose a password.* The commands are available in the
*create_roles.sql* and *insert_admin.sql* files.

## SQLite

The system uses SQLite as auxiliary RDBMS:

    # apt-get install libsqlite3-dev

## Bundler
[Bundler](http://bundler.io/) is used to store the list of gems necessary for
the system. It should install all dependencies automatically:

    # apt-get install whatever-is-missing (depends on gem C dependencies)
    $ cd /wherever_the_app_root
    $ gem install bundler
    $ bundle install
    $ PATH=$PATH:/usr/pgsql-9.1/bin bundle install      # for CentOS

## Nginx
To use Nginx as a reverse proxy:

    upstream webface {
        server 127.0.0.1:4567;
    }

    upstream mashup {
        server 127.0.0.1:5678;
    }

    limit_conn_zone $binary_remote_addr zone=objects:1m;

    server {
        root /home/webface/E2S2-datastore/webface/public;
        listen 80;

        client_max_body_size 16G;

        location / {
            try_files $uri @app;
        }

        location ~ /object/(?<uuid>.*) {
            limit_conn objects 1;

            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_redirect off;
            proxy_buffering off;

            proxy_pass http://mashup/publish/obfuscated/$uuid;
        }

        error_page 502 /500.html;
            location /500.html {
        }

        location @app {
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_redirect off;

            proxy_pass http://webface;
        }
    }

## Puma (web server) from an init script
Files are in /scripts/init/. To wrap puma and pumactl in an executable script:

    $ rvm wrapper current bootup puma
    $ rvm wrapper current bootup pumactl

On Debian, for webface:

    # update-rc.d e2s2-webface defaults

## Mashup (back-end server)

### Production
For production use, the init script should be used to start *puma* as a daemon.
It uses *config/puma.rb* by default.

Sinatra expects a configuration file called *main.conf*, based on the provided
*main.conf.default*.

The *self* section is **ignored**.

The *database* section contains settings to connect to the database. It must be
present and correct, otherwise the server won't start.

Create the missing folders:

    # mkdir /var/lib/e2s2-mashup
    $ mkdir /var/lib/e2s2-mashup/files
    ...

Once *main.conf* is ready, the server can be started:

    $ cd /wherever_the_app_root/mashup
    $ cp main.conf.default main.conf
    $ vim main.conf
    # service e2s2-mashup start

### Development
It expects a configuration file called *main.conf*, based on the provided
*main.conf.default*.

The *self* section contains settings for the web server. If this section is not
present, Sinatra will use its default settings (automatically selecting a web
server and making it listen on 127.0.0.1:4567).

The *database* section contains settings to connect to the database. It must be
present and correct, otherwise the server won't start.

Once main.conf is ready, the server can be started:

    $ cd /wherever_the_app_root/mashup
    $ cp main.conf.default main.conf
    $ vim main.conf
    $ ./main.rb

Alternatively, it is possible to simply run:

    $ puma

## Webface (front-end)

### Explorer project
The code for displaying the file explorer is in a project called elFinder, and
must be installed so:

    $ cd /wherever_the_app_root/webface
    $ git clone git@github.com:white-monkey/elFinder.git elfinder
    $ cd elfinder
    $ git checkout 2.1
    $ git remote add upstream git@github.com:Studio-42/elFinder.git

For production, it is enough to pull the *access_control* branch:

    $ cd /wherever_the_app_root/webface
    $ git clone git@github.com:white-monkey/elFinder.git -b access_control elfinder

### Production (stand-alone)
For production use, the init script should be used to start *puma* as a daemon.
It uses *config/puma.rb* by default.

Sinatra expects a configuration file called *main.conf*, based on the provided
*main.conf.default*.

The *self* section is **ignored**.

The *auth* section declares the values for the HTTP basic access authentication
scheme. If the section is missing, the authentication scheme will be disabled.
This is a first layer of security, the username and password will be shared
between all users.

The *features* section contains miscellaneous features for the server. The use
of *intrusive_versioning* is explained below.

The *mashup* section contains settings to connect to the back-end server. If
not present or incorrect, only the login page will be visible (and won't even
work).

Once *main.conf* is ready, the server can be started:

    $ cd /wherever_the_app_root/webface
    $ cp main.conf.default main.conf
    $ vim main.conf
    # service e2s2-webface start

### Development
It expects a configuration file called *main.conf*, based on the provided
*main.conf.default*.

The *self* section contains settings for the web server. If this section is not
present, Sinatra will use its default settings (automatically selecting a web
server and making it listen on 127.0.0.1:4567).

The *auth* section declares the values for the HTTP basic access authentication
scheme. If the section is missing, the authentication scheme will be disabled.
This is a first layer of security, the username and password will be shared
between all users.

The *mashup* section contains settings to connect to the back-end server. If
not present or incorrect, only the login page will be visible (and won't even
work).

Once main.conf is ready, the server can be started:

    $ cd /wherever_the_app_root/webface
    $ cp main.conf.default main.conf
    $ vim main.conf
    $ ./main.rb

Alternatively, it is possible to simply run:

    $ puma

## Running jobs
In order to run jobs, Mashup and PostgreSQL must be able to share files. All
the file names provided to PostgreSQL must contain an absolute path, which
means that Mashup's *main.conf* must contain absolute paths:

    "storage_file": {
        "root_dir": "/var/lib/e2s2-mashup/files"
    },
    "jobs" : {
        "enabled": true,
        "result_dir": "/var/lib/e2s2-mashup/jobs_results"
    },

It is mandatory to set *jobs/enabled* to *true*, otherwise Mashup will refuse
to run jobs.

### ACLs (Shouldn't be necessary, but kept here for reference)

The best way to allow read-only access to the files is by using an ACL (or so
I hope, it would have to be tested for recursion). First command is in case
Mashup creates files that are 0600 instead of 0640. Second command obviously
adds postgres in the mashup group:

    # setfacl -m d:g:mashup:r files
    # usermod -a -G mashup postgres

Then PostgreSQL must be able to write the result files and Mashup must be able
to move them back into the files repository:

    # chmod g+rwx jobs_results/
    # setfacl -m d:u:mashup:r jobs_results/

## Tips and tricks

### Very slow upload of large files

First check if it gets better when disabling TCP SACK:

    # echo 0 > /proc/sys/net/ipv4/tcp_sack

For persistence after reboot, add in */etc/sysctl.conf*:

    net.ipv4.tcp_sack = 0

### Intrusive versioning

In order to solve browser cache issues (namely, that some static files do not
get refreshed automatically when they are updated), webface can either append
a version number in the form of a request parameter or insert a version number
in the file name:

    ./login.js                                          # becomes either:
    ./login.js?v=b6e8e0656b0391f3ed42d53e4888f1f5       # non-intrusive
    ./login.b6e8e0656b0391f3ed42d53e4888f1f5.js         # intrusive

By default, the non-intrusive method is used. Its advantage is that the file
name itself is not modified. Its inconvenient is that the browser will
systematically try to get a new version at each page load (to which the
reverse-proxy will reply with a 304 message).

The intrusive method does not bear this inconvenient, the browser will only ask
for a new file if the name changes, it can be activated by setting the
*main.conf*'s *features* *intrusive_versioning* to `true`. It also requires an
extra directive in nginx to rewrite the URLs, to be placed in the `server`
section before any `location` directive:

    rewrite '^(/.*\.)[0-9a-f]{32}\.(js|css)$' $1$2 last;
