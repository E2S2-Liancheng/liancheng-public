#!/usr/bin/env python3
#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# -*- coding: utf-8 -*-

import sys
import csv
import simplekml

from dateutil import parser

try:
    sysi=sys.argv[sys.argv.index('-i')+1]
    syso=sys.argv[sys.argv.index('-o')+1]
    sysLat=sys.argv[sys.argv.index('-lat')+1]
    sysLong=sys.argv[sys.argv.index('-long')+1]
    sysTime=sys.argv[sys.argv.index('-time')+1]
    sysPort=sys.argv[sys.argv.index('-port')+1]
except:
    print("Please input correct arguments: -i input CSV file, -lat Latitude header name, -long Longitude header name, -time Timestamp header name, -port PortCode header name, -o output KML file")
    sys.exit()

pols=[]
dts=[]
pnts=[]
kml = simplekml.Kml()
#Latitude,Longitude,Timestamp,
with open(sysi, 'rt') as f:
    reader = csv.DictReader(f)
    csvf=reader.fieldnames
    for row in reader:
        pols.append((float(row[sysLong]),float(row[sysLat])))
        dts.append(parser.parse(row[sysTime]).isoformat())
        #for unwanted_key in [sysLat,sysLong,sysTime]: del row[unwanted_key]
        if len(row[sysPort])>0:
            pnts.append(row[sysPort])
            pnt = kml.newpoint(name=pnts[-1], coords=[pols[-1]])
            pnt.style.iconstyle.scale = 1.0
            pnt.style.labelstyle.scale = 1
            #TO CHANGE IMAGE, JUST REPLACE THE STRING WITH ANOTHER FILE OR URL
            pnt.style.iconstyle.icon.href='anchor.png'
            pnt.description=str(row)#'\n'.join(list(row.values()))



ls = kml.newlinestring(name='A LineString')
ls.coords=pols
ls.extrude = 1
ls.altitudemode = simplekml.AltitudeMode.relativetoground
ls.style.linestyle.width = 5
ls.style.linestyle.color = simplekml.Color.blue
trk=kml.newgxtrack(name='ShipMovement')
trk.newwhen(dts) # Each item in the give nlist will become a new <when> tag
trk.newgxcoord(pols)
trk.altitudemode = simplekml.AltitudeMode.relativetoground
#TO CHANGE IMAGE, JUST REPLACE THE STRING WITH ANOTHER FILE OR URL
trk.stylemap.normalstyle.iconstyle.icon.href = 'ship.png'
trk.stylemap.normalstyle.iconstyle.icon.scale=2.0
trk.stylemap.normalstyle.linestyle.color = '99ffac59'
trk.stylemap.normalstyle.linestyle.width = 11
trk.stylemap.highlightstyle.iconstyle.icon.href = 'ship.png'
trk.stylemap.highlightstyle.iconstyle.scale = 3
trk.stylemap.highlightstyle.linestyle.color = '99ffac89'
trk.stylemap.highlightstyle.linestyle.width = 17
kml.save(syso)



