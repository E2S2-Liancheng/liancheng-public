#!/usr/bin/env ruby
#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require 'csv'
#require 'active_support'
require 'date'
require 'slop'

# read command line
opts = Slop.parse do |o|
  o.string '-i', '--input', 'CSV input file'
  o.string '-o', '--output', 'CSV output file'
  o.string '-l', '--latitude', 'Name of latitude field'
  o.string '-g', '--longitude', 'Name of longitude field'
  o.string '-t', '--timestamp', 'Name of timestamp field'
  o.integer '-s', '--steps', 'Number of interpolated steps'
end

# read input file
csv_in = CSV.new(File.open(opts[:input]))
headers = csv_in.shift
input = csv_in.map { |r| r }
csv_in.close

index_lat = headers.index(opts[:latitude])
index_lon = headers.index(opts[:longitude])
index_ts = headers.index(opts[:timestamp])

traj = input.map { |r| [ Float(r[index_lat]), Float(r[index_lon]), DateTime.iso8601(r[index_ts]) ] }

step_duration = (traj[-1][2] - traj[0][2]) / opts[:steps]

def get_angle(p1, p2)
  x1, y1 = p1[0], p1[1]
  x2, y2 = p2[0], p2[1]
  if (y1 - y2).abs >= 180
    if y1 < 0
      y1 = (y1 + 360) % 360
    else
      y2 = (y2 + 360) % 360
    end
  end

  (Math.atan2(y2 - y1, x2 - x1) + 2 * Math::PI) % (2 * Math::PI)
end

# compute tangents to each intermediary point
traj[0]  << get_angle(traj[0], traj[1])
traj[-1] << get_angle(traj[-1], traj[-2])
(1 .. traj.length-2).each do |i|
  a1 = get_angle(traj[i-1], traj[i])
  a2 = get_angle(traj[i], traj[i+1])
  traj[i] << ((a1 - a2) / 2 + 2 * Math::PI) % 2 * Math::PI
end

CTRL_POINT_RATIO = 1.0 / 4

def gen_ctrl_point(p1, p2)
  x1, y1 = p1[0], p1[1]
  x2, y2 = p2[0], p2[1]

  d = Math.sqrt((x1 - x2)**2 + (y1 - y2)**2)
  dx = d * CTRL_POINT_RATIO * Math.sin(p1[3])
  dy = d * CTRL_POINT_RATIO * Math.cos(p1[3])

  c1x = x1 + dx
  c1y = y1 + dy
  c2x = x1 - dx
  c2y = y1 - dy

  if (c1x - x2)**2 + (c1y - y2)**2 < (c2x - x2)**2 + (c2y - y2)**2
    [c1x, c1y]
  else
    [c2x, c2y]
  end
end

csv_out = CSV.open(opts[:output], "w", { :headers => headers, :write_headers => true })

traj.each_cons(2).each_with_index do |(p1, p2), i|
  csv_out << input[i]
  next if p1[0] == p2[0] && p1[1] == p2[1]

  p1 = p1.dup
  p2 = p2.dup
  if (p1[1] - p2[1]).abs >= 180
    if p1[1] < 0
      p1[1] += 360
    else
      p2[1] += 360
    end
  end

  c1 = gen_ctrl_point(p1, p2)
  c2 = gen_ctrl_point(p2, p1)

  leg_duration = p2[2] - p1[2]
  nb_steps = leg_duration / step_duration
  s = 1
  ts = p1[2]
  while s < nb_steps
    t = s / nb_steps
    k0, k1, k2, k3 = (1 - t)**3, 3*(1 - t)**2 * t, 3*(1 - t) * t**2, t**3

    x = k0 * p1[0] + k1 * c1[0] + k2 * c2[0] + k3 * p2[0]
    y = k0 * p1[1] + k1 * c1[1] + k2 * c2[1] + k3 * p2[1]
    y -= 360 if y > 180
    ts += step_duration

    row = Array.new(headers.length)
    row[index_lat] = x
    row[index_lon] = y
    row[index_ts] = ts
    csv_out << row

    s += 1
  end
end
csv_out << input[-1]
