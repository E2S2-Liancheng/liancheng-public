#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'date'

class String
  def unindent
    indent = split("\n").map { |l| l.size - l.strip.size }.min
    gsub(/^[ \t]{#{indent}}/, '')
  end
end

###################################################################################################
# Deep copy mechanism

class Hash
  def deep_copy
    copy = self.dup
    copy.each do |k,v|
      copy[k] = begin
        v.respond_to?(:deep_copy) ? v.deep_copy : v.dup
      rescue TypeError
        v
      end
    end
  end
end

class Array
  def deep_copy
    copy = self.dup
    copy.map! do |v|
      begin
        v.respond_to?(:deep_copy) ? v.deep_copy : v.dup
      rescue TypeError
        v
      end
    end
  end
end

###################################################################################################

module Utils
  def self.get_utc_now()
    # HACK - because DateTime is retarded and doesn't allow changes on time zones
    #        at creation the object is set on the locale's time zone (for instance +08:00)
    #        first convert to Unix timestamp (somehow, returns it in UTC)
    #        then convert back to a DateTime, now it is using UTC
    DateTime.strptime(DateTime.now.strftime("%s"),"%s")
  end
end
