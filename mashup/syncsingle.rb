#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'thread'

require_relative '../common/rwlock'


# This module provides syntactic sugar for using a system-wide reader/writer lock.
#
module SyncSingle
  @mutex = RwLock::RwMutex.new

  def self.sync_read
    @mutex.synchronize(RwLock::READ_ONLY) { yield }
  end

  def self.sync_write
    @mutex.synchronize(RwLock::READ_WRITE) { yield }
  end

  def self.sync_read_catch(sym)
    catch(sym) { @mutex.synchronize(RwLock::READ_ONLY) { yield } }
  end

  def self.sync_write_catch(sym)
    catch(sym) { @mutex.synchronize(RwLock::READ_WRITE) { yield } }
  end
end
