#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'pg'
require 'sequel'

Sequel.default_timezone = :utc

sequel_loggers =
  case Sinatra::Application.environment
    when :development
      [$logger]
    when :production
      log = SETTINGS['self']['sequel_logs']
      [log ? Logger.new(log) : $logger]
    when :test
      []
    else
      [$logger]
  end

DB =
  if ENV['DATABASE_URL']
    Sequel.connect(ENV['DATABASE_URL'])
  else
    Sequel.postgres( :host => SETTINGS['database']['host'],
                     :port => SETTINGS['database']['port'],
                     :user => SETTINGS['database']['user'],
                     :password => SETTINGS['database']['password'],
                     :database => SETTINGS['database']['dbname'],
                     :loggers => sequel_loggers )
  end

def DB.do_not_log(&block)
  old_loggers = self.loggers
  self.loggers = []

  result = yield

  self.loggers = old_loggers

  result
end

module Sm
  class AccountStatus < Sequel::Model(:account_status); end

  class Crew < Sequel::Model(:crew)
    many_to_one :owner, :class => 'Sm::Member', :key => :creator_member_id

    many_to_many :members, :class => 'Sm::Member', :left_key => :crew_id, :right_key => :member_id, :join_table => 'crew_member'

    InfoFields = [:id, :name]

    dataset_module do
      def get_info
        select(*InfoFields)
      end
    end

    def get_info(with_members = false)
      to_hash.slice(*InfoFields).merge(with_members ? { :members => members.map(&:id) } : {})
    end
  end

  class CrewMember < Sequel::Model(:crew_member)
  end

  class Directory < Sequel::Model(:directory)
    many_to_one :parent,   :class => self, :key => :parent_id
    one_to_many :children, :class => self, :key => :parent_id

    one_to_many :inodes, :class => 'Sm::Inode', :key => :directory_id
  end

  class FileObfuscated < Sequel::Model(:file_obfuscated)
    many_to_one :publisher, :class => 'Sm::Member', :key => :member_id
    many_to_one :inode,     :class => 'Sm::Inode',  :key => :inode_id

    InfoFields = [:id, :inode_id, :annotation]

    dataset_module do
      def get_info
        select(*InfoFields)
      end
    end

    def get_info
      to_hash.slice(*InfoFields)
    end
  end

  class FilePublished < Sequel::Model(:file_published)
    many_to_one :publisher, :class => 'Sm::Member', :key => :member_id
    many_to_one :inode,     :class => 'Sm::Inode',  :key => :inode_id
  end

  class Inode < Sequel::Model(:inode)
    many_to_one :directory, :class => 'Sm::Directory', :key => :directory_id

    one_to_many :revisions, :class => 'Sm::Revision', :key => :inode_id

    def self.latest_revision(inode_id)
      Sm::Revision[inode_id, self[inode_id].revisions_dataset.max(:revision)]
    end
  end

  class Job < Sequel::Model(:job)
    many_to_one :job_status, :class => 'Sm::JobStatus', :key => :job_status_id
  end

  class JobInput < Sequel::Model(:job_input); end

  class JobOutput < Sequel::Model(:job_output)
    many_to_one :job, :class => 'Sm::Job', :key => :job_id
  end

  class JobStatus < Sequel::Model(:job_status); end

  class Member < Sequel::Model(:member)
    many_to_one :account_status, :class => 'Sm::AccountStatus', :key => :account_status_id

    one_to_many :crews, :class => 'Sm::Crew', :key => :creator_member_id

    many_to_many :in_crews, :class => 'Sm::Crew', :left_key => :member_id, :right_key => :crew_id, :join_table => 'crew_member'

    # information retrieval

    InfoFields = [:id, :name]

    dataset_module do
      def get_info
        select(*InfoFields)
      end
    end

    def get_info
      to_hash.slice(*InfoFields)
    end
  end

  class MimeType < Sequel::Model(:mime_type); end

  class Revision < Sequel::Model(:inode_revision)
    many_to_one :inode,        :class => 'Sm::Inode',       :key => :inode_id

    many_to_one :owner,        :class => 'Sm::Member',      :key => :modified_by
    many_to_one :mime_type,    :class => 'Sm::MimeType',    :key => :mime_type_id
    many_to_one :storage_file, :class => 'Sm::StorageFile', :key => :storage_file_id
  end

  class ShareInvite < Sequel::Model(:share_invite); end
  class ShareInviteStatus < Sequel::Model(:share_invite_status); end
  class ShareWithCrew < Sequel::Model(:share_with_crew); end
  class ShareWithMember < Sequel::Model(:share_with_member); end
  class StorageFile < Sequel::Model(:storage_file); end

  # special: admin table
  class Admin < Sequel::Model(:admin); end
end

module Db
  class DbSkeleton
    def initialize(db_name)
      @pg_conn_hash = %W(host port dbname user password).each_with_object({}) do |s,h|
        v = SETTINGS[db_name][s]
        h[s.to_sym] = v if v && v.size > 0
      end
      @conn = get_new_conn()
    end

    def get_new_conn()
      WrapMutex.new(PG.connect(@pg_conn_hash))
    end

    attr_reader :conn, :pg_conn_hash
  end

  class DbMain < DbSkeleton
    def initialize; super('database'); end
  end

  class DbJobs < DbSkeleton
    def initialize; super('jobs_database'); end
  end
end

DB_MAIN = Db::DbMain.new
DB_CONN = DB_MAIN.get_new_conn()
DB_JOBS = Db::DbJobs.new if SETTINGS['jobs']['enabled']
