#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

module Workflow
  # initialise constants from configuration file
  MAX_LENGTH = begin
    SETTINGS['workflow']['max_length'].tap { |m| raise if m.nil? }
  rescue
    65535
  end

  # TODO use json-schema instead
  begin
    w = SETTINGS['jobs']

    b = w['bin_dir']
    %w[optiliner smooth provenance].each do |p|
      w[p]['bin_dir'] ||= b
      raise if w[p]['bin_dir'].nil?
    end
  rescue => e
    raise 'Incorrect "workflow" section in main.conf'
  end

  # Reads a string or a file contents to determine whether it is a workflow or not.
  #
  def self.is_a_workflow?(workflow)
    Graph.parse(workflow)
    true
  rescue ParsingError
    false
  end

  # Error class for unknown or unhandled nodes.
  #
  class UnimplementedNode < StandardError; end


  # Represents a column in a temporary table
  #
  class Column
    def initialize(name, type)
      @name = name
      @type = type
    end

    attr_reader :name, :type
  end

  # Represents a node in the graph, used to represent, validate and transform the workflow.
  #
  class Node
    def self.create(node)
      klass = 'Node' + node['type'].split('_').map{ |e| e.capitalize }.join
      Workflow.const_get(klass).new(node)
    rescue
      raise UnimplementedNode.new("Unhandled operator #{node['type']}")
    end

    def initialize(node, nb_in, nb_out)
      @node = node
      @data = @node['data']
      @id = node['id']

      @nb_in = nb_in
      @nb_out = nb_out

      @in = Array.new(nb_in)
      @out = Array.new(nb_out)

      @in_visited = 0

      @qpart_id = -1
    end

    def add_connection_in(index, node)
      raise "Invalid index" unless index >= 0 && index < @in.size
      raise "Already connected" if @in[index]
      @in[index] = node
    end

    def add_connection_out(index, node)
      raise "Invalid index" unless index >= 0 && index < @out.size
      raise "Already connected" if @out[index]
      @out[index] = node
    end

    def is_valid?()
      [@in, @out].all? { |a| a.none?(&:nil?) }
    end

    def is_input?()
      @nb_in == 0
    end

    def is_output?()
      @nb_out == 0
    end

    attr_accessor :qpart_id
    attr_reader :id, :nb_in, :nb_out, :in, :in_visited, :t_name, :provides
  end

  class NodeImportCsv < Node
    def initialize(node)
      super(node, 0, 1)
    end

    def map_to_file(&block)
      @file_path = yield(Integer(@data['file']['inode_id']), Integer(@data['file']['revision']))
    end

    def to_sql(job_id, queries, cleanup)
      @t_name = "job_#{job_id}_#{@id}"

      nb_cols = @data['cols'].size

      cols = @data['cols'].map { |c| "#{DB_CONN.escape_identifier(c['user'])} text" }

      file_path = DB_CONN.escape_string(@file_path)

      queries << [ :sql, "CREATE FOREIGN TABLE #{@t_name} (#{cols.join(', ')}) " +
                         "SERVER file_fdw_server " +
                         "OPTIONS (format 'csv', header '#{@data['has_header']}', filename '#{file_path}')" ]

      cleanup << [ :sql, "DROP FOREIGN TABLE IF EXISTS #{@t_name}" ]

      @provides = @data['cols'].map { |c| Column.new(c['user'], 'TEXT') }
      @out[0].to_sql(job_id, queries, cleanup)
    end

    attr_reader :cols
  end

  class NodeExportCsv < Node
    def initialize(node)
      super(node, 1, 0)
    end

    def to_sql(job_id, queries, cleanup)
      @output_file = File.join(SETTINGS['jobs']['result_dir'], "job_#{job_id}_#{id}")

      queries << [ :sql, "COPY (SELECT * FROM #{@in[0].t_name}) TO '#{@output_file}' WITH CSV HEADER" ]

      cleanup << [ :cmd, ['rm', ['f', @output_file] ] ]
    end

    def get_output_file()
      { 'directory_id' => @data['file']['directory_id'],
        'name' => @data['file']['name'],
        'mime_type' => 'text/csv',
        'temp_file' => @output_file }
    end
  end

  class NodeOperatorDb < Node
    def initialize(node)
      super(node, 1, 1)
    end

    #
    # job_id
    # parent = calling upstream node
    # prepare = [queries]
    # queries = [complete queries]
    # t_name = table name upstream
    # c_query = current query
    # cleanup = [queries]
    #
    def to_sql(job_id, parent, prepare, queries, t_name, c_query, cleanup)
      c_query = t_name.nil? ? "(#{c_query})" : t_name

      c_query = "SELECT #{@node['params']['select']} FROM #{c_query} WHERE #{@node['params']['where']}"

      @out[0].to_sql(job_id, self, prepare, queries, nil, c_query, cleanup)
    end
  end

  class NodeOperatorProjection < Node
    def initialize(node)
      super(node, 1, 1)
    end

    def to_sql(job_id, queries, cleanup)
      @t_name = "job_#{job_id}_#{@id}"

      cols = @data['cols'].select { |c| c['checked'] }.map { |c| c['name'] }

      queries << [ :sql, "CREATE TABLE #{@t_name} AS SELECT " +
                         cols.map { |c| "\"#{DB_CONN.escape_string(c)}\"" }.join(',') +
                         " FROM #{@in[0].t_name}" ]

      cleanup << [ :sql, "DROP TABLE IF EXISTS #{@t_name}" ]

      @provides = cols.map { |c| @in[0].provides.find { |c2| c == c2.name } }
      @out[0].to_sql(job_id, queries, cleanup)
    end
  end

  class NodeOperatorFilter < Node
    def initialize(node)
      super(node, 1, 1)
    end

    TEXT_TO_OP = { :eq =>   'IS NOT DISTINCT FROM',
                   :neq =>  'IS DISTINCT FROM',
                   :lt =>   '<',
                   :lteq => '<=',
                   :gt =>   '>',
                   :gteq => '>=' }

    def to_sql(job_id, queries, cleanup)
      @t_name = "job_#{job_id}_#{@id}"

      clauses = @data['clauses'].each_with_index.map do |c, i|
        "\"#{DB_CONN.escape_string(c['name'])}\" #{TEXT_TO_OP[c['op'].to_sym]} $#{i+1}"
      end
      values = @data['clauses'].map { |c| c['value'] }

      queries << [ :sqlp, { :q => "CREATE TABLE #{@t_name} AS SELECT * FROM #{@in[0].t_name}" +
                                  " WHERE " + clauses.join(' AND '),
                            :p => values } ]

      cleanup << [ :sql, "DROP TABLE IF EXISTS #{@t_name}" ]

      @provides = @in[0].provides
      @out[0].to_sql(job_id, queries, cleanup)
    end
  end

  class NodeControlCopy < Node
    def initialize(node)
      super(node, 1, node['data']['hooks'])
    end

    def to_sql(job_id, queries, cleanup)
      @t_name = "job_#{job_id}_#{@id}"

      queries << [ :sql, "CREATE TABLE #{@t_name} AS SELECT * FROM #{@in[0].t_name}" ]

      cleanup << [ :sql, "DROP TABLE IF EXISTS #{@t_name}" ]

      @provides = @in[0].provides
      @out.each { |o| o.to_sql(job_id, queries, cleanup) }
    end
  end

  class NodeOperatorOptiliner < Node
    def initialize(node, nb_in, nb_out)
      super(node, nb_in, nb_out)
    end

    OPTILINER_BIN = File.join(SETTINGS['jobs']['optiliner']['bin_dir'], SETTINGS['jobs']['optiliner']['bin'])

    def to_sql(job_id, queries, cleanup)
      return if (@in_visited += 1) < @nb_in

      @t_name = "job_#{job_id}_#{@id}"
      files = @in.map { |n| File.join(SETTINGS['jobs']['temp_dir'], "job_#{job_id}_#{@id}_#{n.id}") }
      result_f = File.join(SETTINGS['jobs']['temp_dir'], "job_#{job_id}_#{@id}_res")
      out_cols = [ [ 'Port of origin', 'TEXT'],
                   [ 'Latitude', 'FLOAT'],
                   [ 'Longitude', 'FLOAT'],
                   [ 'Distance to next', 'FLOAT'],
                   [ 'Speed', 'FLOAT'],
                   [ 'Bunker level', 'FLOAT'],
                   [ 'Departure time', 'TEXT'],
                   [ 'Arrival time at next port', 'TEXT'] ]

      files.each_with_index { |f, i| queries << [ :sql, "COPY (SELECT * FROM #{@in[i].t_name}) TO '#{f}' WITH CSV HEADER" ] }
      args = [ '--ports', files[0],
               '--vessels', files[1],
               '--vessel-type', files[2],
               '--route', files[3],
               '--out', result_f ]
      args.concat([ '--bunker-prices', files[4] ]) if @nb_in >= 5
      args.concat([ '--contract', files[5] ]) if @nb_in == 6

      queries << [ :cmd, [ OPTILINER_BIN, args ] ]
      queries << [ :sql, "CREATE FOREIGN TABLE #{@t_name} " +
                         "(#{out_cols.map { |c| "\"#{c[0]}\" #{c[1]}" }.join(',')}) " +
                         "SERVER file_fdw_server " +
                         "OPTIONS (format 'csv', header 'true', filename '#{result_f}')" ]

      cleanup << [ :cmd, ['rm', ['-f', result_f].concat(files)]]
      cleanup << [ :sql, "DROP FOREIGN TABLE IF EXISTS #{@t_name}" ]

      @provides = out_cols.map { |c| Column.new(*c) }
      @out[0].to_sql(job_id, queries, cleanup)
    end
  end

  class NodeOperatorOptiemission < NodeOperatorOptiliner
    def initialize(node)
      super(node, 4, 1)
    end
  end

  class NodeOperatorOpticost < NodeOperatorOptiliner
    def initialize(node)
      super(node, 5, 1)
    end
  end

  class NodeOperatorOpticontract < NodeOperatorOptiliner
    def initialize(node)
      super(node, 6, 1)
    end
  end

  class NodeOperatorSmooth < Node
    def initialize(node)
      super(node, 1, 1)
    end

    SMOOTH_BIN = File.join(SETTINGS['jobs']['smooth']['bin_dir'], SETTINGS['jobs']['smooth']['bin'])

    def to_sql(job_id, queries, cleanup)
      @t_name = "job_#{job_id}_#{@id}"

      source_file = File.join(SETTINGS['jobs']['temp_dir'], "job_#{job_id}_#{@id}")
      result_file = File.join(SETTINGS['jobs']['temp_dir'], "job_#{job_id}_#{@id}_res")

      queries << [ :sql, "COPY (SELECT * FROM #{@in[0].t_name}) TO '#{source_file}' WITH CSV HEADER" ]

      args = [ '-i', source_file,
               '-o', result_file,
               '-s', @data['steps'],
               '-l', @data['lat'],
               '-g', @data['lon'],
               '-t', @data['ts'] ]
      queries << [ :cmd, [ SMOOTH_BIN, args ] ]

      queries << [ :sql, "CREATE FOREIGN TABLE #{@t_name} " +
                         "(#{@in[0].provides.map { |c| "\"#{c.name}\" #{c.type}" }.join(',')}) " +
                         "SERVER file_fdw_server " +
                         "OPTIONS (format 'csv', header 'true', filename '#{result_file}')" ]

      cleanup << [ :sql, "DROP TABLE IF EXISTS #{@t_name}" ]

      @provides = @in[0].provides
      @out[0].to_sql(job_id, queries, cleanup)
    end
  end

  class NodeOperatorProvconcn < Node
    def initialize(node)
      super(node, 4, 1)
    end

    CONCN_BIN = File.join(SETTINGS['jobs']['provenance']['bin_dir'], SETTINGS['jobs']['provenance']['concn'])

    def to_sql(job_id, queries, cleanup)
      return if (@in_visited += 1) < @nb_in

      @t_name = "job_#{job_id}_#{@id}"
      files = @in.map { |n| File.join(SETTINGS['jobs']['temp_dir'], "job_#{job_id}_#{@id}_#{n.id}") }
      result_f = File.join(SETTINGS['jobs']['temp_dir'], "job_#{job_id}_#{@id}_res")
      out_cols = [ [ 'Toxin level', 'FLOAT'],
                   [ 'Number', 'INT'] ]

      files.each_with_index { |f, i| queries << [ :sql, "COPY (SELECT * FROM #{@in[i].t_name}) TO '#{f}' WITH CSV HEADER" ] }

      args = [ files[0], files[1], files[2], files[3], @data['samples'].to_s, result_f ]
      queries << [ :cmd, [ CONCN_BIN, args ] ]
      queries << [ :sql, "CREATE FOREIGN TABLE #{@t_name} " +
                         "(#{out_cols.map { |c| "\"#{c[0]}\" #{c[1]}" }.join(',')}) " +
                         "SERVER file_fdw_server " +
                         "OPTIONS (format 'csv', header 'true', filename '#{result_f}')" ]

      cleanup << [ :cmd, ['rm', ['-f', result_f].concat(files)]]
      cleanup << [ :sql, "DROP FOREIGN TABLE IF EXISTS #{@t_name}" ]

      @provides = out_cols.map { |c| Column.new(*c) }
      @out[0].to_sql(job_id, queries, cleanup)
    end
  end

  class NodeOperatorProvdiff < Node
    def initialize(node)
      super(node, 1, 1)
    end

    DIFF_BIN = File.join(SETTINGS['jobs']['provenance']['bin_dir'], SETTINGS['jobs']['provenance']['diff'])

    def to_sql(job_id, queries, cleanup)
      @t_name = "job_#{job_id}_#{@id}"

      source_file = File.join(SETTINGS['jobs']['temp_dir'], "job_#{job_id}_#{@id}")
      result_file = File.join(SETTINGS['jobs']['temp_dir'], "job_#{job_id}_#{@id}_res")
      out_cols = [ [ 'Toxin level', 'FLOAT'],
                   [ 'Area', 'INT'] ]

      queries << [ :sql, "COPY (SELECT * FROM #{@in[0].t_name}) TO '#{source_file}' WITH CSV HEADER" ]
      queries << [ :cmd, [ DIFF_BIN, ['-i', source_file, '-o', result_file] ] ]
      queries << [ :sql, "CREATE FOREIGN TABLE #{@t_name} " +
                         "(#{out_cols.map { |c| "\"#{c[0]}\" #{c[1]}" }.join(',')}) " +
                         "SERVER file_fdw_server " +
                         "OPTIONS (format 'csv', header 'true', filename '#{result_file}')" ]

      cleanup << [ :cmd, ['rm', ['-f', result_file] ] ]
      cleanup << [ :sql, "DROP FOREIGN TABLE IF EXISTS #{@t_name}" ]

      @provides = out_cols.map { |c| Column.new(*c) }
      @out[0].to_sql(job_id, queries, cleanup)
    end
  end

  class NodeExportOptikml < Node
    def initialize(node)
      super(node, 1, 0)
    end

    OPTIKML_BIN = File.join(SETTINGS['jobs']['optiliner']['bin_dir'], SETTINGS['jobs']['optiliner']['kml'])

    # TODO make column names choice flexible (need to modify the UI too)
    def to_sql(job_id, queries, cleanup)
      source_file = File.join(SETTINGS['jobs']['temp_dir'], "job_#{job_id}_#{@id}")
      @output_file = File.join(SETTINGS['jobs']['temp_dir'], "job_#{job_id}_#{@id}_res")

      queries << [ :sql, "COPY (SELECT * FROM #{@in[0].t_name}) TO '#{source_file}' WITH CSV HEADER" ]

      args = [ '-i', source_file,
               '-o', @output_file,
               '-lat', 'Latitude',
               '-long', 'Longitude',
               '-time', 'Departure time',
               '-port', 'Port of origin' ]
      queries << [ :cmd, [ OPTIKML_BIN, args ] ]

      cleanup << [ :cmd, ['rm', ['f', source_file, @output_file] ] ]
    end

    def get_output_file()
      { 'directory_id' => @data['file']['directory_id'],
        'name' => @data['file']['name'],
        'mime_type' => 'application/vnd.google-earth.kml+xml',
        'temp_file' => @output_file }
    end
  end

  class QueryPart
    def initialize(last_node, id)
      @first_nodes = []
      @last_node = last_node
      @id = id
    end

    def add_first_node(first_node, id)
      @first_nodes << [first_node, id]
    end

    def depends_on()
      @first_nodes.map { |n| n[1] }.compact
    end

    attr_reader :id, :last_node
  end


  class ParsingError < StandardError; end;
  class InvalidSyntax < ParsingError; end
  class UnknownVersion < ParsingError; end
  class FileTooLarge < ParsingError; end

  class EvaluationError < StandardError; end
  class InvalidStructure < EvaluationError; end
  class UnknownNodeType < EvaluationError; end

  #
  #
  class Graph
    def initialize(workflow)
      @source = Graph.parse(workflow)
    end

    # Parses a string or a file contents to extract the workflow.
    # Returns the parsed structure if it is a workflow, raises a ParsingError exception otherwise
    #
    def self.parse(workflow)
      if workflow.size > MAX_LENGTH
        $logger.info("Workflow file too large: #{workflow.size} > #{MAX_LENGTH}")
        raise FileTooLarge.new
      end

      workflow = workflow.read if workflow.is_a?(Tempfile)

      res = JSON.parse(workflow, { :max_nesting => 8, :create_additions => false })

      raise InvalidSyntax.new unless res['meta']['app'] == 'UrbanDB'
      raise UnknownVersion.new if res['meta']['version'] && res['meta']['version'] != 1

      res
    rescue JSON::ParserError => e
      $logger.info("Parsing workflow: #{e.msg_bt}")
      raise InvalidSyntax.new
    rescue NoMethodError => e
      $logger.info("Parsing workflow: #{e.msg_bt}")
      raise InvalidSyntax.new
    end

    # Evaluates the contents of the workflow file and prepares the in-memory graph.
    # Returns nil when it succeeds, raises an EvaluationError otherwise.
    #
    def evaluate()
      @inputs = []
      @outputs = []
      @nodes = []

      @source['nodes'].each do |n|
        i = n['id']
        raise "Duplicate node ID" if @nodes[i]

        begin
          nn = Node.create(n.dup)
          @nodes[i] = nn
          @inputs << nn if nn.is_input?
          @outputs << nn if nn.is_output?
        rescue UnimplementedNode => e
          $logger.info(e.msg_bt)
          raise UnknownNodeType.new
        end
      end

      @source['connectors'].each do |c|
        n_out = @nodes[c['up']['node']]
        if n_out.nil?
          $logger.info("Invalid node ID out ##{c['up']['node']}")
          raise InvalidStructure.new
        end

        n_in = @nodes[c['down']['node']]
        if n_in.nil?
          $logger.info("Invalid node ID in ##{c['down']['node']}")
          raise InvalidStructure.new
        end

        n_out.add_connection_out(c['up']['hook'], n_in)
        n_in.add_connection_in(c['down']['hook'], n_out)
      end

      return "Some nodes are not completely connected or not connected at all" unless @nodes.compact.all?(&:is_valid?)

      # TODO check for cycles in here

      nil
    rescue => e
      $logger.info("Evaluating workflow: #{e.msg_bt}")
      raise InvalidStructure.new
    end

    def map_to_file(&block)
      @nodes.select { |n| n.respond_to?(:map_to_file) }.each { |n| n.map_to_file(&block) }
    end

    def next_query_id()
      @query_ids += 1
    end

    def set_qpart_id(node, qpart_id)
      return if node.qpart_id > 0
      qpart_id = next_query_id() if node.nb_out > 1
      node.qpart_id = qpart_id
      node.in.each { |n| set_qpart_id(n, qpart_id) }
    end

    def to_sql(job_id)
      # first pass, give a sub-query ID to each node
      @query_ids = 0
      sub_queries = @outputs.each { |n| set_qpart_id(n, next_query_id()) }

      queries = []
      cleanup = []

      @inputs.map { |n| n.to_sql(job_id, queries, cleanup) }

      [queries, cleanup]
    end

    def get_output_files()
      @outputs.select { |o| o.respond_to?(:get_output_file) }.map(&:get_output_file)
    end

    attr_reader :inputs, :outputs, :nodes
  end

end
