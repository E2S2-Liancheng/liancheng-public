#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


helpers do
  def check_member_pending(member)
    raise BadRequestError.new("Account ##{member.id} isn't pending, status is #{member.account_status_id}") \
                                                    if member.account_status_id != Sm::AccountStatus::ValidationPending
  end
end


# POST /register
# Registers a new user in the system.
#
#-- WARNING password must not be logged
#
post '/register' do
  name = param_non_empty(:name)
  email = param_non_empty(:email)
  password = param_non_empty(:password)

  result, contents = DB.transaction { Members.add(name, email, password) }

  content_type :json
  JSON[ result ? { :status => 'ok', :pending_id => contents } : { :status => 'err', :err_msg => contents } ]
end

get '/register/pending' do
  halt 500 unless session[:admin]

  pendings =
    DB.transaction do
      Sm::Member.where(:account_status_id => Sm::AccountStatus::ValidationPending)
                .select(:id, :name, :email)
                .all.map(&:to_hash)
    end

  content_type :json
  JSON[ :status => 'ok', :pendings => pendings ]
end

post '/register/validate' do
  halt 500 unless session[:admin]
  pending_id = param_to_int(:pending_id)

  DB.transaction do
    check_member_pending(check_member_sql(pending_id))

    directory_id = Directories.add_contents('home', Utils.get_utc_now, nil)
    trash_id = Directories.add_contents('trash', Utils.get_utc_now, nil)

    Members.validate(pending_id, directory_id, trash_id)
  end

  content_type :json
  JSON[ :status => 'ok' ]
end

post '/register/reject' do
  halt 500 unless session[:admin]
  pending_id = param_to_int(:pending_id)

  DB.transaction do
    check_member_pending(check_member_sql(pending_id))

    Members.reject(pending_id)
  end

  content_type :json
  JSON[ :status => 'ok' ]
end
