#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

get '/notif/count' do
  count = DB.transaction { Sm::ShareInvite.where(:member_id => session[:id], :item_read => false).count }

  content_type :json
  JSON[ :status => 'ok', :count => count ]
end

get '/notif/list' do
  notifs =
    DB.transaction do
      directories = Sm::ShareInvite.where(:member_id => session[:id], :share_invite_status_id => Sm::ShareInviteStatus::Invited).all
      Sm::ShareInvite.where(:member_id => session[:id], :item_read => false).update(:item_read => true)

      directories.map do |d|
        { :type => 'share',
          :read => d.item_read,
          :directory_id => d.directory_id,
          :directory_name => Sm::Directory[d.directory_id].name,
          :member_name => Sm::Member[Directories.get_owner_id(d.directory_id)].name }
      end
    end

  content_type :json
  JSON[ :status => 'ok', :notifs => notifs ]
end
