#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'thread'


module Jobs

  # Gets the list of jobs with their output files.
  #
  def self.list_jobs(member_id)
    jobs = Sm::Job.where(:created_by => member_id)
                  .association_join(:job_status)
                  .join(:inode, :id => :inode_id)
                  .select(:job__id,
                          :created_at,
                          :started_at,
                          :finished_at,
                          :inode_id,
                          :revision,
                          :inode__name,
                          Sequel.as(:job_status__name, :status))
                  .all.map(&:to_hash)

    out = Hash.new { |h,k| h[k] = [] }

    Sm::JobOutput.association_join(:job)
                 .where(:created_by => member_id)
                 .select_all(:job_output)
                 .each { |f| out[f[:job_id]] << f.values.slice(:inode_id, :revision) }

    jobs.each { |j| j[:output] = out[j[:id]] if out.keys.include?(j[:id]) }

    jobs
  end


  def self.create_job(member_id, inode_id, revision)
    Sm::Job.insert(:created_by => member_id,
                   :created_at => Utils.get_utc_now,
                   :inode_id => inode_id,
                   :revision => revision,
                   :job_status_id => 1)
  rescue => e
    $logger.warn(e.msg_bt)
    nil
  end


  def self.run_job(id, member_id, graph)
    Sm::Job[id].update(:started_at => Utils.get_utc_now, :job_status_id => 2)

    job_queries, cleanup_queries = graph.to_sql(id)

#    job_queries.each { |j| p j }
#    cleanup_queries.each { |j| p j }

    $logger.info("Job #{id} - start")
    j_conn = DB_JOBS.get_new_conn()

    job_queries.each do |j|
      case j[0]
      when :sql
        $logger.info("Job #{id} - #{j[1]}")
        j_conn.exec(j[1])
      when :sqlp
        $logger.info("Job #{id} - #{j[1]}")
        j_conn.exec(j[1][:q], j[1][:p])
      when :cmd
        $logger.info("Job #{id} - #{j[1].flatten.join(' ')}")
        raise "Command failed" unless system(*j[1].flatten)
      end
    end

    output = graph.get_output_files.map do |out_f|
      DB.transaction do
        mime_type_id = MimeTypes.find_id(out_f['mime_type'])
        file = File.open(out_f['temp_file'])
        storage_file_id, created = StorageFiles.save_file(file)

        Inodes.add_contents( Integer(out_f['directory_id']),
                             out_f['name'],
                             file.size,
                             member_id,
                             mime_type_id,
                             storage_file_id )
      end
    end

    output.each do |of|
      Sm::JobOutput.insert(:job_id => id, :inode_id => of[0], :revision => of[1])
    end

    Sm::Job[id].update(:finished_at => Utils.get_utc_now, :job_status_id => 5)

    $logger.info("Job #{id} - cleanup")
    #cleanup_queries.each { |q| $logger.info("Job #{id} - #{q}"); j_conn.exec(q) }
    $logger.info("Job #{id} - done")

  rescue PG::Error, StandardError => e
    $logger.error(e.msg_bt)

    Sm::Job[id].update(:finished_at => Utils.get_utc_now, :job_status_id => 4)
  end

  @@q = Queue.new

  def self.submit_job(job_id, member_id, graph)
    @@q.push([job_id, member_id, graph])
  end

  Thread.new do
    loop do
      job = @@q.pop

      Thread.new(*job, &Jobs.method(:run_job)).join
    end
  end
end


get '/job/list' do
  jobs = DB.transaction { Jobs.list_jobs(session[:id]) }

  content_type :json
  JSON[ :status => 'ok', :jobs => jobs ]
end

post '/job' do
  ok, contents = catch(:msg) do
    throw :msg, [ false, :err_job_disabled ] unless SETTINGS['jobs']['enabled']

    wf_inode_id = param_to_int('inode_id')
    wf_revision = param_to_int('revision')

    workflow = DB.transaction do
      wf_file = check_inode_revision_sql(wf_inode_id, wf_revision)
      raise "Not a valid workflow" unless wf_file.mime_type.name == 'application/x-urbandb-workflow'

      StorageFiles.get_file_handle(wf_file.storage_file_id)
    end.read

    graph = begin
      Workflow::Graph.new(workflow).tap do |g|
        err_msg = g.evaluate
        throw :msg, [ false, err_msg ] if err_msg
      end
    rescue Workflow::ParsingError
      throw :msg, [ false, :err_job_wf_parse ]
    rescue Workflow::EvaluationError
      throw :msg, [ false, :err_job_wf_evaluate ]
    end

    DB.transaction do
      graph.map_to_file do |inode_id, revision|
        file_id = check_inode_revision_sql(inode_id, revision).storage_file_id
        StorageFiles.get_file_handle(file_id).path
      end
    end

    job_id = nil
    DB.transaction do
      job_id = Jobs.create_job(session[:id], wf_inode_id, wf_revision)
      throw :msg, [ false, :err_job_creation ] if job_id.nil?
    end

    Jobs.submit_job( job_id, session[:id], graph)
    [ true, job_id ]
  end

  content_type :json
  JSON[ ok ? { :status => 'ok', :job_id => contents } : { :status => 'err', :err_msg => contents } ]
end
