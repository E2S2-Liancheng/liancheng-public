#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

module Directories

  # Retrieves the directory information as a hash.
  #
  def self.info(directory_id)
    directory = Sm::Directory[directory_id].to_hash
    directory[:children_ids] = Sm::Directory.where(:parent_id => directory_id).map(:id)
    directory[:shared] = is_shared?(directory_id)
    directory[:owner_name] = Sm::Member[get_owner_id(directory_id)].name
    directory[:can_be_shared] = directory[:shared] || can_be_shared?(directory_id)
    directory
  end


  # Creates a new directory in the virtual file system.
  # Throws an error if the name already exists.
  #
  # parent_id can be nil.
  # Returns the directory's id.
  #
  def self.add(name, modified_at, parent_id)
    throw :msg, [ false, :err_dir_ren_already ] if Sm::Directory[parent_id].children.any? { |c| c[:name] == name } || \
                                                   Sm::Directory[parent_id].inodes.any? { |c| c[:name] == name }

    Sm::Directory.insert( :name => name,
                          :modified_at => modified_at,
                          :parent_id => parent_id )
  end


  # Creates a new directory in the virtual file system.
  # Assumes all safety checks have been done beforehand.
  #
  # parent_id can be nil.
  # Returns the directory's id.
  #
  def self.add_contents(name, modified_at, parent_id)
    Sm::Directory.insert( :name => name,
                          :modified_at => modified_at,
                          :parent_id => parent_id )
  end


  # Returns the owner of the directory (going all the way to the root if necessary).
  # Works for deleted directories too.
  # Raises an exception if directory_id does not exist or no owner was found.
  #
  def self.get_owner_id(directory_id)
    directory = Sm::Directory[directory_id]

    loop do
      owner = Sm::Member[:home_directory_id => directory.id]
      return owner.id unless owner.nil?
      owner = Sm::Member[:trash_directory_id => directory.id]
      return owner.id unless owner.nil?

      directory = Sm::Directory[directory.parent_id]
      raise "No owner found for directory #{directory_id}" if directory.nil?
    end
  end


  # Checks whether the directory is deleted.
  # Returns a boolean.
  #
  def self.is_deleted?(directory_id)
    directory = Sm::Directory[directory_id]
    directory = Sm::Directory[directory.parent_id] while directory.parent_id

    !Sm::Member.where(:trash_directory_id => directory.id).empty?
  end


  # Renames the given directory.
  # Returns nil in case of success, an error message otherwise.
  #
  def self.rename(directory_id, name)
    directory = Sm::Directory[directory_id]

    return :err_dir_ren_root if directory.parent_id.nil?
    return :err_dir_ren_share if is_shared?(directory_id)
    return :err_dir_ren_already if Sm::Directory[directory.parent_id].children.any? { |c| c[:name] == name } || \
                                   Sm::Directory[directory.parent_id].inodes.any? { |c| c[:name] == name }

    modified_at = Utils.get_utc_now

    Sm::Directory[directory_id].update(:name => name, :modified_at => modified_at)

    nil
  end


  # Recursively retrieves all the inode_ids contained in the directory.
  # Returns an array of IDs.
  #
  def self.get_all_inode_ids(directory_id)
    directory = Sm::Directory[directory_id]

    inode_ids = directory.inodes_dataset.map(:id)
    directory.children.each { |c| inode_ids.concat get_all_inode_ids(c[:id]) }

    inode_ids
  end


  # Deletes the given directory and its contents recursively.
  # This actually doesn't modify anything, just checks and provides output.
  #
  # Returns the list of directories and files deleted.
  # Throws an error if it finds a shared directory.
  #
  def self.delete_r(directory_id, deleted)
    Sm::Directory.where(:parent_id => directory_id).all.each do |d|
      return :err_dir_del_desc_share if is_shared?(d.id)
      delete_r(d.id, deleted)
    end

    Sm::Inode.where(:directory_id => directory_id)
              .all { |i| deleted << { :dir_id => directory_id, :inode_id => i.id } }
    deleted << { :dir_id => directory_id }

    nil
  end


  # Deletes the given directory and its contents recursively.
  # This actually moves the source directory in the given trash directory.
  #
  # Returns the list of directories and files deleted.
  # Throws an error if the deletion is not possible.
  #
  def self.delete(directory_id, destination_id)
    directory = Sm::Directory[directory_id]

    return [false, :err_dir_del_home] if directory.parent_id.nil?
    return [false, :err_dir_del_share] if is_shared?(directory_id)

    deleted = []

    err = delete_r(directory_id, deleted)
    return [false, err] unless err.nil?

    directory.update(:parent_id => destination_id)

    deleted.map { |v| v[:inode_id] }.compact.each do |inode_id|
      Sm::FileObfuscated.where(:inode_id => inode_id).delete
      Sm::FilePublished.where(:inode_id => inode_id).delete
    end

    [true, deleted]
  end


  # Finds the shared ancestor of the given directory.
  # Returns the ancestor's id if there's one, nil otherwise.
  #
  def self.find_shared_ancestor(directory_id)
    until directory_id.nil? || is_shared?(directory_id)
      directory_id = Sm::Directory[directory_id].parent_id
    end
    directory_id
  end


  # Finds the shared descendants of the given directory.
  # Returns a list of directory IDs, empty if none was found.
  #
  def self.find_shared_descendants(directory_id)
    list = Sm::Directory[directory.id].children.map { |c| c[:id] }
    found = []

    while (directory_id = list.shift)
      if is_shared?(directory_id)
        found << directory_id
      else
        list.concat(Sm::Directory[directory.id].children.map { |c| c[:id] })
      end
    end

    found
  end


  # Determines whether a directory has a shared ancestor.
  # Returns a boolean.
  #
  def self.has_shared_ancestor?(directory_id)
    directory = Sm::Directory[directory_id]

    while directory.parent_id != nil
      directory = Sm::Directory[directory.parent_id]
      return true if Sm::ShareWithMember.where(:directory_id => directory.id).count > 0 || \
                      Sm::ShareWithCrew.where(:directory_id => directory.id).count > 0
    end

    false
  end


  # Determines whether a directory has a shared descendant.
  # Returns a boolean.
  #
  def self.has_shared_descendant?(directory_id)
    list = Sm::Directory.where(:parent_id => directory_id).all

    while (directory = list.shift)
      return true if Sm::ShareWithMember.where(:directory_id => directory.id).count > 0 || \
                      Sm::ShareWithCrew.where(:directory_id => directory.id).count > 0
      list.concat(Sm::Directory.where(:parent_id => directory.id).all)
    end

    false
  end


  # Determines whether the current directory can be shared.
  # Checks whether at least one ancestor or one descendant is already shared.
  # Checks whether it is a home directory.
  # If a member ID is provided, check whether it is the owner.
  # Returns a boolean.
  #
  def self.can_be_shared?(directory_id, member_id = nil)
    return false if (!member_id.nil? && get_owner_id(directory_id) != member_id)
    !has_shared_ancestor?(directory_id) && !has_shared_descendant?(directory_id)
  end


  # Determines whether the current directory is shared.
  # Returns a boolean.
  #
  def self.is_shared?(directory_id)
    [Sm::ShareWithCrew, Sm::ShareWithMember].any? { |t| t.where(:directory_id => directory_id).count > 0 }
  end


  # Determines the directory's complete path from a given member's point of view.
  #
  def self.get_path(directory_id, member_id)
    directory = Sm::Directory[directory_id]
    name = ""

    while directory.parent_id != nil
      name = "/#{directory.name}#{name}"
      if Sm::ShareInvite.where(:directory_id => directory.id, :member_id => member_id).count == 1
        return "/share#{name}"
      end
      directory = Sm::Directory[directory.parent_id]
    end

    "/home#{name}"
  end


  # Checks that the directory names at the source do not collide with the inode names at the destination, and vice-versa.
  # Returns an Array of Hashes representing the conflicts. If the Array is empty, the paste is possible
  #
  def self.check_paste(src_dirs, src_files, destination_id, is_a_cut, conflicts, member_id)
    dst_dirs = Sm::Directory[destination_id].children_dataset
    dst_inodes = Sm::Directory[destination_id].inodes_dataset

    src_dirs.each do |s|
      d = dst_inodes.where(:name => s.name)
      next if d.empty?
      conflicts << { :type => :err_paste_name_clash,
                      :src => get_path(s.parent_id, member_id),
                      :dst => get_path(d.first.directory_id, member_id),
                      :name => s.name }
    end

    src_files.each do |s|
      d = dst_dirs.where(:name => s.name)
      next if d.empty?
      conflicts << { :type => :err_paste_name_clash,
                      :src => get_path(s.directory_id, member_id),
                      :dst => get_path(d.first.parent_id, member_id),
                      :name => s.name }
    end

    src_dirs.each do |s|
      d = dst_dirs.where(:name => s.name)
      if d.count > 0
        dst_id = d.first.id
        src_shared = is_shared?(s.id)
        dst_shared = is_shared?(dst_id)

        if is_a_cut && (src_shared || dst_shared)
          type = case
            when src_shared && dst_shared then :err_paste_both_shared
            when src_shared then :err_paste_src_shared
            else :err_paste_dst_shared
          end
          conflicts << { :type => type,
                          :src => get_path(s.parent_id, member_id),
                          :dst => get_path(d.parent_id. member_id),
                          :name => s.name }
        end

        check_paste( Sm::Directory.where(:parent_id => s.id),
                      Sm::Inode.where(:directory_id => s.id),
                      dst_id,
                      is_a_cut,
                      conflicts,
                      member_id )
      end
    end
  end

  # Checks that the directory names at the source do not collide with the inode names at the destination, and vice-versa.
  # Starts the (eventual) recursion.
  # Returns an Array of Hashes representing the conflicts. If the Array is empty, the paste is possible
  #
  def self.start_check_paste(dir_ids, file_ids, destination_id, is_a_cut, member_id)
    ancestor_id = destination_id
    if is_a_cut && dir_ids.any? { |d| is_shared?(d) || has_shared_descendant?(d) } \
                && (is_shared?(destination_id) || (ancestor_id = find_shared_ancestor(destination_id)))
      src_shared = dir_ids.map { |d| is_shared?(d) ? d : find_shared_descendants(d) }.flatten
      return [{ :type => :err_paste_nested_shared,
                :src_shared => src_shared }]
    end

    conflicts = []
    check_paste( Sm::Directory.where(:id => dir_ids),
                  Sm::Inode.where(:id => file_ids),
                  destination_id,
                  is_a_cut,
                  conflicts,
                  member_id )
    conflicts
  end


  # Copies recursively the contents of a directory into another directory.
  # If destination_name is not nil, the source directory is renamed into it
  #   (the system will look for a non-colliding name).
  # File ownership is preserved.
  # Returns the list of directories and inodes newly created.
  #
  def self.copy(source_id, destination_id, destination_name, timestamp)
    src = Sm::Directory[source_id]
    dst = Sm::Directory[destination_id]

    dir_reused = false

    ndir_id = if destination_name.nil?
      same_name = dst.children_dataset.where(:name => src.name)
      if same_name.count == 0
        add_contents(src.name, timestamp, destination_id)
      else
        dir_reused = true
        same_name.first.id
      end
    else
      name = "copy of #{destination_name}"
      i = 0
      while Sm::Directory.where(:parent_id => destination_id, :name => name).count != 0
        i += 1
        name = "copy of #{destination_name} - %d" % [i]
      end
      add_contents(name, timestamp, destination_id)
    end

    nc = Sm::Directory.where(:parent_id => source_id).all.map { |d| copy(d.id, ndir_id, nil, timestamp) }
    ni = Sm::Inode.where(:directory_id => source_id).all.map { |i| Inodes.copy(i.id, ndir_id, timestamp) }
    ni << info(ndir_id) unless dir_reused

    nc.flatten(1) + ni
  end


  # Cuts and pastes recursively the contents of a directory into another directory.
  # File ownership is preserved.
  #
  def self.cut_paste(source_id, destination_id, timestamp, changes)
    src = Sm::Directory[source_id]
    dst = Sm::Directory[destination_id]

    same_name = dst.children_dataset.where(:name => src.name)
    ndir_id = (same_name.empty?) ? add_contents(src.name, timestamp, destination_id) : same_name.first.id

    src.children.each { |d| cut_paste(d.id, ndir_id, timestamp, changes) }
    src.inodes.each { |i| Inodes.cut_paste(i.id, ndir_id, timestamp, changes) }

    if is_shared?(source_id)
      Sm::ShareWithCrew.where(:directory_id => source_id).update(:directory_id => ndir_id)
      Sm::ShareInvite.where(:directory_id => source_id).update(:directory_id => ndir_id)
      Sm::ShareWithMember.where(:directory_id => source_id).update(:directory_id => ndir_id)
    end

    src.delete

    changes[:created] << info(ndir_id) if same_name.empty?
    changes[:deleted] << { :dir_id => source_id }
  end

end


helpers do
  def check_directory_sql(directory_id)
    Sm::Directory[directory_id].tap { |d| raise ObjectNotFoundError.new('directory', directory_id, session[:id]) if d.nil? }
  end
end


# GET /directory/:directory_id
#
get %r{^/directory/(\d+)$} do
  directory_id = params[:captures][0].to_i

  directory = DB.transaction do
    check_directory_sql(directory_id)

    acl = Security.get_access_rights(directory_id, session[:id])
    raise AccessRightsError.new('directory', directory_id, session[:id]) unless acl[1]

    directory = Directories.info(directory_id)

    # hide the directory parent ID if the directory is shared and belongs to another member
    directory[:parent_id] = nil if directory[:shared] && acl[0] != session[:id]

    directory
  end

  content_type :json
  JSON[ :status => 'ok', :directory => directory ]
end


# GET /directory/home
#
get '/directory/home' do
  directory = DB.transaction { Directories.info(Sm::Member[session[:id]].home_directory_id) }

  content_type :json
  JSON[ :status => 'ok', :directory => directory ]
end


# GET /directory/:directory_id/inodes
#
get %r{^/directory/(\d+)/inodes$} do
  directory_id = params[:captures][0].to_i

  inodes = DB.transaction do
    directory = check_directory_sql(directory_id)

    acl = Security.get_access_rights(directory_id, session[:id])
    raise AccessRightsError.new('directory', directory_id, session[:id]) unless acl[1]

    inodes = directory.inodes.map { |i| Inodes.info(i[:id]) }
    inodes.each { |i| i[:revision][:owner_name] = Sm::Member[i[:revision][:modified_by]].name }
    inodes
  end

  content_type :json
  JSON[ :status => 'ok', :inodes => inodes ]
end


# GET /directory/:directory_id/root_for_member
#
get %r{^/directory/(\d+)/root_for_member$} do
  directory_id = params[:captures][0].to_i

  root_id = DB.transaction do
    directory = check_directory_sql(directory_id)

    acl = Security.get_access_rights(directory_id, session[:id])
    raise AccessRightsError.new('directory', directory_id, session[:id]) unless acl[1]

    if(acl[0] == session[:id])
      Sm::Member[session[:id]].home_directory_id
    else
      Directories.find_shared_ancestor(directory_id)
    end
  end

  content_type :json
  JSON[ :status => 'ok', 'root_id' => root_id ]
end


# GET /directory/:directory_id/read_write
# Returns whether the directory is writable by the current member.
#
get %r{^/directory/(\d+)/read_write$} do
  directory_id = params[:captures][0].to_i

  read_write = DB.transaction do
    acl = Security.get_access_rights(directory_id, session[:id])
    raise AccessRightsError.new('directory', directory_id, session[:id]) unless acl[1]
    acl[2]
  end

  content_type :json
  JSON[ :status => 'ok', 'read_write' => read_write ]
end


# POST /directory
# Creates a new directory
#
# parent_id: ID of the parent directory
# name:      name of the new directory
#
post '/directory' do
  parent_id = param_to_int(:parent_id)
  name = param_non_empty(:name)

  ok, contents = catch(:msg) do
    DB.transaction do
      check_directory_sql(parent_id)

      acl = Security.get_access_rights(parent_id, session[:id])
      unless acl[2]
        raise AccessRightsError.new('directory', parent_id, session[:id]) unless acl[1]
        throw :msg, [ false, :err_dir_ro ]
      end

      directory_id = Directories.add(name, Utils.get_utc_now, parent_id)

      [ true, Directories.info(directory_id) ]
    end
  end

  content_type :json
  JSON[ ok ? { :status => 'ok', :directory => contents } : { :status => 'err', :err_msg => contents } ]
end


# PUT /directory/:directory_id/name
# Renames a directory
#
# directory_id: ID of the directory
# name:         new name of the directory
#
put %r{^/directory/(\d+)/name$} do
  directory_id = params[:captures][0].to_i
  name = param_non_empty(:name)

  ok, contents = catch(:msg) do
    DB.transaction do
      check_directory_sql(directory_id)

      acl = Security.get_access_rights(directory_id, session[:id])
      unless acl[2]
        raise AccessRightsError.new('directory', directory_id, session[:id]) unless acl[1]
        throw :msg, [ false, :err_dir_ro ]
      end

      msg = Directories.rename(directory_id, name)

      (msg.nil?) ? [ true, Directories.info(directory_id) ] : [ false, msg ]
    end
  end

  content_type :json
  JSON[ ok ? { :status => 'ok', :directory => contents } : { :status => 'err', :err_msg => contents } ]
end
