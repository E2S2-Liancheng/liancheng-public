#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'digest'
require 'fileutils'


# We make some important assumption about hexdigest, we'd better check for it.
# That is, leading zeros are conserved in the result string.
#
raise unless Digest::MD5.hexdigest('1803305') == '00000f7264c27ba6fea0c837ed6aa0aa'


# Check access to file storage root folder
begin
  test_dir = File.join(SETTINGS['storage_file']['root_dir'], 'test_access')
  Dir.mkdir(test_dir)
  Dir.rmdir(test_dir)
end


module StorageFiles

  # Finds the deepest level of existing directory for a given MD5 digest.
  #
  def self.get_existing_directory(md5_id)
    path = SETTINGS['storage_file']['root_dir']

    loop do
      temp_path = File.join(path, md5_id.slice!(0, 2))
      return path unless Dir.exists?(temp_path)

      path = temp_path
    end
  end


  # Locates and opens a file (read-only), based on its id.
  # Opening the file ensures that it remains accessible even if moved somewhere else meanwhile.
  #
  def self.get_file_handle(id)
    md5 = Sm::StorageFile[id].md5_id

    f_name = id.to_s
    sub_dirs = ""

    loop do
      path = File.join(SETTINGS['storage_file']['root_dir'], sub_dirs, f_name)
      return File.open(path) if File.exists?(path) && !File.directory?(path)

      raise "Unable to locate file ##{id}" if md5.empty?
      sub_dirs = File.join(sub_dirs, md5.slice!(0, 2))
    end
  end


  # Computes the SHA1 sum of the given opened file.
  # File is read by chunks of 16Mo.
  # Returns the hexadecimal string.
  #
  def self.get_sha1sum(fh)
    fh.rewind
    d = Digest::SHA1.new
    while block = fh.read(2**24)
      d << block
    end
    d.hexdigest
  end


  # Find copy of a given opened file.
  # Uses a file handle as input.
  # Returns a storage_file id if a copy was found, nil otherwise.
  #
  def self.find_copy(fh, sha1sum = nil)
    sha1sum ||= get_sha1sum(fh)

    f_size = fh.stat.size

    copies = Sm::StorageFile.where(:sha1sum => sha1sum).map(:id)
    return nil if copies.empty?

    copies.find do |sf|
      stored_file = get_file_handle(sf)
      if f_size == stored_file.size
        fh.rewind
        FileUtils.compare_stream(fh, stored_file)
      else
        false
      end
    end
  end


  # Saves the opened file and stores the necessary information in the database.
  # Returns an array of two values:
  # - the database id of the file, once stored
  # - a flag set to true if a new entry was created, false if the file was already stored
  #
  def self.save_file(fh)
    sha1sum = get_sha1sum(fh)

    copy = find_copy(fh, sha1sum)
    return [copy, false] if copy

    rs = DB[Q_SAVE, sha1sum].first

    id = rs[:id]
    md5_id = rs[:md5_id]

    dest = File.join(get_existing_directory(md5_id), id.to_s)
    FileUtils.mv(fh.path, dest)
    FileUtils.chmod(0644, dest) if File.owned?(dest)

    [id, true]
  end

  Q_SAVE = <<-EOF.unindent
    INSERT INTO storage_file(md5_id, sha1sum)
    VALUES(encode(digest(lastval()::text, 'md5'), 'hex'), ?)
    RETURNING id, md5_id
  EOF

end
