#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

module Security
  # Returns the access rights to a given directory for a given member.
  # Returns an array of three values:
  # - owner id
  # - member can read
  # - member can write
  #
  def self.get_access_rights(directory_id, member_id)
    owner_id = Directories.get_owner_id(directory_id)
    shared_id = Directories.find_shared_ancestor(directory_id)

    if shared_id.nil?
      (owner_id == member_id) ? [member_id, true, true] : [owner_id, false, false]
    else
      if owner_id == member_id
        [member_id, true, true]
      else
        [owner_id, Shares.can_read?(shared_id, member_id), Shares.can_write?(shared_id, member_id)]
      end
    end
  end
end
