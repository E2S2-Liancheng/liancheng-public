#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

module Ops

  def self.check_members_crews(src_directory_id, dir_ids, inode_ids)
    pb = dir_ids.select { |d| Sm::Directory[d].nil? }
    raise "Directory(ies) #{pb.map { |i| "##{i}" }.join(', ')} do(es) not exist" unless pb.empty?
    pb = inode_ids.select { |d| Sm::Inode[d].nil? }
    raise "File(s) #{pb.map { |i| "##{i}" }.join(', ')} do(es) not exist" unless pb.empty?

    pb = dir_ids.select { |d| Sm::Directory[d].parent_id != src_directory_id }
    raise "Directory(ies) #{pb.map { |i| "##{i}" }.join(', ')} not in source directory ##{src_directory_id}" \
                                                                                                    unless pb.empty?
    pb = inode_ids.select { |d| Sm::Inode[d].directory_id != src_directory_id }
    raise "File(s) #{pb.map { |i| "##{i}" }.join(', ')} not in source directory ##{src_directory_id}" \
                                                                                                    unless pb.empty?
  end

end


post '/ops/delete' do
  directory_ids = params[:dirs] ? param_to_array_ints(:dirs) : []
  inode_ids = params[:inodes] ? param_to_array_ints(:inodes) : []

  ok, contents = catch(:msg) do
    DB.transaction do
      p1 = Sm::Directory.where(:id => directory_ids).select(:parent_id).distinct
      p2 = Sm::Inode.where(:id => inode_ids).select(:directory_id).distinct
      if p1.count > 1 || p2.count > 1 ||
         p1.count > 0 && p2.count > 0 && p1.first.parent_id != p2.first.directory_id
        msg = "Some directories and/or inodes do not have the same parent.\n" +
              "Directory #: #{directory_ids.join(', ')}\n" +
              "Inodes #: #{inode_ids.join(', ')}"
        raise msg
      end

      src_directory_id = p1.count > 0 ? p1.first.parent_id : p2.first.directory_id
      acl = Security.get_access_rights(src_directory_id, session[:id])
      unless acl[2]
        raise AccessRightsError.new('directory', src_directory_id, session[:id]) unless acl[1]
        throw :msg, [ false, :err_dir_ro ]
      end

      ts = Utils.get_utc_now
      path = Directories.get_path(src_directory_id, session[:id])
      destination_id = Directories.add_contents( "#{ts.strftime('%FT%T.%L')} #{path}",
                                                 ts,
                                                 Sm::Member[session[:id]].trash_directory_id )
      deleted = []
      errors = []

      directory_ids.each do |d|
        result, info = Directories.delete(d, destination_id)
        if result
          deleted.concat(info)
        else
          errors << { :msg => info, :dir_id => d }
        end
      end

      inode_ids.each do |i|
        result = Inodes.delete(i, destination_id)
        if result.nil?
          deleted << { :dir_id => src_directory_id, :inode_id => i }
        else
          errors << { :msg => result, :dir_id => src_directory_id, :inode_id => i,  }
        end
      end

      [ true, { :deleted => deleted, :errors => errors } ]
    end
  end

  msg = if ok
    { :status => 'ok', :deleted => contents[:deleted], :errors => contents[:errors] }
  else
    { :status => 'err', :err_msg => contents }
  end

  content_type :json
  JSON[msg]
end


post '/ops/copy_paste' do
  src_directory_id = param_to_int(:src)
  dst_directory_id = param_to_int(:dst)

  dir_ids = params[:dirs] ? param_to_array_ints(:dirs) : []
  inode_ids = params[:inodes] ? param_to_array_ints(:inodes) : []

  ok, contents, info = catch(:msg) do
    DB.transaction do
      src_directory = check_directory_sql(src_directory_id)
      dst_directory = check_directory_sql(dst_directory_id)

      raise "Source and destination are the same (##{src_directory_id})" if src_directory_id == dst_directory_id

      acl = Security.get_access_rights(src_directory_id, session[:id])
      raise AccessRightsError.new('directory', src_directory_id, session[:id]) unless acl[1]

      acl = Security.get_access_rights(dst_directory_id, session[:id])
      unless acl[2]
        raise AccessRightsError.new('directory', dst_directory_id, session[:id]) unless acl[1]
        throw :msg, [ false, :err_dir_ro ]
      end

      Ops.check_members_crews(src_directory_id, dir_ids, inode_ids)

      conflicts = Directories.start_check_paste(dir_ids, inode_ids, dst_directory_id, false, session[:id])
      throw :msg, [ false, :err_dir_paste_conflict, conflicts ] unless conflicts.empty?

      ts = Utils.get_utc_now
      new_inodes = inode_ids.map { |f| Inodes.copy(f, dst_directory_id, ts) }
      new_dirs = dir_ids.map { |d| Directories.copy(d, dst_directory_id, nil, ts) }

      [ true, (new_dirs.flatten(1) + new_inodes).select { |o| o[:revision].nil? || o[:revision][:revision] == 1 } ]
    end
  end

  msg = if ok
    { :status => 'ok', :pasted => contents }
  else
    { :status => 'err', :err_msg => contents, :info => info }
  end

  content_type :json
  JSON[msg]
end


post '/ops/cut_paste' do
  src_directory_id = param_to_int(:src)
  dst_directory_id = param_to_int(:dst)

  dir_ids = params[:dirs] ? param_to_array_ints(:dirs) : []
  inode_ids = params[:inodes] ? param_to_array_ints(:inodes) : []

  ok, contents, info = catch(:msg) do
    DB.transaction do
      src_directory = check_directory_sql(src_directory_id)
      dst_directory = check_directory_sql(dst_directory_id)

      raise "Source and destination are the same (##{src_directory_id})" if src_directory_id == dst_directory_id

      acl = Security.get_access_rights(src_directory_id, session[:id])
      unless acl[2]
        raise AccessRightsError.new('directory', src_directory_id, session[:id]) unless acl[1]
        throw :msg, [ false, :err_dir_ro ]
      end

      acl = Security.get_access_rights(dst_directory_id, session[:id])
      unless acl[2]
        raise AccessRightsError.new('directory', dst_directory_id, session[:id]) unless acl[1]
        throw :msg, [ false, :err_dir_ro ]
      end

      Ops.check_members_crews(src_directory_id, dir_ids, inode_ids)

      conflicts = Directories.start_check_paste(dir_ids, inode_ids, dst_directory_id, true, session[:id])
      throw :msg, [ false, :err_dir_paste_conflict, conflicts ] unless conflicts.empty?

      changes = { :created => [], :deleted => [] }
      ts = Utils.get_utc_now
      dir_ids.map { |d| Directories.cut_paste(d, dst_directory_id, ts, changes) }
      inode_ids.map { |f| Inodes.cut_paste(f, dst_directory_id, ts, changes) }

      [ true, changes, nil ]
    end
  end

  msg = if ok
    { :status => 'ok', :created => contents[:created], :deleted => contents[:deleted] }
  else
    { :status => 'err', :err_msg => contents, :info => info }
  end

  content_type :json
  JSON[ msg ]
end
