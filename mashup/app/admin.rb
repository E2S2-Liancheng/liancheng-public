#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

module Admin
  #
  #-- WARNING password must not be logged
  #
  def self.check_auth_admin(username, password)
    member = DB.do_not_log do
      Sm::Admin.where(:username => username, :password => Sequel.function(:crypt, password, :password))
                .select(:id).first
    end

    (member.nil?) ? nil : member.id
  end

  def self.get_stats()
    members = Sm::Member
    statm = { :total => members.count,
              :pending => members.where(:account_status_id => Sm::AccountStatus::ValidationPending).count,
              :active => members.where(:account_status_id => Sm::AccountStatus::Active).count,
            }

    revisions = Sm::Revision.select(:size, :storage_file_id)
    stati = { :directories => Sm::Directory.count,
              :inodes => Sm::Inode.count,
              :revisions => revisions.count,
              :total_size => revisions.distinct(:storage_file_id).sum(:size).to_int,
            }

    jobs = Sm::Job
    statj = { :total => jobs.count,
              :created => jobs.where(:job_status_id => 1).count,
              :started => jobs.where(:job_status_id => 2).count,
              :cancelled => jobs.where(:job_status_id => 3).count,
              :failed => jobs.where(:job_status_id => 4).count,
              :completed => jobs.where(:job_status_id => 5).count,
            }

    { :members => statm, :inodes => stati, :jobs => statj }
  end
end


#
#-- WARNING password must not be logged
#
post '/admin/login' do
  content_type :json

  if (credentials = Admin.check_auth_admin(params[:username], params[:hashed_password]))
    session[:admin] = credentials
#    session[:conn] = DB_CONN

    JSON[ :status => 'ok' ]
  else
    JSON[ :status => 'err', :err_msg => 'Invalid username or password' ]
  end
end

post '/admin/logout' do
  session.clear

  content_type :json
  JSON[ :status => 'ok' ]
end

get '/admin/stats' do
  halt 500 unless session[:admin]

  stats = DB.transaction { Admin.get_stats }

  content_type :json
  JSON[ :status => 'ok', :stats => stats ]
end
