#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'digest'


module Publish
  UUID_RE = /[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}/
end


# GET /publish/:inode_id
# Returns the current publication information about the node.
#
get %r{^/publish/(\d+)$} do
  inode_id = params[:captures][0].to_i

  publish =
    DB.transaction do
      inode = check_inode_sql(inode_id)

      acl = Security.get_access_rights(inode.directory_id, session[:id])
      raise AccessRightsError.new('inode', inode_id, session[:id]) unless acl[1]

      { :inode => Inodes.info(inode_id),
        :obfuscated => Sm::FileObfuscated.where(:member_id => session[:id], :inode_id => inode_id)
                                         .get_info.all.map(&:values),
        :username => Sm::Member[session[:id]].username,
        :published => !Sm::FilePublished[:member_id => session[:id], :inode_id => inode_id].nil? }
    end

  content_type :json
  JSON[ :status => 'ok', :publish => publish ]
end


# GET /publish/obfuscated/:uuid
# Sends the file's contents.
#
# WARNING Reachable without login
#
get %r{^/publish/obfuscated/(#{Publish::UUID_RE})$} do
  uuid = params[:captures][0]

  file, attrs =
    DB.transaction do
      obs = Sm::FileObfuscated[uuid]
      raise ObjectNotFoundError.new('uuid', uuid, session[:id]) if obs.nil?

      inode = check_inode_sql(obs.inode_id)
      revision = check_revision_sql(obs.inode_id, -1)

      [ StorageFiles.get_file_handle(revision.storage_file_id),
        { :filename => inode.name,
          :disposition => 'attachment',
          :type => revision.mime_type.name } ]
    end

  send_file(file.path, attrs)
end


# GET /publish/obfuscated/:uuid/name
# Sends the file's name.
#
# WARNING Reachable without login
#
get %r{^/publish/obfuscated/(#{Publish::UUID_RE})/name$} do
  uuid = params[:captures][0]

  name =
    DB.transaction do
      obs = Sm::FileObfuscated[uuid]
      raise ObjectNotFoundError.new('uuid', uuid, session[:id]) if obs.nil?

      inode = check_inode_sql(obs.inode_id)
      inode.name
    end

  content_type :json
  JSON[ :status => 'ok', :name => name ]
end


# POST /publish/:inode_id/obfuscated
# Creates a new obfuscated URL for the inode.
#
post %r{^/publish/(\d+)/obfuscated} do
  inode_id = params[:captures][0].to_i
  annotation = params[:annotation]

  uuid = nil

  DB.transaction do
    inode = check_inode_sql(inode_id)

    acl = Security.get_access_rights(inode.directory_id, session[:id])
    raise AccessRightsError.new('inode', inode_id, session[:id]) unless acl[1]

    loop do
      uuid = SecureRandom.uuid
      break if Sm::FileObfuscated[uuid].nil?
    end

    Sm::FileObfuscated.insert(:id => uuid,
                              :member_id => session[:id],
                              :inode_id => inode_id,
                              :annotation => annotation)
  end

  content_type :json
  JSON[ :status => 'ok', :obfuscated => { :id => uuid, :annotation => annotation } ]
end


# DELETE /publish/obfuscated/:uuid
# Deletes the specified obfuscated URL.
#
delete %r{^/publish/obfuscated/([-0-9a-f]+)} do
  uuid = params[:captures][0]

  raise ObjectNotFoundError.new('uuid', uuid, session[:id]) unless uuid =~ Publish::UUID_RE

  DB.transaction do
    obfuscated = Sm::FileObfuscated[uuid]
    raise ObjectNotFoundError.new('uuid', uuid, session[:id]) if obfuscated.nil?

    raise AccessRightsError.new('uuid', uuid, session[:id]) unless obfuscated.member_id == session[:id]

    inode = check_inode_sql(obfuscated.inode_id)

    acl = Security.get_access_rights(inode.directory_id, session[:id])
    raise AccessRightsError.new('inode', inode_id, session[:id]) unless acl[1]

    obfuscated.delete
  end

  content_type :json
  JSON[ :status => 'ok' ]
end


# PUT /publish/home/:inode_id
# Publishes the given inode as part of HOME.
#
put %r{^/publish/home/(\d+)$} do
  inode_id = params[:captures][0].to_i

  DB.transaction do
    inode = check_inode_sql(inode_id)

    acl = Security.get_access_rights(inode.directory_id, session[:id])
    raise AccessRightsError.new('inode', inode_id, session[:id]) unless acl[1]

    raise BadRequestError.new("Username for Member ##{session[:id]} not set") if Sm::Member[session[:id]].username.nil?
    raise BadRequestError.new("Inode ##{inode_id} already published by Member ##{session[:id]}") \
                                      unless Sm::FilePublished[:member_id => session[:id], :inode_id => inode_id].nil?

    Sm::FilePublished.insert(:member_id => session[:id], :inode_id => inode_id)
  end

  content_type :json
  JSON[ :status => 'ok' ]
end


# DELETE /publish/home/:inode_id
# Removes the given inode from HOME.
#
delete %r{^/publish/home/(\d+)$} do
  inode_id = params[:captures][0].to_i

  DB.transaction do
    inode = check_inode_sql(inode_id)

    acl = Security.get_access_rights(inode.directory_id, session[:id])
    raise AccessRightsError.new('inode', inode_id, session[:id]) unless acl[1]

    raise BadRequestError.new("Username for Member ##{session[:id]} not set") if Sm::Member[session[:id]].username.nil?
    pub = Sm::FilePublished[:member_id => session[:id], :inode_id => inode_id]
    raise BadRequestError.new("Inode ##{inode_id} not published by Member ##{session[:id]}") if pub.nil?

    pub.delete
  end

  content_type :json
  JSON[ :status => 'ok' ]
end


# GET /publish/home/info
# Returns information about a HOME URL to prepare file download.
#
# WARNING Reachable without login.
#
get '/publish/home/info' do
  username = param_non_empty(:username)
  path = param_non_empty(:path)

  member_id, inode_id, name =
    DB.transaction do
      member = Sm::Member[:username => username]
      raise ObjectNotFoundError.new('username', username, 0) if member.nil?

      path = path.split('/')
      raise ObjectNotFoundError.new('path', path, member.id) if path.size > 2

      inode_id =
        if path.size == 0
          -1
        else
          begin
            Integer(path[0])
          rescue ArgumentError
            raise ObjectNotFoundError.new('path[0]', path[0], member.id)
          end
        end

      raise BadRequestError.new("Mismatch between names for Inode ##{inode_id}") if Sm::Inode[inode_id].name != path[1]

      raise AccessRightsError.new('inode', inode_id, member.id) \
                                              if Sm::FilePublished[:member_id => member.id, :inode_id => inode_id].nil?

      [ member.id, inode_id, path[1] ]
    end

  content_type :json
  JSON[ :status => 'ok', :info => { :member_id => member_id, :inode_id => inode_id, :name => name } ]
end


# GET /publish/home/:member_id/:inode_id
# Sends the given file as published by the given member.
#
# WARNING Reachable without login.
#
get %r{^/publish/home/(\d+)/(\d+)$} do
  member_id = params[:captures][0].to_i
  inode_id = params[:captures][1].to_i

  file, attrs =
    DB.transaction do
      raise AccessRightsError.new('inode', inode_id, member_id) \
                                              if Sm::FilePublished[:member_id => member_id, :inode_id => inode_id].nil?

      inode = check_inode_sql(inode_id)
      revision = check_revision_sql(inode_id, -1)

      [ StorageFiles.get_file_handle(revision.storage_file_id),
        { :filename => inode.name,
          :disposition => 'attachment',
          :type => revision.mime_type.name } ]
    end

  send_file(file.path, attrs)
end
