#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'cgi'

PIPE_SERVER = SETTINGS['pipe_server']['scheme'] + '://' + SETTINGS['pipe_server']['host'] + ':' + SETTINGS['pipe_server']['port']


helpers do
  def check_http_code(res)
    halt 500 if res.status != 200
  end
end


get '/stream/list' do
  res = session[:pipe_conn].get(PIPE_SERVER + "/hobo/#{session[:id]}")

  msg_hobo = case res.status
    when 200 then JSON.parse(res.content).tap { |h| h.each { |a| a.delete('member_id') } }
    when 404 then []
    else halt 500
  end

  content_type :json
  JSON[ 'hobo' => msg_hobo ]
end


post '/stream/hobo' do
  login = param_value(:new_login)
  password = param_value(:password)
  devices = params[:devices] ? param_to_array(:devices) : nil

  raise BadRequestError.new("Empty string for login") if login.empty?
  raise BadRequestError.new("Empty string for password") if password.empty?
  raise BadRequestError.new("Empty string for device") if !devices.nil? && devices.any?(&:empty?)

  form = { :login => login,
           :password => password,
           :'devices[]' => devices }.delete_if { |_,v| v.nil? }
  res = session[:pipe_conn].post(PIPE_SERVER + "/hobo/#{session[:id]}", form)
  check_http_code(res)

  status 200
end


put '/stream/hobo' do
  old_login = param_value(:old_login)
  new_login = params[:new_login]
  password = params[:password]
  devices = params[:devices] ? param_to_array(:devices) : nil

  raise BadRequestError.new("Empty string for old_login") if old_login.empty?
  raise BadRequestError.new("Empty string for new_login") if !new_login.nil? && new_login.empty?
  raise BadRequestError.new("Empty string for password") if !password.nil? && password.empty?
  raise BadRequestError.new("Empty string for device") if !devices.nil? && devices.any?(&:empty?)

  new_login = nil if new_login == old_login

  form = { :old_login => old_login,
           :new_login => new_login,
           :password => password,
           :'devices[]' => devices }.delete_if { |_,v| v.nil? }
  res = session[:pipe_conn].put(PIPE_SERVER + "/hobo/#{session[:id]}/#{CGI.escape(old_login)}", form)
  check_http_code(res)

  status 200
end


delete '/stream/hobo' do
  login = param_value(:login)

  res = session[:pipe_conn].delete(PIPE_SERVER + "/hobo/#{session[:id]}/#{CGI.escape(login)}")
  check_http_code(res)

  status 200
end


post '/stream/hobo/record' do
  login = param_value(:login)
  device_id = param_value(:device_id)
  directory_id = param_to_int(:directory_id)
  file_name = param_value(:file_name)

  raise BadRequestError.new("Empty string for file_name") if file_name.empty?

  form = { :device_id => device_id,
           :directory_id => directory_id,
           :file_name => file_name }
  res = session[:pipe_conn].post(PIPE_SERVER + "/hobo/#{session[:id]}/#{CGI.escape(login)}/record", form)
  check_http_code(res)

  status 200
end


delete '/stream/hobo/record' do
  login = param_value(:login)
  device_id = param_value(:device_id)

  form = { :device_id => device_id }
  res = session[:pipe_conn].delete(PIPE_SERVER + "/hobo/#{session[:id]}/#{CGI.escape(login)}/record", form)
  check_http_code(res)

  status 200
end


post '/stream/file' do
  if (token = params[:token]).nil? || token.empty? || token != SETTINGS['pipe_server']['token']
    msg = "Incorrect token for client request from #{request.ip} on POST /stream/file"
    $logger.warn(msg)
    halt 401
  end

  member_id = param_to_int(:member_id)
  real_path = param_value(:real_path)
  directory_id = param_to_int(:directory_id)
  file_name = param_value(:file_name)

  SyncSingle.sync_write_catch(:msg) do
    DB_CONN.transaction do |conn|
      mime_type_id = MimeTypes.find_id('application/x-urbandb-stream')
      file = File.open(real_path)
      storage_file_id, created = StorageFiles.save_file(file)

      Inodes.add(conn, directory_id, file_name, file.size, member_id, mime_type_id, storage_file_id, nil) if created
    end
  end

  status 200
end
