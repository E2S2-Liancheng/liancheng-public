#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

module Crews

  # Adds a new Crew for the given Member.
  # Returns the Crew ID.
  #
  def self.add(creator_id, name, members)
    crew_id = Sm::Crew.insert(:name => name, :creator_member_id => creator_id)
    members.each { |m| Sm::CrewMember.insert(:crew_id => crew_id, :member_id => m) }

    crew_id
  end

  # Verifies which members are impacted for file sharing.
  # Returns a complicated thing.
  # Raises an exception in case of an error.
  #
  def self.prepare_update(crew_id, members)
    DB.transaction do
      dirs = Sm::ShareWithCrew.where(:crew_id => crew_id).select(:directory_id).all.map { |swc| swc.directory_id }
      return nil if dirs.empty?

      old_member_ids = Sm::CrewMember.where(:crew_id => crew_id).select(:member_id).all.map { |cm| cm.member_id }.to_set
      new_member_ids = members.to_set

      added = new_member_ids - old_member_ids
      removed = old_member_ids - new_member_ids
      return nil if added.empty? && removed.empty?

      ret = {}
      unless added.empty?
        ret[:added] = []

        added.each do |m|
            gained = dirs.map do |d|
              cpt = Sm::ShareWithCrew.where(:directory_id => d)
                                     .join(:crew_member, :crew_id => :crew_id)
                                     .where(:member_id => m).count +
                    Sm::ShareWithMember.where(:directory_id => d, :member_id => m).count
              (cpt == 0) ? { :id => d, :name => Directories[d].name } : nil
            end.compact

            unless gained.empty?
              member = { :id => m, :name => Members[m].name }
              ret[:added] << { :member => member, :directories => gained }
            end
        end

        ret.delete(:added) if ret[:added].empty?

        members = added.map { |m| { :id => m, :name => Members[m].name } }
        directories = dirs.map { |d| { :id => d, :name => Directories[d].name } }
        ret[:added] = { :members => members, :directories => directories }
      end

      unless removed.empty?
        ret[:removed] = []

        removed.each do |m|
          lost = dirs.map do |d|
            cpt = Sm::ShareWithCrew.where(:directory_id => d)
                                   .join(:crew_member, :crew_id => :crew_id)
                                   .where(:member_id => m).count +
                  Sm::ShareWithMember.where(:directory_id => d, :member_id => m).count
            (cpt == 1) ? { :id => d, :name => Directories[d].name } : nil
          end.compact

          unless lost.empty?
            member = { :id => m, :name => Members[m].name }
            ret[:removed] << { :member => member, :directories => lost }
          end
        end

        ret.delete(:removed) if ret[:removed].empty?
      end

      ret
    end
  end


  # Verifies which members are impacted for file sharing.
  # Returns a complicated thing.
  # Raises an exception in case of an error. Or not.
  #
  def self.prepare_delete(crew_id)
    DB.transaction do
      dirs = Sm::ShareWithCrew.where(:crew_id => crew_id).select(:directory_id).all.map { |swc| swc.directory_id }
      return nil if dirs.empty?

      member_ids = Sm::CrewMember.where(:crew_id => crew_id).select(:member_id).all.map { |cm| cm.member_id }.to_set
      return nil if member_ids.empty?

      ret = []

      member_ids.each do |m|
        lost = dirs.map do |d|
          cpt = Sm::ShareWithCrew.where(:directory_id => d)
                                  .join(:crew_member, :crew_id => :crew_id)
                                  .where(:member_id => m).count +
                Sm::ShareWithMember.where(:directory_id => d, :member_id => m).count
          (cpt == 1) ? { :id => d, :name => Directories[d].name } : nil
        end.compact

        unless lost.empty?
          member = { :id => m, :name => Members[m].name }
          ret << { :member => member, :directories => lost }
        end
      end

      (ret.empty?) ? nil : ret
    end
  end


  # Updates a crew.
  #
  def self.update_crew(crew_id, name, members, give_copy)
    crew = Sm::Crew[crew_id]
    old_members = crew.members.map { |m| m.id }.to_set
    new_members = members.to_set

    old_name = crew.name
    name = (old_name != name) ? name : nil
    add_members = new_members - old_members
    del_members = old_members - new_members
    directory_ids = Sm::ShareWithCrew.where(:crew_id => crew_id).all.map { |swc| swc.directory_id }

    Sm::Crew.where(:id => crew_id).update(:name => name) if name
    del_members.each do |m|
      Sm::CrewMember.where(:crew_id => crew_id, :member_id => m).delete

      directory_ids.each do |d|
        cpt = Sm::ShareWithCrew.where(:directory_id => d)
                          .join(:crew_member, :crew_id => :crew_id)
                          .where(:member_id => m).count +
              Sm::ShareWithMember.where(:directory_id => d, :member_id => m).count
        if cpt == 0
          invite = Sm::ShareInvite.where(:directory_id => d, :member_id => m)
          if invite.count == 1
            if give_copy && invite.first.share_invite_status_id == Sm::ShareInviteStatus::Accepted
              Directories.copy( d,
                                Sm::Member[m].home_directory_id,
                                Sm::Directory[d].name,
                                Utils.get_utc_now )
            end
            invite.delete
          end
        end
      end
    end
    add_members.each do |m|
      Sm::CrewMember.insert(:crew_id => crew_id, :member_id => m)

      directory_ids.each { |d| Shares.invite(d, m) }
    end
  end


  # Removes a crew.
  #
  def self.delete_crew(crew_id, give_copy)
    DB.transaction do
      directory_ids = Sm::ShareWithCrew.where(:crew_id => crew_id).all.map { |c| c.directory_id }
      directory_ids.each { |d| Shares.remove_crew(d, crew_id, give_copy) }

      Sm::CrewMember.where(:crew_id => crew_id).delete
      Sm::Crew.where(:id => crew_id).delete
    end
  end
end


helpers do
  def check_crew_sql(crew_id)
    Sm::Crew[crew_id].tap { |c| raise ObjectNotFoundError.new('crew', crew_id, session[:id]) if c.nil? }
  end
end


# GET /crew/:crew_id
# Returns information about a given crew.
#
get %r{^/crew/(\d+)$} do
  crew_id = params[:captures][0].to_i

  crew =
    DB.transaction do
      crew = check_crew_sql(crew_id)
      raise AccessRightsError.new('crew', crew_id, session[:id]) unless crew.creator_member_id == session[:id]

      crew.get_info
    end

  content_type :json
  JSON[ :status => 'ok', :crew => crew ]
end


# GET /crew/:crew_id/members
# Returns the list of members included in a crew
#
get %r{^/crew/(\d+)/members$} do
  crew_id = params[:captures][0].to_i

  members =
    DB.transaction do
      crew = check_crew_sql(crew_id)
      raise AccessRightsError.new('crew', crew_id, session[:id]) unless crew.creator_member_id == session[:id]

      crew.members_dataset.select(:id, :name).all.map(&:to_hash)
    end

  content_type :json
  JSON[ :status => 'ok', :members => members ]
end


# POST /crew
# Creates a new crew for the current member
#
# name: name of the new crew
# members: array of member IDs belonging to the new crew
#
post '/crew' do
  name = param_non_empty(:name)
  members = (params[:members].nil?) ? [] : param_to_array_ints(:members)

  raise BadRequestError.new("Invalid input: can't include self (##{session[:id]})") if members.include?(session[:id])

  crew =
    DB.transaction do
      members.each { |m| check_member_sql(m) }
      crew_id = Crews.add(session[:id], name, members)
      Sm::Crew[crew_id].get_info
    end

  content_type :json
  JSON[ 'status' => 'ok', 'crew' => crew ]
end


# POST /crew/:crew_id/prepare_update
# Verifies the impact of a group update on file sharing.
#
post %r{^/crew/(\d+)/prepare_update} do
  crew_id = params[:captures][0].to_i
  members = (params[:members].nil?) ? [] : param_to_array_ints(:members)

  impacted_members =
    DB.transaction do
      crew = check_crew_sql(crew_id)
      raise AccessRightsError.new('crew', crew_id, session[:id]) unless crew.creator_member_id == session[:id]
      members.each { |m| check_member_sql(m) }

      Crews.prepare_update(crew_id, members)
    end

  content_type :json
  JSON[ :status => 'ok', :impacted_members => impacted_members ]
end


# POST /crew/:crew_id/prepare_delete
# Verifies the impact of a group deletion on file sharing.
#
post %r{^/crew/(\d+)/prepare_delete} do
  crew_id = params[:captures][0].to_i

  impacted_members, name =
    DB.transaction do
      crew = check_crew_sql(crew_id)
      raise AccessRightsError.new('crew', crew_id, session[:id]) unless crew.creator_member_id == session[:id]

      [ Crews.prepare_delete(crew_id), crew.name ]
    end

  content_type :json
  JSON[ :status => 'ok', :impacted_members => impacted_members, :name => name ]
end


# PUT /crew/:crew_id
# Updates the crew's name and/or members
#
# name: new name of the crew, can be nil (but not an empty string)
# members: array of the new member IDs belonging to the crew
# give_copy: if set, members losing access to shares will receive a copy of them.
#
put %r{^/crew/(\d+)$} do
  crew_id = params[:captures][0].to_i
  name = param_non_empty(:name)
  members = (params[:members].nil?) ? [] : param_to_array_ints(:members)
  give_copy = !params[:give_copy].nil?

  DB.transaction do
    crew = check_crew_sql(crew_id)
    raise AccessRightsError.new('crew', crew_id, session[:id]) unless crew.creator_member_id == session[:id]
    members.each { |m| check_member_sql(m) }

    Crews.update_crew(crew_id, name, members, give_copy)
  end

  content_type :json
  JSON[ :status => 'ok' ]
end


# DELETE /crew/:crew_id
# Deletes a crew, including from the shares.
# If give_copy is set, then members losing access to shares will receive a copy of them.
#
delete %r{^/crew/(\d+)$} do
  crew_id = params[:captures][0].to_i
  give_copy = !params[:give_copy].nil?

  DB.transaction do
    crew = check_crew_sql(crew_id)
    raise AccessRightsError.new('crew', c, session[:id]) unless crew.creator_member_id == session[:id]

    Crews.delete_crew(crew_id, give_copy)
  end

  content_type :json
  JSON[ :status => 'ok' ]
end
