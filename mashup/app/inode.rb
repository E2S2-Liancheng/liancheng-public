#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'csv'


# This module holds the tuples of mime_type.
# MIME types are case insensitive so we systematically store them in lower case.
#
module MimeTypes

  # Finds the id of a given MIME type value.
  # If it does not exist and create_if_missing is true, a new tuple is added in the database.
  # Returns nil if none was found and create_if_missing is false.
  #
  def self.find_id(name, create_if_missing = true)
    name.downcase!
    DB.transaction do
      mt = Sm::MimeType.where(:name => name)

      if mt.empty?
        create_if_missing ? mt.insert(:name => name) : nil
      else
        mt.first.id
      end
    end
  end

end


module Inodes

  # Gets the latest revision of a given inode.
  #
  def self.latest_revision(inode_id)
    revision = Sm::Revision.where(:inode_id => inode_id).max(:revision)
  end


  # Gets an inode information for a given revision.
  # If no revision or -1 is provided, selects the latest revision.
  #
  def self.info(inode_id, revision = nil)
    revision = latest_revision(inode_id) if revision.nil? || revision == -1

    inode = Sm::Inode[inode_id]
    revision = Sm::Revision[inode_id, revision]

    { :id => inode_id,
      :name => inode.name,
      :directory_id => inode.directory_id,
      :revision => { :revision => revision.revision,
                     :size => revision.size,
                     :modified_by => revision.modified_by,
                     :modified_at => revision.modified_at.strftime('%s').to_i,
                     :mime_type => Sm::MimeType[revision.mime_type_id].name } }
  end


  # Determines the complete path to the file name.
  #
  def self.get_path(inode_id, member_id)
    inode = Sm::Inode[inode_id]
    path = Directories.get_path(inode.directory_id, member_id)
    "#{path}/#{inode.name}"
  end


  # Creates a new inode in the virtual file system, or a new revision.
  # Returns the inode's id and revision, and creation/modification timestamp.
  #
  def self.add_contents(directory_id, name, size, modified_by, mime_type_id, storage_file_id, timestamp = nil)
    timestamp ||= Utils.get_utc_now

    inode = Sm::Inode.where(:directory_id => directory_id, :name => name).first

    if inode
      inode_id = inode.id
      revision = Sm::Revision.where(:inode_id => inode_id).max(:revision) + 1
    else
      inode_id = Sm::Inode.insert(:name => name, :directory_id => directory_id)
      revision = 1
    end

    Sm::Revision.insert( :inode_id => inode_id,
                        :revision => revision,
                        :size => size,
                        :modified_by => modified_by,
                        :modified_at => timestamp.to_s,
                        :mime_type_id => mime_type_id,
                        :storage_file_id => storage_file_id )

    [inode_id, revision, timestamp]
  end


  # Renames the given inode.
  # Returns nil in case of success, an error message otherwise.
  #
  def self.rename(inode_id, name)
    inode = Sm::Inode[inode_id]

    others = inode.directory.children_dataset.map(:name).to_set +
              inode.directory.inodes_dataset.map(:name).to_set
    return :err_inode_ren_already if others.include?(name)

    inode.update(:name => name)

    nil
  end


  # Deletes the given inode.
  #
  def self.delete(inode_id, destination_id)
    Sm::FileObfuscated.where(:inode_id => inode_id).delete
    Sm::FilePublished.where(:inode_id => inode_id).delete

    Sm::Inode[inode_id].update(:directory_id => destination_id)

    nil
  end


  # Copies an inode into another directory.
  # Returns the new inode's information.
  #
  def self.copy(inode_id, directory_id, timestamp)
    i = Sm::Inode[inode_id]
    r = Sm::Revision.where(:inode_id => inode_id)
    r = r.where(:revision => r.select{max(revision)}).first

    n = add_contents(directory_id, i.name, r.size, r.modified_by, r.mime_type_id, r.storage_file_id, timestamp)

    info(n[0])
  end


  # Moves an inode into another directory.
  # Returns the updated inode's information.
  #
  def self.cut_paste(inode_id, directory_id, timestamp, changes)
    inode = Sm::Inode[inode_id]
    old_dir = inode.directory_id

    Sm::Inode[inode_id].update(:directory_id => directory_id)

    changes[:created] << info(inode_id)
    changes[:deleted] << { :inode_id => inode_id, :dir_id => old_dir }
  end

end

helpers do
  def check_inode_sql(inode_id)
    Sm::Inode[inode_id].tap { |i| raise ObjectNotFoundError.new('inode', inode_id, session[:id]) if i.nil? }
  end

  def check_revision_sql(inode_id, revision)
    revision = Inodes.latest_revision(inode_id) if revision == -1
    Sm::Revision[inode_id, revision].tap { |r| raise ObjectNotFoundError.new("inode ##{inode_id}/revision", revision, session[:id]) if r.nil? }
  end

  def check_inode_revision_sql(inode_id, revision)
    check_inode_sql(inode_id)
    check_revision_sql(inode_id, revision)
  end
end


# GET /inode/:inode_id
# Returns an inode's information including only its latest revision.
#
get %r{^/inode/(\d+)$} do
  inode_id = params[:captures][0].to_i

  inode =
    DB.transaction do
      inode = check_inode_sql(inode_id)

      acl = Security.get_access_rights(inode.directory_id, session[:id])
      raise AccessRightsError.new('inode', inode_id, session[:id]) unless acl[1]

      Inodes.info(inode_id)
    end

  content_type :json
  JSON[ :status => 'ok', :inode => inode ]
end


# GET /inode/:inode_id/:revision_id/contents
# Downloads the file given its inode and revision.
#
get %r{^/inode/(\d+)/(-1|\d+)/contents$} do
  inode_id = params[:captures][0].to_i
  revision_id = params[:captures][1].to_i

  file, attrs =
    DB.transaction do
      inode = check_inode_sql(inode_id)
      revision = check_revision_sql(inode_id, revision_id)

      acl = Security.get_access_rights(inode.directory_id, session[:id])
      raise AccessRightsError.new('inode', inode_id, session[:id]) unless acl[1]

      [ StorageFiles.get_file_handle(revision.storage_file_id),
        { :filename => inode.name,
          :disposition => 'attachment',
          :type => revision.mime_type.name } ]
    end

  send_file(file.path, attrs)
end


# POST /inode
# Uploads one or more files.
#
post '/inode' do
  $logger.info('post \'/inode/:directory_id\'')
  $logger.info(params)

  directory_id = param_to_int('directory_id')

  filenames = param_to_array('filename')
  uploads = param_to_array('upload')
  mimetypes = param_to_array('mime_type')
  raise BadRequestError, 'Wrong number of arguments' if filenames.length == 0 ||
                                                        filenames.length != uploads.length ||
                                                        filenames.length != mimetypes.length

  # MRI does not consider Tempfile as derived from File
  raise BadRequestError, 'No file(s) uploaded' if uploads.any? { |u| u[:tempfile].nil? ||
                                                                    !( u[:tempfile].is_a?(File) ||
                                                                        u[:tempfile].is_a?(Tempfile) ) }

  total, contents = catch(:msg) do
    DB.transaction do
      directory = check_directory_sql(directory_id)

      acl = Security.get_access_rights(directory_id, session[:id])
      unless acl[2]
        raise AccessRightsError.new('directory', directory_id, session[:id]) unless acl[1]
        throw :msg, [ nil, :err_dir_ro ]
      end

      files = filenames.zip(uploads.map { |f| f[:tempfile] }, mimetypes)

      # Replace the provided MIME types by the ones found by "file"
      files.map! { |f| [ f[0], f[1], `file -b --mime-type #{f[1].path}`.chomp ] }

      saved = []
      files.each do |name, file, mime_type|
        begin
          # skip the file if a folder of the same name already exists
          next if directory.children_dataset.map(:name).include?(name)

          if mime_type == ('text/plain' || 'application/json') && SETTINGS['jobs']['enabled'] && Workflow.is_a_workflow?(file)
            mime_type_id = MimeTypes.find_id('application/x-urbandb-workflow')
          else
            mime_type_id = MimeTypes.find_id(mime_type)
          end

          storage_file_id, created = StorageFiles.save_file(file)

          if !created && !(same_name = directory.inodes_dataset[:name => name]).nil?
            if Sm::Inode.latest_revision(same_name.id).storage_file_id == storage_file_id
              saved << Inodes.info(same_name.id)
              next
            end
          end

          id, revision, timestamp = Inodes.add_contents( directory_id,
                                                        name,
                                                        file.size,
                                                        session[:id],
                                                        mime_type_id,
                                                        storage_file_id )
          saved << Inodes.info(id)
        rescue => e
          $logger.error(e.msg_bt)
          $logger.error(saved)
        end
      end

      [ files.size, saved ]
    end
  end

  ret = case
    when total.nil? then { :status => 'err', :err_msg => contents }
    when total > contents.size then { :status => 'incomplete', :inodes => contents }
    else { :status => 'ok', :inodes => contents }
  end

  content_type :json
  JSON[ret]
end


# PUT /inode/:inode_id/name
# Renames an inode.
#
put %r{^/inode/(\d+)/name$} do
  inode_id = params[:captures][0].to_i
  name = param_non_empty('name')

  ok, contents = catch(:msg) do
    DB.transaction do
      inode = check_inode_sql(inode_id)
      acl = Security.get_access_rights(inode.directory_id, session[:id])
      unless acl[2]
        raise AccessRightsError.new('inode', inode_id, session[:id]) unless acl[1]
        throw :msg, [ false, :err_inode_ro ]
      end

      msg = Inodes.rename(inode_id, name)
      throw :msg, [ false, msg ] if msg

      [ true, Inodes.info(inode_id) ]
    end
  end

  content_type :json
  JSON[ ok ? { :status => 'ok', :inode => contents } : { :status => 'err', :err_msg => contents } ]
end


# GET /inode/:inode_id/:revision_id/import_csv
# Tries to prepare a file as a CSV import.
#
get %r{^/inode/(\d+)/(-1|\d+)/import_csv$} do
  inode_id = params[:captures][0].to_i
  revision_id = params[:captures][1].to_i

  ok, contents =
    DB.transaction do
      inode = check_inode_sql(inode_id)
      revision = check_revision_sql(inode_id, revision_id)

      acl = Security.get_access_rights(inode.directory_id, session[:id])
      raise AccessRightsError.new('inode', inode_id, session[:id]) unless acl[1]

      file = StorageFiles.get_file_handle(revision.storage_file_id)

      begin
        [ true, { 'header' => CSV.open(file, :encoding => 'bom|utf-8') { |csv| csv.readline }, 'path' => Inodes.get_path(inode_id, session[:id]) } ]
      rescue ArgumentError
        [ false, :err_wf_csv_parse ]
      rescue CSV::MalformedCSVError => e
        [ false, e.message ]
      end
    end

  content_type :json
  JSON[ ok ? { :status => 'ok', :inode => contents } : { :status => 'err', :err_msg => contents } ]
end


# GET /inode/:inode_id/:revision_id/display_csv
# Tries to prepare a file as CSV for display.
#
get %r{^/inode/(\d+)/(-1|\d+)/display_csv$} do
  inode_id = params[:captures][0].to_i
  revision_id = params[:captures][1].to_i

  ok, contents =
    DB.transaction do
      inode = check_inode_sql(inode_id)
      revision = check_revision_sql(inode_id, revision_id)

      acl = Security.get_access_rights(inode.directory_id, session[:id])
      raise AccessRightsError.new('inode', inode_id, session[:id]) unless acl[1]

      file = StorageFiles.get_file_handle(revision.storage_file_id)

      begin
        csv = CSV.read(file, :encoding => 'bom|utf-8')
        header = csv.shift
        [ true, { :name => inode.name, :header => header, :values => csv } ]
      rescue ArgumentError
        [ false, :err_wf_csv_parse ]
      rescue CSV::MalformedCSVError => e
        [ false, e.message ]
      end
    end

  content_type :json
  JSON[ ok ? { :status => 'ok', :contents => contents } : { :status => 'err', :err_msg => contents } ]
end
