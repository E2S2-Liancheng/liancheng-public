#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require 'set'


# This module holds the tuples of share_invite_status.
# It provides access to its values by unique methods.
#
module Sm
  class ShareInviteStatus
    statuses = self.all.each_with_object({}) { |t,h| h[t.name] = t.id }

    #-- Verify that each status that we know of is still there
    ['invited', 'blocked', 'accepted'].each do |s|
      if statuses[s].nil?
        msg = "In share_invite_status table, missing status: #{s}"
        $logger.fatal(msg)
        raise msg
      end
    end

    Invited = statuses['invited']
    Blocked = statuses['blocked']
    Accepted = statuses['accepted']
  end
end


module Shares

  # Checks whether a given member can access a given directory according to the share table.
  # Returns true if the member can access that directory, false otherwise.
  # Also returns false if there's no share for that directory.
  #
  def self.can_access?(directory_id, member_id)
    !Sm::ShareInvite.where(:directory_id => directory_id,
                           :member_id => member_id,
                           :share_invite_status_id => Sm::ShareInviteStatus::Accepted).empty?
  end


  # Checks whether a given member can read into a given directory according to the share table.
  # Returns true if the member can read in that directory, false otherwise.
  # Also returns false if there's no share for that directory.
  #
  def self.can_read?(directory_id, member_id)
    return false unless can_access?(directory_id, member_id)

    true
  end


  # Checks whether a given member can write into a given directory according to the share table.
  # Returns true if the member can write in that directory, false otherwise.
  # Also returns false if there's no share for that directory.
  #
  def self.can_write?(directory_id, member_id)
    return false unless can_access?(directory_id, member_id)

    !Sm::ShareWithMember.where(:directory_id => directory_id,
                               :member_id => member_id,
                               :read_write => true).empty? || \
    !Sm::ShareWithCrew.where(:directory_id => directory_id,
                             :read_write => true)
                      .join(:crew_member, :crew_id => :crew_id)
                      .where(:member_id => member_id).empty?
  end


  # Create the invitation if one does not already exists.
  #
  def self.invite(directory_id, member_id)
    if Sm::ShareInvite.where(:directory_id => directory_id, :member_id => member_id).count == 0
      Sm::ShareInvite.insert( :directory_id => directory_id,
                              :member_id => member_id,
                              :share_invite_status_id => 1,
                              :item_read => false )
    end
  end


  # Add invited members and crews to share a directory.
  # Throws in case of an error.
  #
  def self.add_invites(directory_id, member_ids, crew_ids, read_write)
    read_write = true           # WARNING remove to get back access control

    throw :msg, [ false, :err_share_nested_share ] unless Directories.can_be_shared?(directory_id)

    member_ids.each do |m|
      Sm::ShareWithMember.insert( :directory_id => directory_id,
                                  :member_id => m,
                                  :read_write => read_write )
    end

    invites = member_ids.to_set
    crew_ids.each do |c|
      Sm::ShareWithCrew.insert( :directory_id => directory_id,
                                :crew_id => c,
                                :read_write => read_write )
      invites += Sm::CrewMember.where(:crew_id => c).select(:member_id).all.map { |cm| cm.member_id }.to_set
    end

    invites.each { |m| invite(directory_id, m) }
  end


  # Returns the list of crews that both include the given member and for which the directory is shared.
  #
  def self.crews_for_member(directory_id, member_id)
    Sm::ShareWithCrew.where(:directory_id => directory_id)
                      .join(:crew_member, :crew_id => :crew_id)
                      .where(:member_id => member_id)
                      .all.map { |cm| cm.crew_id }
  end


  # Returns the list of the crew members and whether each of them will lose access to the directory.
  # The list is an Array of Arrays, each entry is composed of:
  # - the member's ID
  # - a boolean set to true if the member will keep access to the directory, set to false otherwise.
  #
  def self.members_for_crew(directory_id, crew_id)
    m_shares = Sm::ShareWithMember.where(:directory_id => directory_id)
                                  .select(:member_id).all
                                  .map { |swm| swm.member_id }.to_set
    c_shares = Sm::ShareWithCrew.where(:directory_id => directory_id).exclude(:crew_id => crew_id)
                                .select(:crew_id).all
                                .map { |swc| swc.crew_id }.to_set

    member_ids = Sm::CrewMember.where(:crew_id => crew_id).select(:member_id).all.map { |cm| cm.member_id }

    member_ids.map do |m|
      kept = m_shares.include?(m) ||
            Sm::CrewMember.where(:member_id => m).select(:crew_id).all.any? { |cm| c_shares.include?(cm.crew_id) }
      [m, kept]
    end
  end


  # Removes a member from a share.
  #
  def self.remove_member(directory_id, member_id, give_copy)
    Sm::ShareWithMember.where(:directory_id => directory_id, :member_id => member_id).delete
    nb_crews = Sm::ShareWithCrew.where(:directory_id => directory_id)
                                .join(:crew_member, :crew_id => :crew_id)
                                .where(:member_id => member_id).count
    if nb_crews == 0
      invite = Sm::ShareInvite[:directory_id => directory_id, :member_id => member_id]
      if !invite.nil?
        if invite.share_invite_status_id == Sm::ShareInviteStatus::Accepted
          inode_ids = Directories.get_all_inode_ids(directory_id)
          Sm::FileObfuscated.where(:member_id => member_id, :inode_id => inode_ids).delete
          Sm::FilePublished.where(:member_id => member_id, :inode_id => inode_ids).delete

          if give_copy
            Directories.copy( directory_id,
                              Sm::Member[member_id].home_directory_id,
                              Sm::Directory[directory_id].name,
                              Utils.get_utc_now )
          end
        end

        invite.delete
      end
    end
  end


  # Removes a crew from a share.
  #
  def self.remove_crew(directory_id, crew_id, give_copy)
    members = Sm::CrewMember.where(:crew_id => crew_id).select(:member_id).all.map { |cm| cm.member_id }

    Sm::ShareWithCrew.where(:directory_id => directory_id, :crew_id => crew_id).delete

    members.each do |member_id|
      cpt_shared = Sm::ShareWithCrew.where(:directory_id => directory_id)
                                    .join(:crew_member, :crew_id => :crew_id)
                                    .where(:member_id => member_id).count +
                    Sm::ShareWithMember.where(:directory_id => directory_id, :member_id => member_id).count
      if cpt_shared == 0
        invite = Sm::ShareInvite[:directory_id => directory_id, :member_id => member_id]
        if !invite.nil?
          if invite.share_invite_status_id == Sm::ShareInviteStatus::Accepted
            inode_ids = Directories.get_all_inode_ids(directory_id)
            Sm::FileObfuscated.where(:member_id => member_id, :inode_id => inode_ids).delete
            Sm::FilePublished.where(:member_id => member_id, :inode_id => inode_ids).delete

            if give_copy
              Directories.copy( directory_id,
                                Sm::Member[member_id].home_directory_id,
                                Sm::Directory[directory_id].name,
                                Utils.get_utc_now )
            end
          end

          invite.delete
        end
      end
    end
  end


  # Finds all the shares visible to one given member.
  # The non-accepted shares are discarded.
  # Returns an Array of share_ids.
  #
  def self.find_shares_visible_to(member_id)
    Sm::ShareInvite.where(:member_id => member_id,
                          :share_invite_status_id => Sm::ShareInviteStatus::Accepted)
                    .all.map { |si| si.directory_id }
  end

end


helpers do
  def check_share_sql(directory_id)
    raise ObjectNotFoundError.new('share', directory_id, session[:id]) unless Directories.is_shared?(directory_id)
  end
end


# GET /share/:directory_id
# Returns a share if there is one on the given directory.
#
get %r{^/share/(\d+)$} do
  directory_id = params[:captures][0].to_i

  share =
    DB.transaction do
      check_directory_sql(directory_id)
      acl = Security.get_access_rights(directory_id, session[:id])
      raise AccessRightsError.new('share', directory_id, session[:id]) if acl[0] != session[:id]
      raise AccessRightsError.new('share', directory_id, session[:id]) unless acl[1]
      check_share_sql(directory_id)

      rw = Sm::ShareWithMember.where(:directory_id => directory_id).get(:read_write) || \
          Sm::ShareWithCrew.where(:directory_id => directory_id).get(:read_write)

      { :directory_id => directory_id,
        :member_ids => Sm::ShareWithMember.where(:directory_id => directory_id).map(:member_id),
        :crew_ids => Sm::ShareWithCrew.where(:directory_id => directory_id).map(:crew_id),
        :rights => rw }
    end

  content_type :json
  JSON[ :status => 'ok', :share => share ]
end


# POST /share/:directory_id
# Creates or updates a share on the given directory.
#
post %r{^/share/(\d+)$} do
  directory_id = params[:captures][0].to_i
  raise BadRequestError.new('no one to share with') if params[:member_ids].nil? && params[:crew_ids].nil?

  ok, contents = catch(:msg) do
    DB.transaction do
      check_directory_sql(directory_id)
      acl = Security.get_access_rights(directory_id, session[:id])
      if acl[0] != session[:id]
        raise AccessRightsError.new('share', directory_id, session[:id]) unless acl[1]
        throw :msg, [ false, :err_share_nested_share ]
      end

      member = Sm::Member[session[:id]]
      raise BadRequestError.new("Member ##{session[:id]} can\'t share home directory ##{directory_id}") \
                                                                          if member.home_directory_id == directory_id
      raise BadRequestError.new("Member ##{session[:id]} can\'t share trash directory ##{directory_id}") \
                                                                          if member.trash_directory_id == directory_id
      raise BadRequestError.new("Member ##{session[:id]} can\'t share deleted directory ##{directory_id}") \
                                                                              if Directories.is_deleted?(directory_id)

      member_ids = params[:member_ids] ? param_to_array_ints(:member_ids) : []
      member_ids.each do |m|
        check_member_sql(m)
        raise BadRequestError.new("Invalid input: can\'t share with self (##{m})") if m == session[:id]
      end

      crew_ids = params['crew_ids'] ? param_to_array_ints('crew_ids') : []
      crew_ids.each do |c|
        crew = check_crew_sql(c)
        raise AccessRightsError.new('crew', c, session[:id]) if crew.creator_member_id != session[:id]
      end

      read_write = param_value(:read_write) == 'true'

      Shares.add_invites(directory_id, member_ids, crew_ids, read_write)
      [true, nil]
    end
  end

  content_type :json
  JSON[ ok ? { :status => 'ok' } : { :status => 'err', :err_msg => contents } ]
end


# GET /share/:directory_id/crews_for_member/:member_id
# Returns the list of crews that both include the given member and for which the directory is shared.
# Also returns the member's name.
#
get %r{/share/(\d+)/crews_for_member/(\d+)} do
  directory_id = params[:captures][0].to_i
  member_id = params[:captures][1].to_i

  crews, name =
    DB.transaction do
      check_share_sql(directory_id)
      acl = Security.get_access_rights(directory_id, session[:id])
      raise AccessRightsError.new('share', directory_id, session[:id]) if acl[0] != session[:id]
      member = check_member_sql(member_id)

      crew_ids = Shares.crews_for_member(directory_id, member_id)
      [ crew_ids.map { |c| Sm::Crew[c].get_info }, member.name ]
    end

  content_type :json
  JSON[ :status => 'ok', :crews => crews, :member_name => name ]
end


# GET /share/:directory_id/members_for_crew/:crew_id
# Returns the list of members in the given crew and whether they will lose access to the directory after removal.
# Also returns the crew's name.
#
get %r{/share/(\d+)/members_for_crew/(\d+)} do
  directory_id = params[:captures][0].to_i
  crew_id = params[:captures][1].to_i

  ok, contents =
    catch(:msg) do
      DB.transaction do
        check_share_sql(directory_id)
        acl = Security.get_access_rights(directory_id, session[:id])
        raise AccessRightsError.new('share', directory_id, session[:id]) if acl[0] != session[:id]

        crew = check_crew_sql(crew_id)
        throw :msg, [ false, :err_share_not_shared_with ] \
                                  if Sm::ShareWithCrew.where(:directory_id => directory_id, :crew_id => crew_id).empty?

        members = Shares.members_for_crew(directory_id, crew_id).map do |member_id, kept|
          { :id => member_id, :name => Sm::Member[member_id].name, :kept => kept }
        end

        [ true, { :members => members, :crew_name => crew.name } ]
      end
    end

  msg = ok ? contents.merge({ :status => 'ok' }) : { :status => 'err', :err_msg => contents }

  content_type :json
  JSON[msg]
end


# DELETE /share/:directory_id/member/:member_id
# Removes a given member from the share on a given directory.
#
delete %r{^/share/(\d+)/member/(\d+)$} do
  directory_id = params[:captures][0].to_i
  member_id = params[:captures][1].to_i
  give_copy = !params[:give_copy].nil?

  ok, contents =
    catch(:msg) do
      DB.transaction do
        check_share_sql(directory_id)
        acl = Security.get_access_rights(directory_id, session[:id])
        raise AccessRightsError.new('share', directory_id, session[:id]) if acl[0] != session[:id]

        member = check_member_sql(member_id)
        throw :msg, [ false, :err_share_not_shared_with ] \
                            if Sm::ShareWithMember.where(:directory_id => directory_id, :member_id => member_id).empty?

        Shares.remove_member(directory_id, member_id, give_copy)

        [ true, nil ]
      end
    end

  content_type :json
  JSON[ ok ? { :status => 'ok' } : { :status => 'err', :err_msg => contents } ]
end


# DELETE /share/:directory_id/crew/:crew_id
# Removes a given crew from the share on a given directory.
#
delete %r{^/share/(\d+)/crew/(\d+)$} do
  directory_id = params[:captures][0].to_i
  crew_id = params[:captures][1].to_i
  give_copy = !params[:give_copy].nil?

  ok, contents =
    catch(:msg) do
      DB.transaction do
        check_share_sql(directory_id)
        acl = Security.get_access_rights(directory_id, session[:id])
        raise AccessRightsError.new('share', directory_id, session[:id]) if acl[0] != session[:id]

        crew = check_crew_sql(crew_id)
        raise AccessRightsError.new('crew', crew_id, session[:id]) if crew.creator_member_id != session[:id]
        throw :msg, [ false, :err_share_not_shared_with ] \
                            if Sm::ShareWithCrew.where(:directory_id => directory_id, :crew_id => crew_id).empty?

        Shares.remove_crew(directory_id, crew_id, give_copy)

        [ true, nil ]
      end
    end

  content_type :json
  JSON[ ok ? { :status => 'ok' } : { :status => 'err', :err_msg => contents } ]
end


# GET /share/list_accessible
# Returns the list of shares available to the current member.
#
get '/share/list_accessible' do
  shares = DB.transaction { Shares.find_shares_visible_to(session[:id]) }

  content_type :json
  JSON[ :status => 'ok', :shares => shares ]
end


# POST /share/invitation
# Acts upon an action on an invitation.
#
post '/share/invitation' do
  directory_id = param_to_int(:directory_id)
  action = param_value(:action)
  raise BadRequestError.new("Wrong action #{action}") unless %w{accept ignore block}.include?(action)

  ok, contents =
    catch(:msg) do
      DB.transaction do
        check_share_sql(directory_id)

        invite = Sm::ShareInvite.where(:directory_id => directory_id,
                                      :member_id => session[:id]).first
        raise AccessRightsError.new('invite on directory', directory_id, session[:id]) if invite.nil?

        if invite.share_invite_status_id != Sm::ShareInviteStatus::Invited
          msg = "Wrong action %s for invite on directory #%d (status %d) from member #%d" %
                [ action, directory_id, invite.share_invite_status_id, session[:id] ]
          throw :msg, [ false, :err_invite_already_used ]
        end

        case action
          when 'accept' then invite.update(:share_invite_status_id => Sm::ShareInviteStatus::Accepted)
          when 'block'  then invite.update(:share_invite_status_id => Sm::ShareInviteStatus::Blocked)
          when 'ignore' then invite.delete
        end

        [ true, nil ]
      end
    end

  content_type :json
  JSON[ ok ? { :status => 'ok' } : { :status => 'err', :err_msg => contents } ]
end
