#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This module holds the tuples of account_status.
# It provides access to its values by unique methods.
#
module Sm
  class AccountStatus
    statuses = self.all.each_with_object({}) { |t,h| h[t.name] = t.id }

    #-- Verify that each status that we know of is still there
    ['validation pending', 'active'].each do |s|
      if statuses[s].nil?
        msg = "In account_status table, missing status: #{s}"
        $logger.fatal(msg)
        raise msg
      end
    end

    ValidationPending = statuses['validation pending']
    Active = statuses['active']
  end
end


module Members

  # Registers a new member in the system.
  # This account will need a validation from an administrator before it can be used.
  # Returns true and the account's id in case of success, false and an error message otherwise.
  #
  #-- WARNING password must not be logged
  #
  def self.add(name, email, password)
    return [false, :err_member_name_exists] unless Sm::Member.where(:name => name).empty?
    return [false, :err_member_email_exists] unless Sm::Member.where(:email => email).empty?

    member_id = DB.do_not_log do
      Sm::Member.insert(:name => name,
                        :email => email,
                        :password => Sequel.function(:crypt, password, Sequel.function(:gen_salt, 'bf', 14)),
                        :account_status_id => Sm::AccountStatus::ValidationPending)
    end

    [true, member_id]
  end


  # Validates the account.
  #
  def self.validate(member_id, home_directory_id, trash_id)
    Sm::Member[member_id].update( :home_directory_id => home_directory_id,
                                  :trash_directory_id => trash_id,
                                  :account_status_id => Sm::AccountStatus::Active )
  end


  # Rejects (and deletes) the account.
  #
  def self.reject(member_id)
    Sm::Member[member_id].delete
  end


  # Check the user's credentials to decide whether to grant or deny access to the system.
  # If the credentials are correct and the account is:
  # - active, returns an array containing the id and name of the user.
  # - not active, returns an array containing nil and the name of the user.
  # Otherwise returns nil.
  #
  #-- WARNING password must not be logged
  #
  def self.check_credentials(email, password)
    member = DB.do_not_log do
      Sm::Member.where(:email => email, :password => Sequel.function(:crypt, password, :password))
                .select(:id, :name, :account_status_id).first
    end

    return nil if member.nil?

    [ (member.account_status_id == Sm::AccountStatus::Active) ? member.id : nil, member.name ]
  end


  # Update the user's name in the database.
  # Returns nil in case of success, an error message otherwise.
  #
  def self.update_name(id, name)
    member = Sm::Member[id]
    return nil if member.name == name
    return :err_member_name_exists unless Sm::Member.where(:name => name).empty?

    member.update(:name => name)
    nil
  end


  # Update the user's password in the database.
  # Returns nil in case of success, an error message otherwise.
  #
  #-- WARNING password must not be logged
  #
  def self.update_password(id, old_password, new_password)
    DB.do_not_log do
      member = Sm::Member.where(:id => id, :password => Sequel.function(:crypt, old_password, :password))
      return :err_member_inc_old_pass if member.empty?

      member.update(:password => Sequel.function(:crypt, new_password, Sequel.function(:gen_salt, 'bf', 14)))
      nil
    end
  end


  # Updates the user's username in the database.
  # Returns nil in case of success, an error message otherwise.
  #
  def self.update_username(id, username)
    member = Sm::Member[id]
    return nil if member.username == username
    return :err_member_username_exists unless Sm::Member.where(:username => username).empty?

    member.update(:username => username)
    nil
  end
end


helpers do
  def check_member_sql(member_id)
    Sm::Member[member_id].tap { |m| raise ObjectNotFoundError.new('member', member_id, session[:id]) if m.nil? }
  end
end


# GET /member
# Returns the current member's detailed account.
#
get '/member' do
  member = DB.transaction do
    Sm::Member.where(:id => session[:id])
              .select(:id, :email, :name, :home_directory_id, :username)
              .first.to_hash
  end

  content_type :json
  JSON[ :status => 'ok', :member => member ]
end


# GET /member/list
# Returns the list of active members' summarised accounts, excluding the current member.
#
get '/member/list' do
  members = DB.transaction do
    Sm::Member.where(:account_status_id => Sm::AccountStatus::Active).exclude(:id => session[:id])
              .select(:id, :name)
              .all.map(&:to_hash)
  end

  content_type :json
  JSON[ :status => 'ok', :members => members ]
end


# GET /member/crews
# Returns the list of crews defined by the current user.
#
get '/member/crews' do
  crews = DB.transaction { Sm::Crew.where(:creator_member_id => session[:id]).get_info.map(&:to_hash) }

  content_type :json
  JSON[ :status => 'ok', :crews => crews ]
end


# GET /member/:member_id
# Returns a given member's summarised account.
#
get %r{^/member/(\d+)$} do
  member_id = params[:captures][0].to_i

  member = DB.transaction { check_member_sql(member_id).get_info }

  content_type :json
  JSON[ :status => 'ok', :member => member ]
end


# PUT /member/name
# Changes the name of the current member. The name must contain at least 1 character.
#
put '/member/name' do
  name = param_non_empty(:name)

  result = DB.transaction { Members.update_name(session[:id], name) }

  content_type :json
  JSON[ (result.nil?) ? { :status => 'ok', :name => name } : { :status => 'err', :err_msg => result } ]
end


# PUT /member/password
# Changes the password of the current member.
#
put '/member/password' do
  old_password = param_value('old_password')
  new_password = param_value('new_password')

  result = DB.transaction { Members.update_password(session[:id], old_password, new_password) }

  content_type :json
  JSON[ (result.nil?) ? { :status => 'ok' } : { :status => 'err', :err_msg => result } ]
end


# GET /member/username/check
# Checks whether the username is available.
#
get '/member/username/check' do
  username = param_non_empty(:username)

  raise BadRequestError.new('Invalid username') if username !~ /^[-.\w]{2,32}$/

  available = DB.transaction { Sm::Member.exclude(:id => session[:id]).where(:username => username).empty? }

  content_type :json
  JSON[ :status => 'ok', :username => username, :available => available ]
end


# PUT /member/username
# Changes the username of the current member.
#
put '/member/username' do
  username = param_non_empty(:username)

  raise BadRequestError.new('Invalid username') if username !~ /^[-.\w]{2,32}$/

  result = DB.transaction { Members.update_username(session[:id], username) }

  content_type :json
  JSON[ (result.nil?) ? { :status => 'ok', :username => username } : { :status => 'err', :err_msg => result } ]
end
