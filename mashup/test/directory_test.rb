#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require_relative 'test_helper.rb'

class DirectoryTest < ApiTest
  def test_directory_successful
    json = get_json_id '/directory/10003', nil, 1000

    assert_equal json, { :status => 'ok', :directory => { :id => 10003,
                                                          :name => 'assez',
                                                          :modified_at => '2015-01-01 00:00:00 +0800',
                                                          :parent_id => 10001,
                                                          :children_ids => [10005, 10006],
                                                          :shared => false,
                                                          :owner_name => 'thomas',
                                                          :can_be_shared => true } }
  end

  def test_shared_directory_successful
    json = get_json_id '/directory/10505', nil, 1000

    assert_equal json, { :status => 'ok', :directory => { :id => 10505,
                                                          :name => 'chère',
                                                          :modified_at => '2015-01-01 00:00:00 +0800',
                                                          :parent_id => nil,
                                                          :children_ids => [10506],
                                                          :shared => true,
                                                          :owner_name => 'セルティ',
                                                          :can_be_shared => true } }
  end

  def test_directory_non_existing
    get_id '/directory/666', nil, 1000

    assert_equal last_response.status, 404
  end

  def test_directory_unauthorised
    get_id '/directory/10501', nil, 1000

    assert_equal last_response.status, 403
  end

  def test_home
    json = get_json_id '/directory/home', nil, 1000

    assert_equal json, { :status => 'ok', :directory => { :id => 10001,
                                                          :name => 'home',
                                                          :modified_at => '2015-01-01 00:00:00 +0800',
                                                          :parent_id => nil,
                                                          :children_ids => [10003, 10004],
                                                          :shared => false,
                                                          :owner_name => 'thomas',
                                                          :can_be_shared => true } }
  end

  def test_directory_inodes_successful
    json = get_json_id '/directory/10003/inodes', nil, 1000

    assert_equal json, { :status => 'ok',
                         :inodes => [ { :id => 10001,
                                        :name => 'pre.txt',
                                        :directory_id => 10003,
                                        :revision => { :revision => 2,
                                                       :size => 8,
                                                       :modified_by => 1000,
                                                       :modified_at => 1420128000,
                                                       :mime_type => 'text/plain',
                                                       :owner_name => 'thomas' } },
                                      { :id => 10002,
                                        :name => 'ter.rar',
                                        :directory_id => 10003,
                                        :revision => { :revision => 1,
                                                       :size => 8,
                                                       :modified_by => 1000,
                                                       :modified_at => 1420041600,
                                                       :mime_type => 'text/plain',
                                                       :owner_name => 'thomas' } } ] }
  end

  def test_directory_inodes_empty_successful
    json = get_json_id 'directory/10004/inodes', nil, 1000

    assert_equal json, { :status => 'ok', :inodes => [] }
  end

  def test_directory_inodes_non_existing
    get_id '/directory/666/inodes', nil, 1000

    assert_equal last_response.status, 404
  end

  def test_directory_inodes_unauthorised
    get_id '/directory/10501/inodes', nil, 1000

    assert_equal last_response.status, 403
  end

  def test_directory_root_for_member_owner
    json = get_json_id '/directory/10506/root_for_member', nil, 1050

    assert_equal json, { :status => 'ok', :root_id => 10501 }
  end

  def test_directory_root_for_member_shared
    json = get_json_id '/directory/10506/root_for_member', nil, 1000

    assert_equal json, { :status => 'ok', :root_id => 10505 }
  end

  def test_directory_root_for_member_non_existing
    get_id '/directory/666/root_for_member', nil, 1000

    assert_equal last_response.status, 404
  end

  def test_directory_root_for_member_blocking
    get_id '/directory/10506/root_for_member', nil, 1040

    assert_equal last_response.status, 403
  end

  def test_directory_root_for_member_unauthorised
    get_id '/directory/10506/root_for_member', nil, 1060

    assert_equal last_response.status, 403
  end

  # TODO GET /directory/:directory_id/read_write ?

  def test_directory_create_successful
    json = post_json_id '/directory', { :parent_id => 10001, :name => 'daniela' }, 1000

    new_dir = DB[:directory].where(:parent_id => 10001, :name => 'daniela')
    refute_empty new_dir
    new_dir_id = new_dir.first[:id]
    new_dir_ts = new_dir.first[:modified_at]

    assert_equal json, { :status => 'ok',
                         :directory => { :id => new_dir_id,
                                         :name => 'daniela',
                                         :modified_at => new_dir_ts.to_s,
                                         :parent_id => 10001,
                                         :children_ids => [],
                                         :shared => false,
                                         :owner_name => 'thomas',
                                         :can_be_shared => true } }
  end

  def test_directory_create_missing_name
    post_id '/directory', { :parent_id => 10001 }, 1000

    assert_equal last_response.status, 400
  end

  def test_directory_create_missing_parent_id
    post_id '/directory', { :name => 'daniela' }, 1000

    assert_equal last_response.status, 400
  end

  def test_directory_create_empty_name
    post_id '/directory', { :parent_id => 10001, :name => '' }, 1000

    assert_equal last_response.status, 400
  end

  def test_directory_create_unauthorised
    post_id '/directory', { :parent_id => 10501, :name => 'daniela' }, 1000

    assert_equal last_response.status, 403
  end

  def test_directory_create_already_used_name           # directory
    json = post_json_id '/directory', { :parent_id => 10003, :name => 'thunder' }, 1000

    assert_equal json, { :status => 'err', :err_msg => 'err_dir_ren_already' }
  end

  def test_directory_create_already_used_name           # inode
    json = post_json_id '/directory', { :parent_id => 10003, :name => 'pre.txt' }, 1000

    assert_equal json, { :status => 'err', :err_msg => 'err_dir_ren_already' }
  end

  def test_directory_rename_successful
    json = put_json_id '/directory/10003/name', { :name => 'caroline' }, 1000

    ren_dir = DB[:directory].where(:id => 10003, :name => 'caroline')
    refute_empty ren_dir
    ren_dir_ts = ren_dir.first[:modified_at]

    assert_equal json, { :status => 'ok',
                         :directory => { :id => 10003,
                                         :name => 'caroline',
                                         :modified_at => ren_dir_ts.to_s,
                                         :parent_id => 10001,
                                         :children_ids => [10005, 10006],
                                         :shared => false,
                                         :owner_name => 'thomas',
                                         :can_be_shared => true } }
  end

  def test_directory_rename_missing_name
    put_id '/directory/10003/name', nil, 1000

    assert_equal last_response.status, 400
  end

  def test_directory_rename_empty_name
    put_id '/directory/10003/name', { :name => '' }, 1000

    assert_equal last_response.status, 400
  end

  def test_directory_rename_non_existing
    put_id '/directory/666/name', { :name => 'caroline' }, 1000

    assert_equal last_response.status, 404
  end

  def test_directory_rename_unauthorised
    put_id '/directory/10503/name', { :name => 'caroline' }, 1000

    assert_equal last_response.status, 403
  end

  def test_directory_rename_root
    json = put_json_id '/directory/10001/name', { :name => 'caroline' }, 1000

    assert_equal json, { :status => 'err', :err_msg => 'err_dir_ren_root' }
  end

  def test_directory_rename_shared
    json = put_json_id '/directory/10505/name', { :name => 'caroline' }, 1000

    assert_equal json, { :status => 'err', :err_msg => 'err_dir_ren_share' }
  end

  def test_directory_rename_already_used_name_directory
    json = put_json_id '/directory/10006/name', { :name => 'thunder' }, 1000

    assert_equal json, { :status => 'err', :err_msg => 'err_dir_ren_already' }
  end

  def test_directory_rename_already_used_name_inode
    json = put_json_id '/directory/10006/name', { :name => 'pre.txt' }, 1000

    assert_equal json, { :status => 'err', :err_msg => 'err_dir_ren_already' }
  end
end
