#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require_relative 'test_helper.rb'
require 'digest'

class RegistrationTest < ApiTest
  def test_missing_name
    post '/register', { :email => 'thomas@emi',
                        :password => Digest::SHA256.hexdigest('password') }

    assert_equal last_response.status, 400
  end

  def test_missing_email
    post '/register', { :name => 'thomas',
                        :password => Digest::SHA256.hexdigest('password') }

    assert_equal last_response.status, 400
  end

  def test_missing_password
    post '/register', { :name => 'thomas',
                        :email => 'thomas@emi' }

    assert_equal last_response.status, 400
  end

  def test_name_already_in_use
    json = post_json '/register', { :name => 'thomas',
                                    :email => 'thomas@emi',
                                    :password => Digest::SHA256.hexdigest('password') }

    assert_equal json, { :status => 'err',
                         :err_msg => 'err_member_name_exists' }
  end

  def test_email_already_in_use
    json = post_json '/register', { :name => 'thomas2',
                                    :email => 'thomas@emi',
                                    :password => Digest::SHA256.hexdigest('password') }

    assert_equal json, { :status => 'err',
                         :err_msg => 'err_member_email_exists' }
  end

  def test_successful_registration
    json = post_json '/register', { :name => 'thomas2',
                                    :email => 'thomas@emi2',
                                    :password => Digest::SHA256.hexdigest('password') }

    new_member = DB[:member].where(:name => 'thomas2',
                                   :email => 'thomas@emi2',
                                   :password => Sequel.function(:crypt,
                                                                Digest::SHA256.hexdigest('password'),
                                                                :password))
    refute_empty new_member
    new_member_id = new_member.first[:id]
    assert_equal json, { :status => 'ok',
                         :pending_id => new_member_id }
  end

  def test_account_listing
    json = get_json '/register/pending',
                    nil,
                    { 'rack.session' => { :admin => 'so you think you can stone me' } }

    assert_equal json, { :status => 'ok',
                         :pendings => [{ :id => 1010,
                                         :name => 'urbain',
                                         :email => 'urbain@db' }] }
  end

  def test_validate_correct_account
    json = post_json '/register/validate',
                     { :pending_id => 1010 },
                     { 'rack.session' => { :admin => 'and spit in my eye' } }

    assert_equal json, { :status => 'ok' }
  end

  def test_validate_non_existing_account
    post '/register/validate',
         { :pending_id => 666 },
         { 'rack.session' => { :admin => 'so you think you can love me' } }

    assert_equal last_response.status, 404
  end

  def test_validate_non_pending_account
    post '/register/validate',
         { :pending_id => 1000 },
         { 'rack.session' => { :admin => 'and leave me to die' } }

    assert_equal last_response.status, 400
  end

  def test_reject_correct_account
    json = post_json '/register/reject',
                     { :pending_id => 1010 },
                     { 'rack.session' => { :admin => 'oh baby, can''t do this to me baby' } }

    assert_equal json, { :status => 'ok' }
  end

  def test_reject_non_existing_account
    post '/register/reject',
         { :pending_id => 666 },
         { 'rack.session' => { :admin => 'just gotta get out' } }

    assert_equal last_response.status, 404
  end

  def test_reject_non_pending_account
    post '/register/reject',
         { :pending_id => 1000 },
         { 'rack.session' => { :admin => 'just gotta get right out of here' } }

    assert_equal last_response.status, 400
  end
end
