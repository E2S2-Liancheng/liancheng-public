#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require_relative 'test_helper.rb'
require 'digest'

class InodeTest < ApiTest
  def test_inode_successful
    json = get_json_id '/inode/10001', nil, 1000

    assert_equal json, { :status => 'ok',
                         :inode => { :id => 10001,
                                     :name => 'pre.txt',
                                     :directory_id => 10003,
                                     :revision => { :revision => 2,
                                                    :size => 8,
                                                    :modified_by => 1000,
                                                    :modified_at => 1420128000,
                                                    :mime_type => 'text/plain' } } }
  end

  def test_inode_non_existing
    get_id '/inode/666', nil, 1000

    assert_equal last_response.status, 404
  end

  def test_inode_unauthorised
    get_id '/inode/10001', nil, 1020

    assert_equal last_response.status, 403
  end

  def test_inode_get_file
    f = get_id '/inode/10001/1/contents', nil, 1000

    assert_equal last_response.status, 200
    fields = [ 'Content-Type', 'Content-Disposition', 'Content-Length' ]
    assert_equal f.header.slice(*fields), { 'Content-Type' => 'text/plain;charset=utf-8',
                                            'Content-Disposition' => 'attachment; filename="pre.txt"',
                                            'Content-Length' => '8' }
    assert_equal f.body, 'file 900'
  end

  def test_inode_get_file_latest_revision
    f = get_id '/inode/10001/-1/contents', nil, 1000

    assert_equal last_response.status, 200
    fields = [ 'Content-Type', 'Content-Disposition', 'Content-Length' ]
    assert_equal f.header.slice(*fields), { 'Content-Type' => 'text/plain;charset=utf-8',
                                            'Content-Disposition' => 'attachment; filename="pre.txt"',
                                            'Content-Length' => '8' }
    assert_equal f.body, 'file 902'
  end

  def test_inode_get_file_non_existing
    get_id '/inode/666/1/contents', nil, 1000

    assert_equal last_response.status, 404
  end

  def test_inode_get_file_non_existing_revision
    get_id '/inode/10001/666/contents', nil, 1000

    assert_equal last_response.status, 404
  end

  def test_inode_get_file_unauthorised
    get_id '/inode/10001/-1/contents', nil, 1020

    assert_equal last_response.status, 403
  end

  def test_inode_post_file
    file_contents = 'Detroit Mock City'
    file_name = 'dmc.txt'
    mime_type = 'text/plain'
    file = Tempfile.new('inode_test')
    file.write(file_contents)
    file.rewind

    up = Rack::Test::UploadedFile.new(file.path)

    json = post_json_id '/inode', { :directory_id => 10001,
                                    :'filename' => [file_name],
                                    :'upload' => [up],
                                    :'mime_type' => [mime_type] }, 1000

    qr = <<-EOF
      SELECT i.id as id, modified_at, sha1sum
      FROM inode i, inode_revision ir, storage_file sf
      WHERE i.id = ir.inode_id
        AND ir.storage_file_id = sf.id
        AND name = ?
        AND directory_id = 10001
    EOF
    nf = DB[qr, file_name].first

    assert_equal nf[:sha1sum], Digest::SHA1.hexdigest(file_contents)

    assert_equal json, { :status => 'ok',
                         :inodes => [ { :id => nf[:id],
                                        :name => file_name,
                                        :directory_id => 10001,
                                        :revision => { :revision => 1,
                                                       :size => file_contents.size,
                                                       :modified_by => 1000,
                                                       :modified_at => nf[:modified_at].strftime('%s').to_i,
                                                       :mime_type => mime_type } } ] }
  end

  def test_inode_post_file_multiple
    files = [ { :contents => 'Prosper', :name => 'prosper.txt', :type => 'text/plain' },
              { :contents => 'Youpla',  :name => 'youpla.txt',  :type => 'text/plain' },
              { :contents => 'Boom',    :name => 'boom.txt',    :type => 'text/plain' } ]

    ups = files.map do |f|
      t = Tempfile.new('inode_test')
      t.write(f[:contents])
      t.rewind
      Rack::Test::UploadedFile.new(t.path)
    end

    json = post_json_id '/inode', { :directory_id => 10005,
                                    :'filename' => files.map { |f| f[:name] },
                                    :'upload' => ups,
                                    :'mime_type' => files.map { |f| f[:type] } }, 1000

    qr = <<-EOF
      SELECT i.id as id, name, modified_at, mime_type_id, sha1sum
      FROM inode i, inode_revision ir, storage_file sf
      WHERE i.id = ir.inode_id
        AND ir.storage_file_id = sf.id
        AND directory_id = 10005
    EOF
    nf = DB[qr].all

    assert_equal nf.map { |f| f[:name] }, files.map { |f| f[:name] }
    nf.each do |f|
      f[:contents] = files.find { |f2| f2[:name] == f[:name] }[:contents]
      f[:mime_type] = DB[:mime_type].where(:id => f[:mime_type_id]).map(:name).first
      assert_equal f[:sha1sum], Digest::SHA1.hexdigest(f[:contents])
    end

    assert_equal json, { :status => 'ok',
                         :inodes => [ { :id => nf[0][:id],
                                        :name => nf[0][:name],
                                        :directory_id => 10005,
                                        :revision => { :revision => 1,
                                                       :size => nf[0][:contents].size,
                                                       :modified_by => 1000,
                                                       :modified_at => nf[0][:modified_at].strftime('%s').to_i,
                                                       :mime_type => nf[0][:mime_type] } },
                                      { :id => nf[1][:id],
                                        :name => nf[1][:name],
                                        :directory_id => 10005,
                                        :revision => { :revision => 1,
                                                       :size => nf[1][:contents].size,
                                                       :modified_by => 1000,
                                                       :modified_at => nf[1][:modified_at].strftime('%s').to_i,
                                                       :mime_type => nf[1][:mime_type] } },
                                      { :id => nf[2][:id],
                                        :name => nf[2][:name],
                                        :directory_id => 10005,
                                        :revision => { :revision => 1,
                                                       :size => nf[2][:contents].size,
                                                       :modified_by => 1000,
                                                       :modified_at => nf[2][:modified_at].strftime('%s').to_i,
                                                       :mime_type => nf[2][:mime_type] } } ] }
  end

  def test_inode_post_file_workflow
    file_contents = '{"meta":{"app":"UrbanDB"}}'
    file_name = 'dmc.txt'
    mime_type = 'text/plain'
    file = Tempfile.new('inode_test')
    file.write(file_contents)
    file.rewind

    up = Rack::Test::UploadedFile.new(file.path)

    json = post_json_id '/inode', { :directory_id => 10001,
                                    :'filename' => [file_name],
                                    :'upload' => [up],
                                    :'mime_type' => [mime_type] }, 1000

    qr = <<-EOF
      SELECT i.id, modified_at, sha1sum, mt.name
      FROM inode i, inode_revision ir, storage_file sf, mime_type mt
      WHERE i.id = ir.inode_id
        AND ir.storage_file_id = sf.id
        AND ir.mime_type_id = mt.id
        AND i.name = ?
        AND directory_id = 10001
    EOF
    nf = DB[qr, file_name].first

    assert_equal nf[:sha1sum], Digest::SHA1.hexdigest(file_contents)
    assert_equal nf[:name], 'application/x-urbandb-workflow'

    assert_equal json, { :status => 'ok',
                         :inodes => [ { :id => nf[:id],
                                        :name => file_name,
                                        :directory_id => 10001,
                                        :revision => { :revision => 1,
                                                       :size => file_contents.size,
                                                       :modified_by => 1000,
                                                       :modified_at => nf[:modified_at].strftime('%s').to_i,
                                                       :mime_type => nf[:name] } } ] }
  end

  def test_inode_post_file_missing_directory
    file_name = 'dmc.txt'
    mime_type = 'text/plain'
    file = Tempfile.new('inode_test')

    up = Rack::Test::UploadedFile.new(file.path)

    json = post_id '/inode', { :'filename' => [file_name],
                               :'upload' => [up],
                               :'mime_type' => [mime_type] }, 1000

    assert_equal last_response.status, 400
  end

  def test_inode_post_file_missing_file_name
    file_name = 'dmc.txt'
    mime_type = 'text/plain'
    file = Tempfile.new('inode_test')

    up = Rack::Test::UploadedFile.new(file.path)

    json = post_id '/inode', { :directory_id => 10501,
                               :'upload' => [up],
                               :'mime_type' => [mime_type] }, 1000

    assert_equal last_response.status, 400
  end

  def test_inode_post_file_missing_file
    file_name = 'dmc.txt'
    mime_type = 'text/plain'
    file = Tempfile.new('inode_test')

    up = Rack::Test::UploadedFile.new(file.path)

    json = post_id '/inode', { :directory_id => 10501,
                               :'filename' => [file_name],
                               :'mime_type' => [mime_type] }, 1000

    assert_equal last_response.status, 400
  end

  def test_inode_post_file_missing_mime_type
    file_name = 'dmc.txt'
    mime_type = 'text/plain'
    file = Tempfile.new('inode_test')

    up = Rack::Test::UploadedFile.new(file.path)

    json = post_id '/inode', { :directory_id => 10501,
                               :'filename' => [file_name],
                               :'upload' => [up] }, 1000

    assert_equal last_response.status, 400
  end

  def test_inode_post_file_unauthorised
    file_name = 'dmc.txt'
    mime_type = 'text/plain'
    file = Tempfile.new('inode_test')

    up = Rack::Test::UploadedFile.new(file.path)

    json = post_id '/inode', { :directory_id => 10501,
                               :'filename' => [file_name],
                               :'upload' => [up],
                               :'mime_type' => [mime_type] }, 1000

    assert_equal last_response.status, 403
  end

  def test_inode_post_file_name_already_used
    file_name = 'assez'
    mime_type = 'text/plain'
    file = Tempfile.new('inode_test')

    up = Rack::Test::UploadedFile.new(file.path)

    json = post_json_id '/inode', { :directory_id => 10001,
                                    :'filename' => [file_name],
                                    :'upload' => [up],
                                    :'mime_type' => [mime_type] }, 1000

    assert_equal json, { :status => 'incomplete', :inodes => [] }
  end

  def test_inode_rename
    json = put_json_id '/inode/10001/name', { :name => 'rari.sim' }, 1000

    assert_equal json, { :status => 'ok',
                         :inode => { :id => 10001,
                                     :name => 'rari.sim',
                                     :directory_id => 10003,
                                     :revision => { :revision => 2,
                                                    :size => 8,
                                                    :modified_by => 1000,
                                                    :modified_at => 1420128000,
                                                    :mime_type => 'text/plain' } } }
  end

  def test_inode_rename_empty
    put_id '/inode/10001/name', { :name => '' }, 1000

    assert_equal last_response.status, 400
  end

  def test_inode_rename_non_existing
    put_id '/inode/666/name', { :name => 'rari.sim' }, 1000

    assert_equal last_response.status, 404
  end

  def test_inode_rename_unauthorised
    put_id '/inode/10001/name', { :name => 'rari.sim' }, 1050

    assert_equal last_response.status, 403
  end

  def test_inode_rename_already_existing_folder
    json = put_json_id '/inode/10001/name', { :name => 'thunder' }, 1000

    assert_equal json, { :status => 'err', :err_msg => 'err_inode_ren_already' }
  end

  def test_inode_rename_already_existing_file
    json = put_json_id '/inode/10001/name', { :name => 'ter.rar' }, 1000

    assert_equal json, { :status => 'err', :err_msg => 'err_inode_ren_already' }
  end
end
