#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
require_relative 'test_helper.rb'

class ShareTest < ApiTest
  def test_get_publish
    json = get_json_id '/publish/10501', nil, 1050

    assert_equal json, { :status => 'ok',
                         :publish => { :inode => { :id => 10501,
                                                   :name => 'whatsup.doc',
                                                   :directory_id => 10505,
                                                   :revision => { :revision => 1,
                                                                  :size => 8,
                                                                  :modified_by => 1050,
                                                                  :modified_at => 1420041600,
                                                                  :mime_type => 'text/plain' } },
                                       :obfuscated => [ { :id => '68f5508b-bc73-4722-bddc-dd907f152f04',
                                                          :inode_id => 10501,
                                                          :annotation => 'not nil' },
                                                        { :id => '6b9bc6ce-7a18-4397-bf09-3574f8d6ad01',
                                                          :inode_id => 10501,
                                                          :annotation => nil } ],
                                       :username => 'drrr',
                                       :published => true } }
  end

  def test_get_publish_unauthorised
    get_id '/publish/10001', nil, 1050

    assert_equal last_response.status, 403
  end

  def test_get_publish_non_existing
    get_id '/publish/666', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_get_publish_obfuscated_name
    json = get_json '/publish/obfuscated/68f5508b-bc73-4722-bddc-dd907f152f04/name', nil

    assert_equal json, { :status => 'ok', :name => 'whatsup.doc' }
  end

  def test_get_publish_obfuscated_name_non_existing
    get '/publish/obfuscated/00000000-0000-0000-0000-000000000000/name', nil

    assert_equal last_response.status, 404
  end

  def test_get_publish_obfuscated
    f = get '/publish/obfuscated/68f5508b-bc73-4722-bddc-dd907f152f04', nil

    assert_equal last_response.status, 200
    fields = [ 'Content-Type', 'Content-Disposition', 'Content-Length' ]
    assert_equal f.header.slice(*fields), { 'Content-Type' => 'text/plain;charset=utf-8',
                                            'Content-Disposition' => 'attachment; filename="whatsup.doc"',
                                            'Content-Length' => '8' }
    assert_equal f.body, 'file 903'
  end

  def test_get_publish_obfuscated_non_existing
    get '/publish/obfuscated/00000000-0000-0000-0000-000000000000', nil

    assert_equal last_response.status, 404
  end

  def test_post_publish_obfuscated
    json = post_json_id '/publish/10502/obfuscated', { :annotation => 'cathodation' }, 1070

    obf = DB[:file_obfuscated].where(:member_id => 1070, :inode_id => 10502).exclude(:annotation => nil)
    assert_equal obf.count, 1
    uuid = obf.get(:id)

    assert_equal json, { :status => 'ok', :obfuscated => { :id => uuid, :annotation => 'cathodation' } }
  end

  def test_post_publish_obfuscated_non_existing
    post_id '/publish/666/obfuscated', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_post_publish_obfuscated_unauthorised
    post_id '/publish/10501/obfuscated', nil, 1060

    assert_equal last_response.status, 403
  end

  def test_delete_publish_obfuscated
    json = delete_json_id '/publish/obfuscated/68f5508b-bc73-4722-bddc-dd907f152f04', nil, 1050

    assert_empty DB[:file_obfuscated].where(:id => '68f5508b-bc73-4722-bddc-dd907f152f04')

    assert_equal json, { :status => 'ok' }
  end

  def test_delete_publish_obfuscated_non_existing
    delete_id '/publish/obfuscated/00000000-0000-0000-0000-000000000000', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_delete_publish_obfuscated_unauthorised
    delete_id '/publish/obfuscated/68f5508b-bc73-4722-bddc-dd907f152f04', nil, 1070

    assert_equal last_response.status, 403
  end

  def test_put_publish_home
    json = put_json_id '/publish/home/10502', nil, 1050

    assert_equal json, { :status => 'ok' }
  end

  def test_put_publish_home_non_existing
    put_id '/publish/home/666', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_put_publish_home_unauthorised
    put_id '/publish/home/10502', nil, 1060

    assert_equal last_response.status, 403
  end

  def test_put_publish_home_already_published
    put_id '/publish/home/10501', nil, 1050

    assert_equal last_response.status, 400
  end

  def test_put_publish_home_no_username
    put_id '/publish/home/10501', nil, 1020

    assert_equal last_response.status, 400
  end

  def test_delete_publish_home
    json = delete_json_id '/publish/home/10501', nil, 1050

    assert_equal json, { :status => 'ok' }
  end

  def test_delete_publish_home_non_existing
    delete_id '/publish/home/666', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_delete_publish_home_unauthorised
    delete_id '/publish/home/10501', nil, 1060

    assert_equal last_response.status, 403
  end

  def test_delete_publish_home_not_published
    delete_id '/publish/home/10502', nil, 1050

    assert_equal last_response.status, 400
  end

  def test_delete_publish_home_no_username
    delete_id '/publish/home/10501', nil, 1020

    assert_equal last_response.status, 400
  end

  def test_delete_parent_directory
    json = post_json_id '/ops/delete', { :dirs => [10603] }, 1060

    assert_empty DB[:file_obfuscated].where(:member_id => 1060)
    assert_empty DB[:file_published].where(:member_id => 1060)

    # :deleted has an expected order because there's only one file and one directory in each directory
    assert_equal json, { :status => "ok",
                         :deleted => [ { :dir_id => 10605, :inode_id => 10602 },
                                       { :dir_id => 10605 },
                                       { :dir_id => 10604, :inode_id => 10601 },
                                       { :dir_id => 10604 },
                                       { :dir_id => 10603 } ],
                         :errors => [] }
  end

  def test_unshare_directory_member
    json = delete_json_id '/share/10505/member/1070', nil, 1050

    assert_empty DB[:file_obfuscated].where(:member_id => 1070)
    assert_empty DB[:file_published].where(:member_id => 1070)

    assert_equal json, { :status => 'ok' }
  end

  def test_unshare_directory_crew
    json = delete_json_id '/share/10505/crew/10503', nil, 1050

    assert_empty DB[:file_obfuscated].where(:member_id => 1020)
    assert_empty DB[:file_published].where(:member_id => 1020)

    assert_equal json, { :status => 'ok' }
  end
end
