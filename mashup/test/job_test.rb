#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require_relative 'test_helper.rb'
require 'digest'

class ShareTest < ApiTest
  def test_get_jobs
    json = get_json_id '/job/list', nil, 1030

    assert_equal json, { :status => 'ok', :jobs => [ { :id => 10301,
                                                       :created_at => '2015-01-01 00:00:00 +0800',
                                                       :started_at => nil,
                                                       :finished_at => nil,
                                                       :inode_id => 10302,
                                                       :revision => 1,
                                                       :name => 'filtre csv',
                                                       :status => 'created' },
                                                     { :id => 10302,
                                                       :created_at => '2015-01-02 00:00:00 +0800',
                                                       :started_at => '2015-01-02 01:00:00 +0800',
                                                       :finished_at => nil,
                                                       :inode_id => 10302,
                                                       :revision => 1,
                                                       :name => 'filtre csv',
                                                       :status => 'started' },
                                                     { :id => 10303,
                                                       :created_at => '2015-01-03 00:00:00 +0800',
                                                       :started_at => '2015-01-03 01:00:00 +0800',
                                                       :finished_at => '2015-01-03 02:00:00 +0800',
                                                       :inode_id => 10302, :revision => 1,
                                                       :name => 'filtre csv',
                                                       :status => 'cancelled' },
                                                     { :id => 10304,
                                                       :created_at => '2015-01-04 00:00:00 +0800',
                                                       :started_at => '2015-01-04 01:00:00 +0800',
                                                       :finished_at => '2015-01-04 03:00:00 +0800',
                                                       :inode_id => 10302,
                                                       :revision => 1,
                                                       :name => 'filtre csv',
                                                       :status => 'failed' } ] }
  end

  def test_post_job
    json = post_json_id '/job', { :inode_id => 10302, :revision => 1 }, 1030

    assert_equal json[:status], 'ok'
  end
end
