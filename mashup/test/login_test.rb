#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require_relative 'test_helper.rb'
require 'digest'

class LoginTest < ApiTest
  def test_login_successful
    json = post_json '/login', { :username => 'thomas@emi',
                                 :hashed_password => Digest::SHA256.hexdigest('password') }

    assert_equal json, { :status => 'ok',
                         :id => 1000,
                         :name => 'thomas' }
    assert_equal last_request.env['rack.session']['id'], 1000
  end

  def test_wrong_login
    json = post_json '/login', { :username => 'urbain@demer',
                                 :hashed_password => 'password' }

    assert_equal json, { :status => 'err',
                         :err_msg => 'Invalid email or password' }
  end

  def test_wrong_password
    json = post_json '/login', { :username => 'urbain@db',
                                 :hashed_password => 'passphrase' }

    assert_equal json, { :status => 'err',
                         :err_msg => 'Invalid email or password' }
  end

  def test_empty_password
    json = post_json '/login', { :username => 'urbain@db',
                                 :hashed_password => '' }

    assert_equal json, { :status => 'err',
                         :err_msg => 'Invalid email or password' }
  end

  def test_pending_account
    json = post_json '/login', { :username => 'urbain@db',
                                 :hashed_password => Digest::SHA256.hexdigest('password') }

    assert_equal json, { :status => 'err',
                         :err_msg => 'Dear urbain, your account is either pending validation or deactivated' }
  end

  def test_logout
    json = post_json_id '/logout', nil, 1000

    assert_equal json, { :status => 'ok' }
    assert_empty last_request.env['rack.session']
  end
end
