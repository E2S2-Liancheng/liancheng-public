#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# based on https://pooreffort.com/blog/testing-sinatra-apis/

# Set the rack environment to `test`
ENV['RACK_ENV'] = 'test'

# Pull in all of the gems including those in the `test` group
require 'bundler'
Bundler.require :default, :test

# Require test libraries
require 'minitest/autorun'
require 'minitest/pride'
#require 'minitest/spec'

# Load the sinatra app
require_relative '../main'

require 'rack/test'

class ApiTest < MiniTest::Test
  include Rack::Test::Methods

  parallelize_me!

  # JSON helpers
  def json_parse(body)
    json =
      begin
        JSON.parse(body, :symbolize_names => true)
      rescue JSON::JSONError
      end

    refute_nil json
    json
  end

  # Request helpers
  def get_json(*params)
    get(*params)
    assert_equal last_response.status, 200
    json_parse(last_response.body)
  end

  def post_json(*params)
    post(*params)
    assert_equal last_response.status, 200
    json_parse(last_response.body)
  end

  def get_id(url, params, user_id)
    get(url, params, {'rack.session' => { 'id' => user_id} })
  end

  def post_id(url, params, user_id)
    post(url, params, {'rack.session' => { 'id' => user_id} })
  end

  def put_id(url, params, user_id)
    put(url, params, {'rack.session' => { 'id' => user_id} })
  end

  def delete_id(url, params, user_id)
    delete(url, params, {'rack.session' => { 'id' => user_id} })
  end

  def get_json_id(url, params, user_id)
    get(url, params, {'rack.session' => { 'id' => user_id} })
    assert_equal last_response.status, 200
    json_parse(last_response.body)
  end

  def post_json_id(url, params, user_id)
    post(url, params, {'rack.session' => { 'id' => user_id} })
    assert_equal last_response.status, 200
    json_parse(last_response.body)
  end

  def put_json_id(url, params, user_id)
    put(url, params, {'rack.session' => { 'id' => user_id} })
    assert_equal last_response.status, 200
    json_parse(last_response.body)
  end

  def delete_json_id(url, params, user_id)
    delete(url, params, {'rack.session' => { 'id' => user_id} })
    assert_equal last_response.status, 200
    json_parse(last_response.body)
  end

  # This should rollback every transaction
  def run(*args, &block)
    result = nil
    Sequel::Model.db.transaction(:rollback => :always, :auto_savepoint => true) { result = super }
    result
  end

  def app
    Sinatra::Application
  end
end
