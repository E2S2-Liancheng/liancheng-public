#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require_relative 'test_helper.rb'
require 'digest'

class ShareTest < ApiTest
  def test_get_share_on_directory
    json = get_json_id '/share/10505', nil, 1050

    assert_equal json, { :status => 'ok',
                         :share => { :directory_id => 10505,
                                     :member_ids => [1000, 1070],
                                     :crew_ids => [10502, 10503],
                                     :rights => true } }
  end

  def test_get_share_on_directory_unshared
    get_id '/share/10503', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_get_share_on_directory_non_existing
    get_id '/share/666', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_get_share_on_directory_unauthorised
    get_id '/share/10001', nil, 1050

    assert_equal last_response.status, 403
  end

  def test_get_share_on_directory_shared_by_other
    get_id '/share/10505', nil, 1000

    assert_equal last_response.status, 403
  end

  def test_post_share
    json = post_json_id '/share/10003', { :member_ids => [1050],
                                          :crew_ids => [10001],
                                          :read_write => true }, 1000

    refute_empty DB[:share_with_member].where(:member_id => 1050, :directory_id => 10003)
    refute_empty DB[:share_with_crew].where(:crew_id => 10001, :directory_id => 10003)
    invites = DB[:share_invite].where(:member_id => [1020, 1030, 1040, 1050],
                                      :directory_id => 10003,
                                      :share_invite_status_id => 1,
                                      :item_read => false)
    assert_equal invites.count, 4

    assert_equal json, { :status => 'ok' }
  end

  def test_post_share_no_crew
    json = post_json_id '/share/10003', { :member_ids => [1050],
                                          :read_write => true }, 1000

    refute_empty DB[:share_with_member].where(:member_id => 1050, :directory_id => 10003)
    invites = DB[:share_invite].where(:member_id => 1050,
                                      :directory_id => 10003,
                                      :share_invite_status_id => 1,
                                      :item_read => false)
    assert_equal invites.count, 1

    assert_equal json, { :status => 'ok' }
  end

  def test_post_share_no_member
    json = post_json_id '/share/10003', { :crew_ids => [10001],
                                          :read_write => true }, 1000

    refute_empty DB[:share_with_crew].where(:crew_id => 10001, :directory_id => 10003)
    invites = DB[:share_invite].where(:member_id => [1020, 1030, 1040],
                                      :directory_id => 10003,
                                      :share_invite_status_id => 1,
                                      :item_read => false)
    assert_equal invites.count, 3

    assert_equal json, { :status => 'ok' }
  end

  def test_post_share_no_one
    post_id '/share/10003', { :read_write => true }, 1000

    assert_equal last_response.status, 400
  end

  def test_post_share_no_acl
    post_id '/share/10003', { :member_ids => [1050] }, 1000

    assert_equal last_response.status, 400
  end

  def test_post_share_non_existing_member
    post_id '/share/10003', { :member_ids => [666],
                              :crew_ids => [10001],
                              :read_write => true }, 1000

    assert_equal last_response.status, 404
  end

  def test_post_share_non_existing_crew
    post_id '/share/10003', { :member_ids => [1050],
                              :crew_ids => [666],
                              :read_write => true }, 1000

    assert_equal last_response.status, 404
  end

  def test_post_share_unauthorised_directory
    post_id '/share/10403', { :member_ids => [1050],
                              :crew_ids => [10001],
                              :read_write => true }, 1000

    assert_equal last_response.status, 403
  end

  def test_post_share_unauthorised_crew
    post_id '/share/10003', { :member_ids => [1050],
                              :crew_ids => [10501],
                              :read_write => true }, 1000

    assert_equal last_response.status, 403
  end

  def test_post_share_self
    post_id '/share/10003', { :member_ids => [1000,1050],
                              :crew_ids => [10001],
                              :read_write => true }, 1000

    assert_equal last_response.status, 400
  end

  def test_post_share_home
    post_id '/share/10001', { :member_ids => [1050],
                              :crew_ids => [10001],
                              :read_write => true }, 1000

    assert_equal last_response.status, 400
  end

  def test_post_share_trash
    post_id '/share/10002', { :member_ids => [1050],
                              :crew_ids => [10001],
                              :read_write => true }, 1000

    assert_equal last_response.status, 400
  end

  def test_post_share_trash
    post_id '/share/10008', { :member_ids => [1050],
                              :crew_ids => [10001],
                              :read_write => true }, 1000

    assert_equal last_response.status, 400
  end

  def test_post_share_ancestor_already_shared
    json = post_json_id '/share/10506', { :member_ids => [1000],
                                          :read_write => true }, 1050

    assert_equal json, { :status => 'err', :err_msg => 'err_share_nested_share' }
  end

  def test_post_share_descendant_already_shared
    json = post_json_id '/share/10403', { :member_ids => [1000],
                                          :read_write => true }, 1040

    assert_equal json, { :status => 'err', :err_msg => 'err_share_nested_share' }
  end

  def test_post_share_inside_another_shared
    json = post_json_id '/share/10506', { :member_ids => [1020],
                                          :read_write => true }, 1000

    assert_equal json, { :status => 'err', :err_msg => 'err_share_nested_share' }
  end

  def test_get_crews_for_member
    json = get_json_id '/share/10505/crews_for_member/1030', nil, 1050

    assert_equal json, { :status => 'ok',
                         :crews => [ { :id => 10502, :name => 'work' },
                                     { :id => 10503, :name => 'sports' } ],
                         :member_name => '윤아' }
  end

  def test_get_crews_for_member_no_crew
    json = get_json_id '/share/10404/crews_for_member/1050', nil, 1040

    assert_equal json, { :status => 'ok', :crews => [], :member_name => 'セルティ' }
  end

  def test_get_crews_for_member_non_existing_directory
    get_id '/share/666/crews_for_member/1030', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_get_crews_for_member_non_shared
    get_id '/share/10501/crews_for_member/1030', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_get_crews_for_member_unauthorised
    get_id '/share/10404/crews_for_member/1030', nil, 1050

    assert_equal last_response.status, 403
  end

  def test_get_crews_for_member_non_existing_member
    get_id '/share/10505/crews_for_member/666', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_get_members_for_crew_all_kept
    json = get_json_id '/share/10505/members_for_crew/10502', nil, 1050

    assert_equal json, { :status => 'ok',
                         :members => [ { :id => 1000, :name => 'thomas', :kept => true },
                                       { :id => 1030, :name => '윤아', :kept => true } ],
                         :crew_name => 'work' }
  end

  def test_get_members_for_crew
    json = get_json_id '/share/10505/members_for_crew/10503', nil, 1050

    assert_equal json, { :status => 'ok',
                         :members => [ { :id => 1020, :name => '수영', :kept => false },
                                       { :id => 1030, :name => '윤아', :kept => true },
                                       { :id => 1040, :name => '태연', :kept => false } ],
                         :crew_name => 'sports' }
  end

  def test_get_members_for_crew_non_existing_directory
    get_id '/share/666/members_for_crew/10503', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_get_members_for_crew_non_existing_crew
    get_id '/share/10505/members_for_crew/666', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_get_members_for_crew_non_shared_directory
    get_id '/share/10501/members_for_crew/10503', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_get_members_for_crew_unauthorised
    get_id '/share/10505/members_for_crew/10503', nil, 1000

    assert_equal last_response.status, 403
  end

  def test_get_members_for_crew_non_included_crew
    json = get_json_id '/share/10505/members_for_crew/10504', nil, 1050

    assert_equal json, { :status => 'err', :err_msg => 'err_share_not_shared_with' }
  end

  def test_delete_share_member
    json = delete_json_id '/share/10505/member/1000', nil, 1050

    assert_equal json, { :status => 'ok' }
  end

  def test_delete_share_member_keep
    json = delete_json_id '/share/10505/member/1000', { :give_copy => true }, 1050

    assert_empty DB[:directory].where(:parent_id => 10001).map(:name).select { |n| n.start_with?('copy of ') }

    assert_equal json, { :status => 'ok' }
  end

  def test_delete_share_member_access_lost_keep
    json = delete_json_id '/share/10505/member/1070', { :give_copy => true }, 1050

    refute_empty DB[:directory].where(:parent_id => 10701).map(:name).select { |n| n.start_with?('copy of ') }
    copy = DB[:directory].where(:parent_id => 10701)
    assert_equal copy.count, 1

    f1 = DB[:inode].where(:directory_id => copy.first[:id])
    assert_equal f1.count, 1
    assert_equal f1.first[:name], 'whatsup.doc'

    desc = DB[:directory].where(:parent_id => copy.first[:id]).first
    assert_equal desc[:name], 'argent'
    f1 = DB[:inode].where(:directory_id => desc[:id])
    assert_equal f1.count, 1
    assert_equal f1.first[:name], 'avery.tex'

    assert_equal json, { :status => 'ok' }
  end

  def test_delete_share_member_non_existing_directory
    delete_id '/share/666/member/1000', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_delete_share_member_non_shared_directory
    delete_id '/share/10501/member/1000', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_delete_share_member_non_existing_member
    delete_id '/share/10505/member/666', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_delete_share_member_unauthorised
    delete_id '/share/10505/member/1000', nil, 1000

    assert_equal last_response.status, 403
  end

  def test_delete_share_member_not_shared_with
    json = delete_json_id '/share/10505/member/1060', nil, 1050

    assert_equal json, { :status => 'err', :err_msg => 'err_share_not_shared_with' }
  end

  def test_delete_share_crew
    json = delete_json_id '/share/10505/crew/10502', nil, 1050

    assert_empty DB[:directory].where(:parent_id => [10001, 10301])
                               .map(:name).select { |n| n.start_with?('copy of ') }

    assert_equal json, { :status => 'ok' }
  end

  def test_delete_share_crew_access_lost
    json = delete_json_id '/share/10505/crew/10503', nil, 1050

    assert_empty DB[:directory].where(:parent_id => [10201, 10301, 10401])
                               .map(:name).select { |n| n.start_with?('copy of ') }

    assert_equal json, { :status => 'ok' }
  end

  def test_delete_share_crew_keep
    json = delete_json_id '/share/10505/crew/10502', { :give_copy => true }, 1050

    assert_empty DB[:directory].where(:parent_id => [10001, 10301])
                               .map(:name).select { |n| n.start_with?('copy of ') }

    assert_equal json, { :status => 'ok' }
  end

  def test_delete_share_crew_keep_access_lost
    json = delete_json_id '/share/10505/crew/10503', { :give_copy => true }, 1050

    assert_empty DB[:directory].where(:parent_id => [10301, 10401])
                               .map(:name).select { |n| n.start_with?('copy of ') }

    copy = DB[:directory].where(:parent_id => 10201)
    assert_equal copy.count, 1

    f1 = DB[:inode].where(:directory_id => copy.first[:id])
    assert_equal f1.count, 1
    assert_equal f1.first[:name], 'whatsup.doc'

    desc = DB[:directory].where(:parent_id => copy.first[:id]).first
    assert_equal desc[:name], 'argent'
    f1 = DB[:inode].where(:directory_id => desc[:id])
    assert_equal f1.count, 1
    assert_equal f1.first[:name], 'avery.tex'

    assert_equal json, { :status => 'ok' }
  end

  def test_delete_share_crew_non_existing_directory
    delete_id '/share/666/crew/10502', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_delete_share_crew_non_shared_directory
    delete_id '/share/10501/crew/10502', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_delete_share_crew_non_existing_crew
    delete_id '/share/10505/crew/666', nil, 1050

    assert_equal last_response.status, 404
  end

  def test_delete_share_crew_unauthorised
    delete_id '/share/10505/crew/10502', nil, 1000

    assert_equal last_response.status, 403
  end

  def test_delete_share_crew_not_shared_with
    json = delete_json_id '/share/10505/crew/10504', nil, 1050

    assert_equal json, { :status => 'err', :err_msg => 'err_share_not_shared_with' }
  end

  def test_get_accessible_share_list
    json = get_json_id '/share/list_accessible', nil, 1030

    assert_equal json, { :status => 'ok', :shares => [10505] }
  end

  def test_get_accessible_share_list_none
    json = get_json_id '/share/list_accessible', nil, 1050

    assert_equal json, { :status => 'ok', :shares => [] }
  end

  def test_post_invitation_accept
    json = post_json_id '/share/invitation', { :directory_id => 10404, :action => 'accept' }, 1000

    refute_empty DB[:share_invite].where(:directory_id => 10404, :member_id => 1000, :share_invite_status_id => 10)

    assert_equal json, { :status => 'ok' }
  end

  def test_post_invitation_ignore
    json = post_json_id '/share/invitation', { :directory_id => 10404, :action => 'ignore' }, 1000

    assert_empty DB[:share_invite].where(:directory_id => 10404, :member_id => 1000)

    assert_equal json, { :status => 'ok' }
  end

  def test_post_invitation_block
    json = post_json_id '/share/invitation', { :directory_id => 10404, :action => 'block' }, 1000

    refute_empty DB[:share_invite].where(:directory_id => 10404, :member_id => 1000, :share_invite_status_id => 2)

    assert_equal json, { :status => 'ok' }
  end

  def test_post_invitation_non_existing_directory
    post_id '/share/invitation', { :directory_id => 666, :action => 'accept' }, 1000

    assert_equal last_response.status, 404
  end

  def test_post_invitation_non_existing_action
    post_id '/share/invitation', { :directory_id => 10404, :action => 'remboursez' }, 1000

    assert_equal last_response.status, 400
  end

  def test_post_invitation_not_invited
    post_id '/share/invitation', { :directory_id => 10404, :action => 'accept' }, 1060

    assert_equal last_response.status, 403
  end

  def test_post_invitation_already_blocked
    json = post_json_id '/share/invitation', { :directory_id => 10505, :action => 'accept' }, 1040

    assert_equal json, { :status => 'err', :err_msg => 'err_invite_already_used' }
  end

  def test_post_invitation_already_accepted
    json = post_json_id '/share/invitation', { :directory_id => 10505, :action => 'accept' }, 1000

    assert_equal json, { :status => 'err', :err_msg => 'err_invite_already_used' }
  end
end
