#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# prepares the testing database
require 'pg'
require 'sequel'
require 'digest'
require 'json'

puts 'Creating the testing database'

Sequel.connect(ENV['DATABASE_URL']) do |db|

  db.drop_table?(:job_output, :job, :job_status,
                 :file_obfuscated, :file_published,
                 :inode_revision, :inode, :storage_file, :mime_type,
                 :share_invite, :share_with_member, :share_with_crew,
                 :crew_member, :crew,
                 :member, :account_status,
                 :directory, :share_invite_status)

  ### account_status ##########################################################

  db.create_table(:account_status) do
    Integer :id,  :primary_key => true
    String  :name
  end

  db[:account_status].import( [:id, :name],
                            [ [1,   'validation pending'],
                              [10,  'active'] ])

  ### directory ###############################################################

  db.create_table(:directory) do
    primary_key :id,                    :type => Bignum
    String      :name,                  :null => false
    Timestamp   :modified_at,           :null => false
    foreign_key :parent_id, :directory, :type => Bignum
  end

  db[:directory].import( [:id,   :name,       :modified_at,          :parent_id],
                       [ [10001, 'home',      '2015-01-01T00:00:00', nil],
                         [10002, 'trash',     '2015-01-01T00:00:00', nil],
                         [10003, 'assez',     '2015-01-01T00:00:00', 10001],
                         [10004, 'd\'essais', '2015-01-01T00:00:00', 10001],
                         [10005, 'thunder',   '2015-01-01T00:00:00', 10003],
                         [10006, 'struck',    '2015-01-01T00:00:00', 10003],

                         [10007, 'black',     '2015-01-01T00:00:00', 10002],
                         [10008, 'shuck',     '2015-01-01T00:00:00', 10007],

                         [10201, 'home',      '2015-01-01T00:00:00', nil],
                         [10202, 'trash',     '2015-01-01T00:00:00', nil],

                         [10301, 'home',      '2015-01-01T00:00:00', nil],
                         [10302, 'trash',     '2015-01-01T00:00:00', nil],
                         [10303, 'François',  '2015-01-01T00:00:00', 10301],
                         [10304, 'Élizabeth', '2015-01-01T00:00:00', 10301],

                         [10401, 'home',      '2015-01-01T00:00:00', nil],
                         [10402, 'trash',     '2015-01-01T00:00:00', nil],
                         [10403, '오빠',       '2015-01-01T00:00:00', 10401],
                         [10404, '사랑해',     '2015-01-01T00:00:00', 10403],            # shared directory

                         [10501, 'home',      '2015-01-01T00:00:00', nil],
                         [10502, 'trash',     '2015-01-01T00:00:00', nil],
                         [10503, 'vierge',    '2015-01-01T00:00:00', 10501],
                         [10504, 'de fer',    '2015-01-01T00:00:00', 10501],
                         [10505, 'chère',     '2015-01-01T00:00:00', 10501],            # shared directory
                         [10506, 'argent',    '2015-01-01T00:00:00', 10505],

                         [10601, 'home',      '2015-01-01T00:00:00', nil],
                         [10602, 'trash',     '2015-01-01T00:00:00', nil],
                         [10603, 'yolo',      '2015-01-01T00:00:00', 10601],
                         [10604, 'swag',      '2015-01-01T00:00:00', 10603],
                         [10605, 'doge',      '2015-01-01T00:00:00', 10604],

                         [10701, 'home',      '2015-01-01T00:00:00', nil],
                         [10702, 'trash',     '2015-01-01T00:00:00', nil],
                       ])

  ### member ##################################################################

  db.create_table(:member) do
    primary_key :id
    String      :name,                                :null => false
    String      :email,                               :null => false
    String      :password,                            :null => false
    foreign_key :home_directory_id,  :directory,      :type => Bignum
    foreign_key :trash_directory_id, :directory,      :type => Bignum
    String      :username
    foreign_key :account_status_id,  :account_status, :null => false
  end

  def fun_pass(password)
    Sequel.function(:crypt, Digest::SHA256.hexdigest(password), Sequel.function(:gen_salt, 'bf', 14))
  end

  db[:member].import( [:id,  :name,     :email,       :password,            :home_directory_id, :trash_directory_id, :username, :account_status_id], [
                      [1000, 'thomas',  'thomas@emi', fun_pass('password'), 10001,              10002,               nil,       10],
                      [1010, 'urbain',  'urbain@db',  fun_pass('password'), nil,                nil,                 nil,       1],
                      [1020, '수영',     'su@yeong',   fun_pass('password'), 10201,              10202,               nil,       10],
                      [1030, '윤아',     'yu@na',      fun_pass('password'), 10301,              10302,               nil,       10],
                      [1040, '태연',     'tae@yeon',   fun_pass('password'), 10401,              10402,               nil,       10],
                      [1050, 'セルティ', 'celty@drrr', fun_pass('password'), 10501,              10502,               'drrr',        10],
                      [1060, '林立慧',   'li@hui',     fun_pass('password'), 10601,              10602,               nil,       10],
                      [1070, 'บลูเบอร์รี่', 'shi@mi',     fun_pass('password'), 10701,              10702,               '-_-...',  10],
                    ])

  ### share_invite_status #####################################################

  db.create_table(:share_invite_status) do
    Integer :id,   :primary_key => true
    String  :name, :null => false
  end

  db[:share_invite_status].import( [:id, :name],
                                 [ [1,   'invited'],
                                   [2,   'blocked'],
                                   [10,  'accepted'] ])

  ### crew ####################################################################

  db.create_table(:crew) do
    primary_key :id,                         :type => Bignum
    String      :name,                       :null => false
    foreign_key :creator_member_id, :member, :null => false
  end

  db[:crew].import( [:id,   :name,    :creator_member_id],
                  [ [10001, '소녀시대', 1000],
                    [10002, 'empty',  1000],
                    [10501, 'riders', 1050],
                    [10502, 'work',   1050],
                    [10503, 'sports', 1050],
                    [10504, 'noshar', 1050],                    # this one is not shared
                  ])

  ### crew_member #############################################################

  db.create_table(:crew_member) do
    foreign_key  :member_id, :member
    foreign_key  :crew_id,   :crew
    primary_key [:member_id, :crew_id]
  end

  db[:crew_member].import( [:crew_id, :member_id],
                         [ [10001,    1020],
                           [10001,    1030],
                           [10001,    1040],

                           [10501,    1000],

                           [10502,    1000],
                           [10502,    1030],

                           [10503,    1020],
                           [10503,    1030],
                           [10503,    1040],

                           [10504,    1000],
                           [10504,    1060],
                         ])

  ### share_invite ############################################################

  db.create_table(:share_invite) do
    foreign_key  :directory_id, :directory
    foreign_key  :member_id,    :member,    :null => false
    primary_key [:directory_id, :member_id]
    foreign_key  :share_invite_status_id, :share_invite_status, :null => false
    TrueClass    :item_read,                                    :null => false
  end

  db[:share_invite].import( [:directory_id, :member_id, :share_invite_status_id, :item_read],
                          [ [10404,         1000,       1,                       false],
                            [10505,         1000,       10,                      true],
                            [10505,         1020,       10,                      true],
                            [10505,         1030,       10,                      true],
                            [10505,         1040,       2,                       true],
                            [10505,         1070,       10,                      true],
                          ])

  ### share_with_member #######################################################

  db.create_table(:share_with_member) do
    foreign_key  :directory_id, :directory
    foreign_key  :member_id,    :member
    primary_key [:directory_id, :member_id]
    TrueClass :read_write, :null => false
  end

  db[:share_with_member].import( [:directory_id, :member_id, :read_write],
                               [ [10404,         1000,       true],
                                 [10505,         1000,       true],
                                 [10505,         1070,       true],
                               ])

  ### share_with_crew #########################################################

  db.create_table(:share_with_crew) do
    foreign_key  :directory_id, :directory
    foreign_key  :crew_id,      :crew
    primary_key [:directory_id, :crew_id]
    TrueClass :read_write, :null => false
  end

  db[:share_with_crew].import( [:directory_id, :crew_id, :read_write],
                             [ [10505,         10502,    true],
                               [10505,         10503,    true] ])

  ### storage_file ############################################################

  db.create_table(:storage_file) do
    primary_key :id, :null => false
    String :md5_id,  :null => false
    String :sha1sum, :null => false
  end

  #file_folder = '../files'      # WARNING why ../ ?
  file_folder = '/var/lib/e2s2-mashup/files'
  Dir.mkdir(file_folder) unless Dir.exists?(file_folder)
  files = (900..904).map do |n|
    c = "file #{n}"
    fn = File.join(file_folder, n.to_s)     # HACK hardcoded path
    File.write(fn, c)
    [n, Digest::MD5.hexdigest(c), Digest::SHA1.hexdigest(c)]
  end

  File.open(File.join(file_folder, '905'), 'w') do |f|
    c = [ 'temperature,humidity,pressure',
          '32.7,92.0,1009',
          '32.4,91.8,1011',
          '32.0,93.0,1014' ].join("\n")
    f.puts c

    files << [905, Digest::MD5.hexdigest(c), Digest::SHA1.hexdigest(c)]
  end

  File.open(File.join(file_folder, '906'), 'w') do |f|
    c = JSON[ :meta => { :app => 'UrbanDB', :version => 1 },
              :nodes => [ { :id => 1,
                            :type => 'import_csv',
                            :data => { :file => { :inode_id => 10301,
                                                  :revision => 1,
                                                  :full_path => '/home/François/MetsTesHauts.csv' },
                                      :has_header => true,
                                      :cols => [ { :orig => 'temperature', :user => 'temperature' },
                                                  { :orig => 'humidity', :user => 'humidity' },
                                                  { :orig => 'pressure', :user => 'pressure' } ]
                                    } },
                          { :id => 2,
                            :type => 'operator_filter',
                            :data => { :clauses => [ { :name => 'pressure', :op => 'gteq', 'value' => '1010' } ],
                                      :cols => [ { :name => 'temperature', :type => 'string' },
                                                  { :name => 'humidity', :type => 'string' },
                                                  { :name => 'pressure', :type => 'string' } ]
                                    } },
                          { :id => 3,
                            :type => 'export_csv',
                            :data => { :file => { :directory_id => 10303,
                                                  :name => '.csv' }
                                    } },
                        ],
              :connectors => [ { :up   => { :node => 1, :hook => 0 },
                                :down => { :node => 2, :hook => 0 } },
                              { :up   => { :node => 2, :hook => 0 },
                                :down => { :node => 3, :hook => 0 } } ] ]
    f.puts c

    files << [906, Digest::MD5.hexdigest(c), Digest::SHA1.hexdigest(c)]
  end

  db[:storage_file].import( [:id,  :md5_id, :sha1sum], files )

  ### mime_type ###############################################################

  db.create_table(:mime_type) do
    primary_key :id
    String :name, :null => false
  end

  db[:mime_type].import( [:id,  :name],
                       [ [1,    'application/x-urbandb-workflow'],
                         [2,    'application/x-urbandb-stream'],
                         [1000, 'text/plain'] ])

  ### inode / inode_revision ##################################################

  db.create_table(:inode) do
    primary_key :id,                       :type => Bignum
    String      :name,                     :null => false
    foreign_key :directory_id, :directory, :null => false
  end

  db.create_table(:inode_revision) do
    foreign_key  :inode_id, :inode
    Bignum       :revision
    primary_key [:inode_id, :revision]
    Bignum       :size,                           :null => false
    foreign_key  :modified_by, :member,           :null => false
    Timestamp    :modified_at,                    :null => false
    Integer      :mime_type_id
    foreign_key  :storage_file_id, :storage_file, :type => Bignum
  end

  db[:inode].import( [:id,   :name,              :directory_id],
                   [ [10001, 'pre.txt',          10003],
                     [10002, 'ter.rar',          10003],

                     [10301, 'MetsTesHauts.csv', 10303],
                     [10302, 'filtre csv',       10303],

                     [10501, 'whatsup.doc',      10505],
                     [10502, 'avery.tex',        10506],

                     [10601, 'lenny.face',       10604],
                     [10602, 'top.kek',          10605],
                   ])

  db[:inode_revision].import( [:inode_id, :revision, :size, :modified_by, :modified_at,          :mime_type_id, :storage_file_id],
                            [ [10001,     1,         8,     1000,         '2015-01-01T00:00:00', 1000,          900],
                              [10001,     2,         8,     1000,         '2015-01-02T00:00:00', 1000,          902],
                              [10002,     1,         8,     1000,         '2015-01-01T00:00:00', 1000,          901],

                              [10301,     1,         75,    1030,         '2015-01-01T00:00:00', 1000,          905],
                              [10302,     1,         747,   1030,         '2015-01-01T00:00:00', 1,             906],

                              [10501,     1,         8,     1050,         '2015-01-01T00:00:00', 1000,          903],
                              [10502,     1,         8,     1050,         '2015-01-01T00:00:00', 1000,          904],

                              [10601,     1,         8,     1060,         '2015-01-01T00:00:00', 1000,          901],
                              [10602,     1,         8,     1060,         '2015-01-01T00:00:00', 1000,          902],
                            ])

  ### job/ job_status / job_output ############################################

  db.create_table(:job_status) do
    Integer :id,  :primary_key => true
    String  :name
  end

  db.create_table(:job) do
    primary_key :id,                  :type => Bignum
    foreign_key :created_by, :member, :type => Bignum
    Timestamp   :created_at
    Timestamp   :started_at
    Timestamp   :finished_at
    Bignum      :inode_id
    Bignum      :revision
    foreign_key [:inode_id, :revision], :inode_revision
    foreign_key :job_status_id, :job_status
  end

  db.create_table(:job_output) do
    foreign_key :job_id, :job
    Bignum      :inode_id
    Bignum      :revision
    foreign_key [:inode_id, :revision], :inode_revision
  end

  db[:job_status].import( [:id, :name],
                        [ [1,   'created'],
                          [2,   'started'],
                          [3,   'cancelled'],
                          [4,   'failed'],
                          [5,   'completed'] ])

  db[:job].import( [:id,   :created_by, :created_at,           :started_at,           :finished_at,          :inode_id, :revision, :job_status_id],
                 [ [10301, 1030,        '2015-01-01T00:00:00', nil,                   nil,                   10302,     1,         1],
                   [10302, 1030,        '2015-01-02T00:00:00', '2015-01-02T01:00:00', nil,                   10302,     1,         2],
                   [10303, 1030,        '2015-01-03T00:00:00', '2015-01-03T01:00:00', '2015-01-03T02:00:00', 10302,     1,         3],
                   [10304, 1030,        '2015-01-04T00:00:00', '2015-01-04T01:00:00', '2015-01-04T03:00:00', 10302,     1,         4] ])

  ### file_obfuscated / file_published ########################################

  db.create_table(:file_obfuscated) do
    String      :id,                 :primary_key => true
    foreign_key :member_id, :member
    foreign_key :inode_id,  :inode
    String      :annotation
  end

  db.create_table(:file_published) do
    foreign_key :member_id, :member
    foreign_key :inode_id,  :inode
    primary_key [:member_id, :inode_id]
  end

  db[:file_obfuscated].import( [:id,                                    :member_id, :inode_id, :annotation], [
                               ['68f5508b-bc73-4722-bddc-dd907f152f04', 1050,       10501,     'not nil'],
                               ['6b9bc6ce-7a18-4397-bf09-3574f8d6ad01', 1050,       10501,     nil],
                               ['7c42e6b1-9e80-492e-8e8c-de48383abc0c', 1060,       10601,     nil],
                               ['1d1ad771-09bd-4e18-b0cf-a0294de669ea', 1070,       10501,     nil],
                               ['919a8c00-8f6d-4ebc-9478-2300e1ede755', 1070,       10502,     nil],
                               ['b37aa9e5-6ccf-4cdd-9f95-ce91688fa68a', 1020,       10502,     nil],
                             ])

  db[:file_published].import( [:member_id, :inode_id], [
                              [1050, 10501],
                              [1060, 10602],
                              [1070, 10501],
                              [1070, 10502],
                              [1020, 10501],
                            ])

end

puts 'Testing database created'
