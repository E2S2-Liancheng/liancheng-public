#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require_relative 'test_helper.rb'

class CrewTest < ApiTest
  def test_crew_successful
    json = get_json_id '/crew/10001', nil, 1000

    assert_equal json, { :status => 'ok', :crew => { :id => 10001, :name => '소녀시대' } }
  end

  def test_crew_non_existing
    get_id '/crew/666', nil, 1000

    assert_equal last_response.status, 404
  end

  def test_crew_unauthorized
    get_id '/crew/10501', nil, 1000

    assert_equal last_response.status, 403
  end

  def test_crew_members_successful
    json = get_json_id '/crew/10001/members', nil, 1000

    assert_equal json, { :status => 'ok', :members => [{:id=>1020, :name=>"수영"},
                                                       {:id=>1030, :name=>"윤아"},
                                                       {:id=>1040, :name=>"태연"}] }
  end

  def test_crew_members_empty
    json = get_json_id '/crew/10002/members', nil, 1000

    assert_equal json, { :status => 'ok', :members => [] }
  end

  def test_crew_members_non_existing
    get_id '/crew/666/members', nil, 1000

    assert_equal last_response.status, 404
  end

  def test_crew_members_unauthorised
    get_id '/crew/10501/members', nil, 1000

    assert_equal last_response.status, 403
  end

  def test_new_crew_successful
    json = post_json_id '/crew', { :name => 'test_group', :members => [1020, 1050] }, 1000

    new_crew = DB[:crew].where(:name => 'test_group', :creator_member_id => 1000)
    refute_empty new_crew
    new_crew_id = new_crew.first[:id]
    assert_equal DB[:crew_member].where(:crew_id => new_crew_id).map(:member_id), [1020, 1050]

    assert_equal json, { :status => 'ok', :crew => { :id => new_crew_id, :name => 'test_group' } }
  end

  def test_new_crew_empty_successful
    json = post_json_id '/crew', { :name => 'test_group' }, 1000

    new_crew = DB[:crew].where(:name => 'test_group', :creator_member_id => 1000)
    refute_empty new_crew
    new_crew_id = new_crew.first[:id]

    assert_equal json, { :status => 'ok', :crew => { :id => new_crew_id, :name => 'test_group' } }
  end

  def test_new_crew_missing_name
    post_id '/crew', { :members => [1020, 1050] }, 1000

    assert_equal last_response.status, 400
  end

  def test_new_crew_empty_name
    post_id '/crew', { :name => '', :members => [1020, 1050] }, 1000

    assert_equal last_response.status, 400
  end

  def test_new_crew_non_existing_member_id
    post_id '/crew', { :name => 'test_group', :members => [666, 1050] }, 1000

    assert_equal last_response.status, 404
  end

  def test_new_crew_bogus_member
    post_id '/crew', { :name => 'test_group', :members => 1050 }, 1000

    assert_equal last_response.status, 400
  end

  def test_new_crew_bogus_member_id
    post_id '/crew', { :name => 'test_group', :members => ['29A', 1050] }, 1000

    assert_equal last_response.status, 400
  end

  def test_new_crew_self
    post_id '/crew', { :name => 'test_group', :members => [1000, 1050] }, 1000

    assert_equal last_response.status, 400
  end
end
