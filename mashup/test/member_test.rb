#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require_relative 'test_helper.rb'
require 'digest'

class MemberTest < ApiTest
  def test_get_self
    json = get_json_id '/member', nil, 1000

    assert_equal json, { :status => 'ok', :member => { :id => 1000,
                                                       :email => 'thomas@emi',
                                                       :name => 'thomas',
                                                       :home_directory_id => 10001,
                                                       :username => nil } }
  end

  def test_get_member_list
    json = get_json_id '/member/list', nil, 1000

    assert_equal json, { :status => 'ok', :members => [ { :id => 1020, :name => '수영' },
                                                        { :id => 1030, :name => '윤아' },
                                                        { :id => 1040, :name => '태연' },
                                                        { :id => 1050, :name => 'セルティ' },
                                                        { :id => 1060, :name => '林立慧' },
                                                        { :id => 1070, :name => 'บลูเบอร์รี่' } ] }
  end

  def test_get_member_crews
    json = get_json_id '/member/crews', nil, 1000

    assert_equal json, { :status => 'ok', :crews => [ { :id => 10001, :name => '소녀시대' },
                                                      { :id => 10002, :name => 'empty' } ] }
  end

  def test_get_member
    json = get_json_id '/member/1050', nil, 1000

    assert_equal json, { :status => 'ok', :member => { :id => 1050, :name => 'セルティ' } }
  end

  def test_member_rename
    json = put_json_id '/member/name', { :name => 'ฐม้า' }, 1000

    assert_equal json, { :status => 'ok', :name => 'ฐม้า' }
  end

  def test_member_rename_missing_name
    put_id '/member/name', nil, 1000

    assert_equal last_response.status, 400
  end

  def test_member_rename_empty_name
    put_id '/member/name', { :name => '' }, 1000

    assert_equal last_response.status, 400
  end

  def test_member_rename_already_exists                 # even though not validated
    json = put_json_id '/member/name', { :name => 'urbain' }, 1000

    assert_equal json, { :status => 'err', :err_msg => 'err_member_name_exists' }
  end

  def test_member_change_password
    new_pass = Digest::SHA256.hexdigest('drowssap')

    json = put_json_id '/member/password', { :old_password => Digest::SHA256.hexdigest('password'),
                                             :new_password => new_pass }, 1000

    refute_empty DB[:member].where(:id => 1000, :password => Sequel.function(:crypt, new_pass, :password))

    assert_equal json, { :status => 'ok' }
  end

  def test_member_change_password_wrong_old
    new_pass = Digest::SHA256.hexdigest('drowssap')

    json = put_json_id '/member/password', { :old_password => Digest::SHA256.hexdigest('wrong'),
                                             :new_password => new_pass }, 1000

    assert_equal json, { :status => 'err', :err_msg => 'err_member_inc_old_pass' }
  end
end
