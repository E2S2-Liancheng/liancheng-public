#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require_relative 'test_helper.rb'

class NotificationTest < ApiTest
  def test_get_notification_count
    json = get_json_id '/notif/count', nil, 1000

    assert_equal json, { :status => 'ok', :count => 1 }
  end

  def test_get_notification_list
    json = get_json_id '/notif/list', nil, 1000

    assert_equal json, { :status => 'ok', :notifs => [ { :type => 'share',
                                                         :read => false,
                                                         :directory_id => 10404,
                                                         :directory_name => '사랑해',
                                                         :member_name => '태연' } ] }
  end
end
