#!/usr/bin/env ruby
#
# Copyright (C) 2016  Thomas Kister, Fajrian Yunus, Stéphane Bressan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# general
require 'set'

# gems
require 'sinatra'
require 'haml'
require 'json'
require 'thread'
require 'fileutils'
require 'httpclient'
require 'active_support/core_ext/hash'

# first read the settings to eventually configure the logger
require_relative '../common/settings'

unless defined?($logger)
  require 'logger'
  $logger = Logger.new('/dev/null')
end

# local files
require_relative '../common/common'
require_relative '../common/sinatra_errors'
require_relative '../common/api_errors'
require_relative '../common/wrapmutex'
require_relative 'utils'
require_relative 'database'


# read the Sinatra's web server's configuration and set it up
SETTINGS['self'].each { |k,v| set k.to_sym, v } if SETTINGS['self']

# use sessions with a timeout of 12 hours
use Rack::Session::Pool, :expire_after => 43200


# User/Admin Access Control
# Check before access to all resources but the ones from PUBLIC_PAGES
#
PUBLIC_PAGES = %w[ /login /admin/login /stream/file ].to_set
before do
  pass if PUBLIC_PAGES.include?(request.path_info) || request.path_info =~ /\/register.*/               \
                                                   || request.path_info =~ /\/publish\/obfuscated\/.*/  \
                                                   || request.path_info =~ /\/publish\/home\/.*/
  unless session[:id] || session[:admin]
    msg = "Non-authenticated client request from #{request.ip} on #{request.request_method} #{request.path_info}"
    $logger.warn(msg)
    halt 401
  end
end


#-- WARNING password must not be logged
#
post '/login' do
  credentials = DB.transaction { Members.check_credentials(params[:username], params[:hashed_password]) }

  content_type :json
  if credentials
    if credentials[0]
      session[:id] = credentials[0]

      session[:pipe_conn] = HTTPClient.new

      JSON[ :status => 'ok', :id => credentials[0], :name => credentials[1] ]
    else
      msg = "Dear #{credentials[1]}, your account is either pending validation or deactivated"
      JSON[ :status => 'err', :err_msg => msg ]
    end
  else
    JSON[ :status => 'err', :err_msg => 'Invalid email or password' ]
  end
end

post '/logout' do
  session.clear
  JSON[ :status => 'ok' ]
end


require_relative 'app/admin'
require_relative 'app/registration'
require_relative 'app/misc'

require_relative 'app/directory'
require_relative 'app/member'
require_relative 'app/crew'
require_relative 'app/share'

require_relative 'app/storage_file'
require_relative 'app/inode'
require_relative 'app/publish'

require_relative 'app/ops'

if SETTINGS['jobs']['enabled']
  require_relative 'app/workflow'
  require_relative 'app/job'
end

require_relative 'app/stream'

require_relative 'app/notification'
